﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using VVS_API_CF.Models;

namespace VVS_API_CF.Services
{
    public class EmailJobs
    {

        private vvsContext db = new vvsContext();
        public async Task StudentNotAtSchool()
        {

            DateTime dateTime = DateTime.Now;

            string dateToday = dateTime.ToString("d");
            DayOfWeek day = DateTime.Now.DayOfWeek;
            string dayToday = day.ToString();

            //If day of the week is between Monday and Friday
            if ((dayToday == DayOfWeek.Monday.ToString()) ||
                (dayToday == DayOfWeek.Tuesday.ToString()) ||
                (dayToday == DayOfWeek.Wednesday.ToString()) ||
                (dayToday == DayOfWeek.Thursday.ToString()) ||
                (dayToday == DayOfWeek.Friday.ToString())
                )
            {
                // Get all Bicycles that have a tag ID and are assigned to a student
                var bicycles = await db.Bicycle.Where(b =>
                (b.TagID > 0 || b.TagID != null) &&
                (b.BicycleStatusID == 1)
                ).ToListAsync();

                var TodayDateTime = DateTime.Now.Date;
                var TodayDate = TodayDateTime.Date;

                // If there are no bicycles that have been assigned tags or assigned to a student, then do nothing
                if (bicycles.Count > 0)
                {

                    foreach (var bicycle in bicycles)
                    {
                        // Look for the bicycle 
                        var log = await db.Log.Where(l => l.TagID == bicycle.TagID && DbFunctions.TruncateTime(l.DateTime.Value) == DbFunctions.TruncateTime(TodayDate)).FirstOrDefaultAsync();

                        //If the log entry does not exist then send the email
                        if (log == null)
                        {
                            var bicyceStudent = await db.BicycleStudent
                                .Include(bs => bs.Student)
                                .Include(bs => bs.Student.Guardian)
                                .Where(bs => bs.BicycleID == bicycle.BicycleID && bs.DateUnassigned == null)
                                .FirstOrDefaultAsync();

                            using (var message = new MailMessage("vvinf370@gmail.com", bicyceStudent.Student.Guardian.GuardianEmail))
                            {
                                message.Subject = "School Attendance    ";
                                message.Body = "Good day," + "Please not that your child did not attend school on " + DateTime.Now + "." +
                                    "Please contact the school Administrator should you have any queries." + "Kind regards, -#Bikes4ERP";
                                using (SmtpClient client = new SmtpClient
                                {
                                    EnableSsl = true,
                                    Host = "smtp.gmail.com",
                                    Port = 587,
                                    Credentials = new NetworkCredential("vvinf370@gmail.com", "Virtu@l370")
                                })
                                {
                                    client.Send(message);
                                }
                            }// using

                        }// if (log == null)

                    } //foreach (var bicycle in bicycles)

                }// if bicycles.Count > 0


            }// If current Day is a weekday

            //return dateTime;

            //throw new NotImplementedException();
        }

        public void BikeForTwoYears()
        {
            db.Configuration.ProxyCreationEnabled = false;

            var BicycleStudentList = db.BicycleStudent
                .Include( bs => bs.Student )
                .Where(bs => bs.DateUnassigned == null)
                .ToList();

            var TodaysDate = DateTime.Now.Date;

            if (BicycleStudentList.Count > 0)
            {
                foreach (var student in BicycleStudentList)
                {
                    var TimePassed = TodaysDate - student.DateAssigned.Value.Date;

                    var Days = Convert.ToInt32(db.BusinessRule.Find(3).Value);

                    if (TimePassed >= TimeSpan.FromDays(Days))
                    {
                        var SchoolAdminList = db.AdminSchool
                            .Include( a => a.Admin.User )
                            .Where(a => a.SchoolID == student.Student.SchoolID)
                            .ToList();

                        foreach (var schoolAdmin in SchoolAdminList)
                        {
                            using (var message = new MailMessage("vvinf370@gmail.com", schoolAdmin.Admin.User.Email))
                            {
                                message.Subject = "Bicycle Ownership";
                                message.Body = "Good day, Please note that " + student.Student.StudentFullName + " has reached their ownership period on the date of " + DateTime.Now + " Kind regards, -#Bikes4ERP";
                                using (SmtpClient client = new SmtpClient
                                {
                                    EnableSsl = true,
                                    Host = "smtp.gmail.com",
                                    Port = 587,
                                    Credentials = new NetworkCredential("vvinf370@gmail.com", "Virtu@l370")
                                })
                                {
                                    client.Send(message);
                                }
                            }// using
                        }

                    }
                }
            }

        }

    }
}