﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
//using System.Windows.Forms;
using Quartz;
using VVS_API_CF.Models;

namespace VVS_API_CF.Services
{
    public class EmailJob : IJob
    {
        private vvsContext db = new vvsContext();
        public async Task Execute(IJobExecutionContext context)
        {

            DateTime dateTime = DateTime.Now;

            string dateToday = dateTime.ToString("d");
            DayOfWeek day = DateTime.Now.DayOfWeek;
            string dayToday = day.ToString();

            //If day of the week is between Monday and Friday
            if ((dayToday == DayOfWeek.Monday.ToString()) || 
                (dayToday == DayOfWeek.Tuesday.ToString()) ||
                (dayToday == DayOfWeek.Wednesday.ToString()) ||
                (dayToday == DayOfWeek.Thursday.ToString()) ||
                (dayToday == DayOfWeek.Friday.ToString())
                )
            {
                // Get all Bicycles that have a tag ID and are assigned to a student
                var bicycles = await db.Bicycle.Where(b => 
                (b.TagID > 0 || b.TagID != null) &&
                (b.BicycleStatusID == 1)
                ).ToListAsync();

                var TodayDateTime = DateTime.Now.Date;
                var TodayDate = TodayDateTime.Date;

                // If there are no bicycles that have been assigned tags or assigned to a student, then do nothing
                if (bicycles.Count > 0)
                {

                    foreach (var bicycle in bicycles)
                    {
                        // Look for the bicycle 
                        var log = await db.Log.Where(l => l.TagID == bicycle.TagID && DbFunctions.TruncateTime(l.DateTime.Value) == DbFunctions.TruncateTime(TodayDate)).FirstOrDefaultAsync();

                        //If the log entry does not exist then send the email
                        if (log == null)
                        {
                            var bicyceStudent = await db.BicycleStudent
                                .Include( bs => bs.Student )
                                .Include(bs => bs.Student.Guardian)
                                .Where(bs => bs.BicycleID == bicycle.BicycleID && bs.DateUnassigned == null)
                                .FirstOrDefaultAsync();

                            using (var message = new MailMessage("vvinf370@gmail.com", bicyceStudent.Student.Guardian.GuardianEmail))
                            {
                                message.Subject = "School Attendance    ";
                                message.Body = "Good day, Please not that your child did not attend school today. " +
                                    "Please contact the school Administrator should you have any queries. King regards, #Bikes4ERP" + DateTime.Now;
                                using (SmtpClient client = new SmtpClient
                                {
                                    EnableSsl = true,
                                    Host = "smtp.gmail.com",
                                    Port = 587,
                                    Credentials = new NetworkCredential("vvinf370@gmail.com", "Virtu@l370")
                                })
                                {
                                    client.Send(message);
                                }
                            }// using

                        }// if (log == null)

                    } //foreach (var bicycle in bicycles)

                }// if bicycles.Count > 0


            }// If current Day is a weekday

            //return dateTime;

            //throw new NotImplementedException();
        }

        public void ReturnNothing()
        {

        }
    }
}