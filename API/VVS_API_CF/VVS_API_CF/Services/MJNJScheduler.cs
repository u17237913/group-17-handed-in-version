﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;

namespace VVS_API_CF.Services
{
    public class MJNJScheduler
    {
        public static void Start()
        {
            //IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            ISchedulerFactory schedule = new StdSchedulerFactory();
            IScheduler scheduler = schedule.GetScheduler().GetAwaiter().GetResult();

            scheduler.Start();

            IJobDetail job = JobBuilder.Create<MaintenanceJobNotificationJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(21, 25))
                  )
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}