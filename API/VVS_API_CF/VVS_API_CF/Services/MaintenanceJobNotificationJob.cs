﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using VVS_API_CF.Models;
using System.Net.Mail;
using System.Net;

namespace VVS_API_CF.Services
{
    public class MaintenanceJobNotificationJob : IJob
    {
        private vvsContext db = new vvsContext();
        public async Task Execute(IJobExecutionContext context)
        {
            db.Configuration.ProxyCreationEnabled = false;

            DateTime TodayDate = DateTime.Now.Date;

            //string dateToday = dateTime.ToString("d");
            //DayOfWeek day = DateTime.Now.DayOfWeek;
            //string dayToday = day.ToString();

            // Get all Bicycles that are assigned to a student and have atleast one maintenance job
            //But this list does not check if the maintance job has already been completed or not
            var bicyleStudents = await db.BicycleStudent
                .Include(bs => bs.Bicycle)
                .Include(bs => bs.Student.Guardian)
                .Include(bs => bs.Bicycle.MaintenanceJobs)
                .Where(bs => bs.DateUnassigned == null && bs.Bicycle.MaintenanceJobs.Count > 0)
                .ToListAsync();

            //Loop through the Bicycle's (Inside Bicycle student) Maintenance Job List to 
            //Check if the status is Completed or not.
            foreach (var bs in bicyleStudents)
            {
                foreach (var mj in bs.Bicycle.MaintenanceJobs)
                {
                    //If the maintenance job status is complete, then remove it from the list
                    if (mj.MaintenanceJobStatusID == 2)
                    {
                        bs.Bicycle.MaintenanceJobs.Remove(mj);
                    }
                }
            }

            //loop through each bicyle student
            foreach (var bs in bicyleStudents)
            {
                //Loop through each bicycle maintenance job
                foreach (var mj in bs.Bicycle.MaintenanceJobs)
                {
                    //If the Scheduled Maintenance job is one week from today
                    if ((DbFunctions.TruncateTime(mj.DateScheduled) - TodayDate).Value.TotalDays == 7)
                    {
                        //Send the email
                        //Currently we send an email each time we enter the if statement
                        //We will later change it so that it only sends one emaail per bicycle
                        using (var message = new MailMessage("vvinf370@gmail.com", bs.Student.Guardian.GuardianEmail))
                        {
                            message.Subject = "Test";
                            message.Body = "Test at " + DateTime.Now;
                            using (SmtpClient client = new SmtpClient
                            {
                                EnableSsl = true,
                                Host = "smtp.gmail.com",
                                Port = 587,
                                Credentials = new NetworkCredential("vvinf370@gmail.com", "Virtu@l370")
                            })
                            {
                                client.Send(message);
                            }
                        }// using
                    }
                }
            }

        }
    }
}