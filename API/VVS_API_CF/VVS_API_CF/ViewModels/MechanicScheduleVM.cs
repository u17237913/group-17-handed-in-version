﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class MechanicScheduleVM
    {
        public MechanicScheduleVM() { }

        //Vars
        public int JobCode;
        public int BicycleCode;
        public string StudentName;
        public string JobType;
        public string StudentContact;
        public string GuardianContact;
        public /*DateTime*/ string DateScheduled;
        public string TimeScheduled;
    }
}