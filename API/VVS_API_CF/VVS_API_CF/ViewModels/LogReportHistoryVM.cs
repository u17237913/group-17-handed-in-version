﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class LogReportHistoryVM
    {
        public LogReportHistoryVM() { }

        public string TagSerialNumber;
        public /*int*/ string BicycleCode;
        public string StudentName;
        public string entryDate;
        public /*DateTime*/ string entryTime;
        public string School;
    }
}