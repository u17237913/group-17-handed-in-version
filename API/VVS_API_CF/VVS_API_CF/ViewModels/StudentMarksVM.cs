﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class StudentMarksVM
    {

        public StudentMarksVM () { }

        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public string StudentSurname { get; set; }


        public string MarkType1 { get; set; }
        public int MarkTypeID { get; set; } 

        public string Date { get; set; }
        public string Mark { get; set;  }
    }
}