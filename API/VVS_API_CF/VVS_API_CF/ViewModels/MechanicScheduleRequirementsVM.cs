﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class MechanicScheduleRequirementsVM
    {
        public MechanicScheduleRequirementsVM() { }

        //Vars
        public int JobCode;
        public int BicycleCode;
        public string StudentName;
        public string JobType;
        public /*DateTime*/ string DateScheduled;
    }
}