//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VVS_API_CF.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PartFault
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PartFault()
        {
            this.JobFaults = new HashSet<JobFault>();
        }
    
        public int PartFaultID { get; set; }
        public Nullable<int> PartID { get; set; }
        public Nullable<int> FaultTypeID { get; set; }
        public string FaultImage { get; set; }
    
        public virtual FaultType FaultType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobFault> JobFaults { get; set; }
        public virtual Part Part { get; set; }
    }
}
