﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using VVS_API_CF.Services;
using Hangfire;
using Hangfire.SqlServer;
using System.Collections.Generic;
using System.Diagnostics;
using VVS_API_CF.Models;
using VVS_API_CF.Controllers;
using System.Globalization;
using System.Linq;

[assembly: OwinStartup(typeof(VVS_API_CF.Startup))]

namespace VVS_API_CF
{
    public class Startup
    {
        private vvsContext db = new vvsContext();
        private IEnumerable<IDisposable> GetHangfireServers()
        {
            GlobalConfiguration.Configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage("Server=tcp:bikes4erp.database.windows.net,1433;Initial Catalog=bikes4erp;Persist Security Info=False;User ID=vvs;Password=Virtual370;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;", new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                });

            yield return new BackgroundJobServer();
        }

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.UseJwtBearerAuthentication(
                    new JwtBearerAuthenticationOptions
                    {
                        AuthenticationMode = AuthenticationMode.Active,
                        TokenValidationParameters = new TokenValidationParameters()
                        {
                            ValidateIssuer = true,
                            ValidateAudience = false,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = "http://mysite.com", //some string, normally web url,  
                            //ValidAudience = "http://mysite.com",
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("HansenTheAsianThePasswordThatCanNeverBeCrackedkilyhanybfjrqjkls"))
                        }
                    });

            EmailJobs email = new EmailJobs();
            app.UseHangfireAspNet(GetHangfireServers);
            app.UseHangfireDashboard();

            string StudentNotAtSchoolString = db.BusinessRule.Find(1).Value;

            //SHOULD WORK
            var SNASTH = Convert.ToInt32(StudentNotAtSchoolString.Substring(0, 2));
            var SNASTM = Convert.ToInt32(StudentNotAtSchoolString.Substring(3, 2));

            string BikeForTwoYearsString = db.BusinessRule.Find(7).Value;

            //SHOULD WORK
            var BFTYH = Convert.ToInt32(BikeForTwoYearsString.Substring(0, 2));
            var BFTYM = Convert.ToInt32(BikeForTwoYearsString.Substring(3, 2));

            RecurringJob.AddOrUpdate(() => email.StudentNotAtSchool(), Cron.Daily(SNASTH, SNASTM));
            RecurringJob.AddOrUpdate(() => email.BikeForTwoYears(), Cron.Daily(BFTYH, BFTYM));

            //JobScheduler.Start();
            }
    }
}
