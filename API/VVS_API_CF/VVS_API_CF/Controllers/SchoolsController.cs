﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;

namespace VVS_API_CF.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class SchoolsController : ApiController
    {
        private vvsContext db = new vvsContext();

        [System.Web.Http.Route("api/Schools/GetSchools")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetSchools()
        {
            //return db.Users;
            vvsContext db = new vvsContext(); 
            db.Configuration.ProxyCreationEnabled = false;
            return getSchoolsReturnList(db.School.Include( s => s.Village ).ToList());
        }
        private List<dynamic> getSchoolsReturnList(List<School> forClient)
        {
            List<dynamic> dynamicSchools = new List<dynamic>();
            foreach (School school in forClient)
            {
                dynamic dynamicSchool = new ExpandoObject();

                dynamicSchool.SchoolID = school.SchoolID;
                dynamicSchool.Village = school.Village.VillageName;
                dynamicSchool.VillageID = school.VillageID;
                dynamicSchool.SchoolTelephone = school.SchoolTelephone;
                dynamicSchool.SchoolEmail = school.SchoolEmail;
                dynamicSchool.SchoolName = school.SchoolName;

                dynamicSchools.Add(dynamicSchool);
            }

            return dynamicSchools;
        }

        // GET: api/Schools
        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route(Name = "GetSchool")]
        public IQueryable<School> GetSchool()
        {
            return db.School;
        }

        // GET: api/Schools/5
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> GetSchool(int id)
        {
            School school = await db.School.FindAsync(id);
            if (school == null)
            {
                return NotFound();
            }

            return Ok(school);
        }

        // PUT: api/Schools/5
        [System.Web.Http.Route("api/Schools/PutSchool")]
        [System.Web.Mvc.HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSchool(int id, School school)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != school.SchoolID)
            {
                return BadRequest();
            }

            db.Entry(school).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SchoolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Schools
        [System.Web.Http.Route("api/Schools/PostSchool")]
        [System.Web.Mvc.HttpPost]
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> PostSchool(School school)
        {
            //if (!modelstate.isvalid)
            //{
            //    return badrequest(modelstate);
            //}

            db.School.Add(school);
            await db.SaveChangesAsync();

            return CreatedAtRoute("GetSchool", new { id = school.SchoolID }, school);
        }

        // DELETE: api/Schools/5
        [System.Web.Http.Route("api/Schools/DeleteSchool")]
        [System.Web.Mvc.HttpDelete]
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> DeleteSchool(int id)
        {
            School school = await db.School.FindAsync(id);
            if (school == null)
            {
                return NotFound();
            }

            db.School.Remove(school);
            await db.SaveChangesAsync();

            return Ok(school);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SchoolExists(int id)
        {
            return db.School.Count(e => e.SchoolID == id) > 0;
     
        }


        [System.Web.Http.Route("api/Schools/GetLocations")]
        [System.Web.Mvc.HttpPost]

        public LocationViewModel GetLocations()
        {
            db.Configuration.ProxyCreationEnabled = false;

            LocationViewModel Locations = new LocationViewModel();
            Locations.Villages = db.Village.ToList();
            Locations.Countries = db.Country.ToList();
            Locations.Cities = db.City.ToList();
            Locations.Provinces = db.Province.ToList();
            Locations.Suburbs = db.Suburb.ToList();

            return Locations;
        }


        //[System.Web.Http.Route("api/Schools/GetVillages")]
        //[System.Web.Mvc.HttpGet]
        //// GET: api/Students
        //public List<dynamic> GetVillages()
        //{
        //    //return db.Users;
        //    vvsContext db = new vvsContext();
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return getVillagesReturnList(db.Village.ToList());
        //}
        //private List<dynamic> getVillagesReturnList(List<Village> forClient)
        //{
        //    List<dynamic> dynamicVillages = new List<dynamic>();
        //    foreach (Village village in forClient)
        //    {
        //        dynamic dynamicVillage = new ExpandoObject();
        //        dynamicVillage.VillageID = village.VillageID;
        //        dynamicVillage.SuburbID = village.SuburbID;
        //        dynamicVillage.VillageName = village.VillageName;



        //        dynamicVillages.Add(dynamicVillage);
        //    }

        //    return dynamicVillages;
        //}

        //[System.Web.Http.Route("api/Schools/GetProvinces")]
        //[System.Web.Mvc.HttpGet]
        //// GET: api/Students
        //public List<dynamic> GetProvinces()
        //{
        //    //return db.Users;
        //    vvsContext db = new vvsContext();
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return getProvincesReturnList(db.Province.ToList());
        //}
        //private List<dynamic> getProvincesReturnList(List<Province> forClient)
        //{
        //    List<dynamic> dynamicProvinces = new List<dynamic>();
        //    foreach (Province province in forClient)
        //    {
        //        dynamic dynamicProvince = new ExpandoObject();
        //        dynamicProvince.ProvinceID = province.ProvinceID;
        //        dynamicProvince.CountryID = province.CountryID;
        //        dynamicProvince.ProvinceName = province.ProvinceName;



        //        dynamicProvinces.Add(dynamicProvince);
        //    }

        //    return dynamicProvinces;
        //}

        //[System.Web.Http.Route("api/Schools/GetCountries")]
        //[System.Web.Mvc.HttpGet]
        //// GET: api/Students
        //public List<dynamic> GetCountries()
        //{
        //    //return db.Users;
        //    vvsContext db = new vvsContext();
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return getCountriesReturnList(db.Country.ToList());
        //}
        //private List<dynamic> getCountriesReturnList(List<Country> forClient)
        //{
        //    List<dynamic> dynamicCountries = new List<dynamic>();
        //    foreach (Country country in forClient)
        //    {
        //        dynamic dynamicCountry = new ExpandoObject();
        //        dynamicCountry.CountryID = country.CountryID;
        //        dynamicCountry.CountryName = country.CountryName;

        //        dynamicCountries.Add(dynamicCountry);
        //    }

        //    return dynamicCountries;
        //}
    }



}