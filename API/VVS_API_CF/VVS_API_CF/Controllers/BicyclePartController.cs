﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Cors;
using System.Web.Http.Description;
using System.Dynamic;
using System.Web.Mvc;
using VVS_API_CF.Models;
using VVS_API_CF.Services;
using System.Web;
using System.IO;
//using <namespace>.

namespace VVS_API_CF.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BicyclePartController : ApiController
    {

        //Globalize this guy
        private vvsContext db = new vvsContext();

        //----------------------------------------ALL Bicycle Part API END-POINTS BELOW----------------------------------------------

        //GET ---> BicycleParts
        //[System.Web.Http.Route("api/BicyclePart/GetAllParts")]
        //[System.Web.Mvc.HttpGet]
        //public List<Part> GetAllParts()
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return db.Part.Include(sec => sec.Section).Include(pt => pt.PartType).ToList();
        //}

        //GET ---> BicycleParts
        //[System.Web.Http.Route("api/BicyclePart/GetAllParts")]
        //[System.Web.Mvc.HttpPost]
        //public List<dynamic> GetAllParts()
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return getPartsReturnList(db.Part.Include(sec => sec.Section).Include(pt => pt.PartType).ToList());
        //}

        //private List<dynamic> getPartsReturnList(List<Part> forBicycle)
        //{
        //    List<dynamic> dynamicParts = new List<dynamic>();
        //    foreach (Part part in forBicycle)
        //    {
        //        dynamic dynamicPart = new ExpandoObject();

        //        dynamicPart.PartID = part.PartID;
        //        dynamicPart.PartName = part.PartName;
        //        dynamicPart.PartDescription = part.PartDescription;
        //        dynamicPart.SectionName = part.Section.SectionName;
        //        dynamicPart.PartType = part.PartType.PartType1;
        //        //dynamicPart.PartImage = part.PartImage;

        //        dynamicParts.Add(dynamicPart);
        //    }
        //    return dynamicParts;
        //}


        [System.Web.Http.Route("api/BicyclePart/GetAllParts")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllParts()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getPartsReturnList(db.Part.Include(sec => sec.Section).Include(pt => pt.PartType).ToList());
        }

        private List<dynamic> getPartsReturnList(List<Part> forBicycle)
        {
            List<dynamic> dynamicParts = new List<dynamic>();
            foreach (Part part in forBicycle)
            {
                dynamic dynamicPart = new ExpandoObject();

                dynamicPart.PartID = part.PartID;
                dynamicPart.PartName = part.PartName;
                dynamicPart.PartDescription = part.PartDescription;
                dynamicPart.Section = part.Section.SectionName;
                dynamicPart.PartType = part.PartType.PartType1;
                dynamicPart.SectionID = part.SectionID;
                dynamicPart.PartTypeID = part.PartTypeID;
                //dynamicPart.PartImage = part.PartImage;

                dynamicParts.Add(dynamicPart);
            }
            return dynamicParts;
        }

        //ADD ---> BicyclePart
        [System.Web.Http.Route("api/BicyclePart/AddPart")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddPart([FromBody] Part part)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Part.Add(part);
            db.SaveChanges();
            //return db.Part.Include(sec => sec.Section).Include(pt => pt.PartType).ToList();
            return getPartsReturnList(db.Part.Include(sec => sec.Section).Include(pt => pt.PartType).ToList());
        }

// add image file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/BicyclePart/UploadPartImage/{ID}")]
        [System.Web.Http.Route("api/BicyclePart/UploadPartImage")]
        [System.Web.Mvc.HttpPost]
        public bool UploadPartImage(/*int ID, */[FromBody] FileModel file)
        {
            //this is the static file path on the server, change it to a relative path on deployment
            //var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Images/";
            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Images/Parts/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                var _part = db.Part.OrderByDescending(sc => sc.PartID).FirstOrDefault();

                _part.PartImage = filePathName;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        // finished image file upload tanx ------------------------------------>---------------------------------->---------->


        // read image file uploaded plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/BicyclePart/GetPartImage/{ID}")]
        [System.Web.Http.Route("api/BicyclePart/GetPartImage")]
        [System.Web.Mvc.HttpGet]
        public FileModel GetPartImage(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
            FileModel EventFile = new FileModel();
                //retrieve the file path
                var _part = db.Part.Where(par => par.PartID == ID).FirstOrDefault();

                //get the file as a byte array
                Byte[] bytes = File.ReadAllBytes(_part.PartImage);

                //allocate variables to dynamic object
                EventFile.fileName = _part.PartImage;

                //convert byte array to base64
                EventFile.fileAsBase64 = Convert.ToBase64String(bytes);
                return EventFile;
            }
            catch (Exception)
            {
                return null;
                //throw;
            }
        }
        // finished image file read tanx ------------------------------------>---------------------------------->---------->


        //UPDATE ---> BicyclePart
        [System.Web.Http.Route("api/BicyclePart/UpdatePart")]
        [System.Web.Mvc.HttpPost]
        public bool UpdatePart([FromBody] Part _part)
        {
            using (vvsContext vvsSys = new vvsContext())
            {
                Part updatedPart = (from bp in vvsSys.Part
                                      where bp.PartID == _part.PartID
                                      select bp).FirstOrDefault();
                updatedPart.PartID = _part.PartID;
                updatedPart.PartName = _part.PartName;
                updatedPart.PartDescription = _part.PartDescription;
                updatedPart.SectionID = _part.SectionID;
                updatedPart.PartTypeID = _part.PartTypeID;
                //updatedPart.PartImage = _part.PartImage;
                vvsSys.SaveChanges();
            }
            return true;
        }

// update image file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/BicyclePart/UpdatePartImage/{ID}")]
        [System.Web.Http.Route("api/BicyclePart/UpdatePartImage")]
        [System.Web.Mvc.HttpPost]
        public bool UpdatePartImage(int ID, [FromBody] FileModel file)
        {
            //this is the static file path on the server, change it to a relative path on deployment
            // var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Images/";
            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Images/Parts/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                var _part = db.Part.Where(pr => pr.PartID == ID).FirstOrDefault();

                _part.PartImage = filePathName;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        // finished image file update tanx ------------------------------------>---------------------------------->---------->


        //DELETE ---> BicyclePart
        [System.Web.Http.Route("api/BicyclePart/DeletePart")]
        [System.Web.Mvc.HttpDelete]
        public void DeletePart(int id)
        {
            Part PartToDelete = (db.Part.Where(o => o.PartID == id)).FirstOrDefault();
            db.Part.Remove(PartToDelete);
            db.SaveChanges();
        }

        //Additional API Endpoints needed for editing a bicycle Part.
        
        //GET ---> PartType
        [System.Web.Http.Route("api/BicyclePart/GetAllPartTypes")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllPartTypes()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getPartTypesReturnList(db.PartType.ToList());
        }

        private List<dynamic> getPartTypesReturnList(List<PartType> forBicycle)
        {
            List<dynamic> dynamicParts = new List<dynamic>();
            foreach (PartType Ptype in forBicycle)
            {
                dynamic dynamicPtype = new ExpandoObject();

                dynamicPtype.PartTypeID = Ptype.PartTypeID;
                dynamicPtype.PartType = Ptype.PartType1;

                dynamicParts.Add(dynamicPtype);
            }
            return dynamicParts;
        }



        //----------------------------------------ALL Bicycle Section API END-POINTS BELOW----------------------------------------------

            //GET ---> BicycleSections
        [System.Web.Http.Route("api/BicyclePart/GetAllSections")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllSections()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getSectionsReturnList(db.Section.ToList());
        }

        private List<dynamic> getSectionsReturnList(List<Section> forBicycle)
        {
            List<dynamic> dynamicSections = new List<dynamic>();
            foreach (Section Section in forBicycle)
            {
                dynamic dynamicSection = new ExpandoObject();

                dynamicSection.SectionID = Section.SectionID;
                dynamicSection.SectionName = Section.SectionName;
                dynamicSection.SectionDescription = Section.SectionDescription;
                dynamicSection.SectionImage = Section.SectionImage;

                dynamicSections.Add(dynamicSection);
            }
            return dynamicSections;
        }

        //ADD ---> BicycleSection
        [System.Web.Http.Route("api/BicyclePart/AddSection")]
        [System.Web.Mvc.HttpPost]
        public int/*List<dynamic>*/ AddSection([FromBody] Section section)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Section.Add(section);
            db.SaveChanges();
            return section.SectionID/*getSectionsReturnList(db.Section.ToList())*/;
        }

        // add image file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/BicyclePart/UploadFile/{ID}")]
        [System.Web.Http.Route("api/BicyclePart/UploadFile")]
        [System.Web.Mvc.HttpPost]
        public bool UploadFile(/*int ID, */[FromBody] FileModel file)
        {
            //this is the static file path on the server, change it to a relative path on deployment
            //var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Images/";
            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Images/Sections/");

            //string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            //string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //string exeDir = System.IO.Path.GetDirectoryName(exePath);
            //DirectoryInfo binDir = System.IO.Directory.GetParent(exeDir);

                //var filepath = Path.GetFullPath("~/Resources/Images/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                //var _section = db.Section.Where(sc => sc.SectionID == ID).FirstOrDefault();
                //Get the last Section that was created
                var _section = db.Section.OrderByDescending(sc => sc.SectionID).FirstOrDefault();

                _section.SectionImage = filePathName;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        // finished image file upload tanx ------------------------------------>---------------------------------->---------->




        //BIG TIME ADD TEST - nah never mind baba, pane zvaitika zviri serious so

        //ADD ---> BicycleSection
        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Route("api/BicyclePart/AddSection")]
        //public HttpResponseMessage AddSection(/*[FromBody] Section _section*/)
        //{
        //    string imageName = null;
        //    var httpRequest = HttpContext.Current.Request;

        //    //Upload Image
        //    var postedFile = httpRequest.Files["Image"];
        //    //Create custom filename
        //    imageName = new String(Path.GetFileNameWithoutExtension(postedFile.FileName).Take(10).ToArray()).Replace(" ", "-");
        //    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(postedFile.FileName);

        //    var filepath = HttpContext.Current.Server.MapPath("~/Resources/Images/" + imageName);
        //    postedFile.SaveAs(filepath);

        //    //Save everything to DB
        //    using (db = new vvsContext())
        //    {
        //        Section section = new Section()
        //        {
        //            //SectionName = _section.SectionName,
        //SectionDescription = _section.SectionDescription,
        //SectionImage = imageName

        //            SectionName = httpRequest["SectionName"],
        //            SectionDescription = httpRequest["SectionDescription"],
        //            SectionImage = imageName
        //        };
        //        db.Section.Add(section);
        //        db.SaveChanges();
        //    }           
        //        return Request.CreateResponse(HttpStatusCode.Created);
        //}


        // read image file uploaded plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/BicyclePart/GetFile/{ID}")]
        [System.Web.Http.Route("api/BicyclePart/GetFile")]
        [System.Web.Mvc.HttpGet]
        public FileModel GetFile(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
            FileModel EventFile = new FileModel();
                //retrieve the file path
                var _section = db.Section.Where(sec => sec.SectionID == ID).FirstOrDefault();

                //get the file as a byte array
                Byte[] bytes = File.ReadAllBytes(_section.SectionImage);

                //allocate variables to dynamic object
                EventFile.fileName = _section.SectionImage;

                //convert byte array to base64
                EventFile.fileAsBase64 = Convert.ToBase64String(bytes);
                return EventFile;
            }
            catch (Exception)
            {
                return null;
                //throw;
            }
        }
        // finished image file read tanx ------------------------------------>---------------------------------->---------->



        //UPDATE ---> BicycleSection
        [System.Web.Http.Route("api/BicyclePart/UpdateSection")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateSection([FromBody] Section _section)
        {
            using (vvsContext vvsSys = new vvsContext())
            {
                Section updatedSection = (from bsec in vvsSys.Section
                                    where bsec.SectionID == _section.SectionID
                                    select bsec).FirstOrDefault();
                updatedSection.SectionID = _section.SectionID;
                updatedSection.SectionName = _section.SectionName;
                updatedSection.SectionDescription = _section.SectionDescription;
                updatedSection.SectionImage = _section.SectionImage;
                vvsSys.SaveChanges();
            }
            return true;
        }

        // update image file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/BicyclePart/UpdateFile/{ID}")]
        [System.Web.Http.Route("api/BicyclePart/UpdateFile")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateFile(int ID, [FromBody] FileModel file)
        {
            //this is the static file path on the server, change it to a relative path on deployment
            // var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Images/";

            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Images/Sections/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                var _section = db.Section.Where(sc => sc.SectionID == ID).FirstOrDefault();
                //var _section = db.Section.OrderByDescending(sc => sc.SectionID).FirstOrDefault();

                _section.SectionImage = filePathName;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        // finished image file update tanx ------------------------------------>---------------------------------->---------->

        //DELETE ---> BicycleSection
        [System.Web.Http.Route("api/BicyclePart/DeleteSection")]
        [System.Web.Mvc.HttpDelete]
        public void DeleteSection(int id)
        {
            Section SectionToDelete = (db.Section.Where(o => o.SectionID == id)).FirstOrDefault();
            db.Section.Remove(SectionToDelete);
            db.SaveChanges();
        }
//ReportRepairJob---------------------------------------------------------

        [System.Web.Http.Route("api/BicyclePart/GetSectionPart")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetSectionPart(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getPartList(db.Part.Where(z => z.SectionID == id).ToList());
        }

        private List<dynamic> getPartList(List<Part> forBicycle)
        {
            List<dynamic> dynamicSections = new List<dynamic>();
            foreach (Part part in forBicycle)
            {
                dynamic dynamicSection = new ExpandoObject();

                dynamicSection.PartID = part.PartID;
                dynamicSection.PartName = part.PartName;
                dynamicSection.PartDescription = part.PartDescription;
                dynamicSection.PartImage = part.PartImage;

                dynamicSections.Add(dynamicSection);
            }
            return dynamicSections;
        }

        [System.Web.Http.Route("api/BicyclePart/getRepairFault")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> getRepairFault(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            //List<FaultType> faultlist = new List<FaultType>();
            List<PartFault> PartFaultList = new List<PartFault>();

            var RepairFaultdetails = db.PartFault.Where(rf => rf.PartID == id).ToList();

            foreach (var item in RepairFaultdetails)
            {
                //var faulttype = db.FaultType.Include(tt => tt.PartFaults).Where(rr => rr.FaultTypeID == item.FaultTypeID).FirstOrDefault();
                var faulttype = db.PartFault.Include(pf => pf.FaultType).Where(pf => pf.FaultTypeID == item.FaultTypeID).FirstOrDefault();
                //faultlist.Add(faulttype);
                PartFaultList.Add(faulttype);
            }
            return getFaultList(PartFaultList);
        }

        private List<dynamic> getFaultList(List<PartFault> forPart)
        {
            List<dynamic> dynamicSections = new List<dynamic>();
            foreach (PartFault faultType in forPart)
            {
                dynamic dynamicSection = new ExpandoObject();

                //dynamicSection.FaultTypeID = faultType.FaultTypeID;
                //dynamicSection.FaultType = faultType.FaultType1;
                //dynamicSection.PartNeeded = faultType.PartNeeded;

                dynamicSection.FaultTypeID = faultType.FaultTypeID;
                dynamicSection.FaultType = faultType.FaultType.FaultType1;
                dynamicSection.PartNeeded = faultType.FaultType.PartNeeded;
                dynamicSection.PartFaultID = faultType.PartFaultID;

                dynamicSections.Add(dynamicSection);
            }
            return dynamicSections;
        }

        [System.Web.Http.Route("api/BicyclePart/FindPart")]
        [System.Web.Http.AllowAnonymous]
        public object FindPart(BicyclePart bicyclePart)
        {
            int partID = 0;
            partID = Convert.ToInt32(bicyclePart.PartID);
            //partfound kinda useless doe
            bool partfound = false;

            dynamic toReturn = new ExpandoObject();
           PartFault mypart = db.PartFault.Where(x => x.PartID == partID).FirstOrDefault();

            if (mypart == null)
            {
                partfound = false;
                toReturn.NotFound = "NotFound";
                return toReturn;
            }
            else
            {

                partfound = true;
                toReturn.Found = "Found";
                return toReturn;
            }

        }
    }
}