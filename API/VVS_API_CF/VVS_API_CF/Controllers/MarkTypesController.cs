﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    public class MarkTypesController : ApiController
    {
        private vvsContext db = new vvsContext();

//----------------------------------------ALL Mark Types API END-POINTS BELOW----------------------------------------------

        //GET ---> Mark Types
        [System.Web.Http.Route("api/MarkTypes/GetAllMarkTypes")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllMarkTypes()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getMarkTypesReturnList(db.MarkType.ToList());
        }

        private List<dynamic> getMarkTypesReturnList(List<MarkType> forMark)
        {
            List<dynamic> dynamicMarkTypes = new List<dynamic>();
            foreach (MarkType marktype in forMark)
            {
                dynamic dynamicMarkType = new ExpandoObject();

                dynamicMarkType.MarkTypeID = marktype.MarkTypeID;
                dynamicMarkType.MarkType1 = marktype.MarkType1;

                dynamicMarkTypes.Add(dynamicMarkType);
            }
            return dynamicMarkTypes;
        }

        //ADD ---> Mark Type
        [System.Web.Http.Route("api/MarkTypes/AddMarkType")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddMarkType([FromBody] MarkType marktype)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.MarkType.Add(marktype);
            db.SaveChanges();

            return getMarkTypesReturnList(db.MarkType.ToList());
        }

        //UPDATE ---> Mark Type
        [System.Web.Http.Route("api/MarkTypes/UpdateMarkType")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateMarkType([FromBody] MarkType _marktype)
        {
            using (vvsContext vvsSys = new vvsContext())
            {
                MarkType updatedMarkType = (from mt in vvsSys.MarkType
                                            where mt.MarkTypeID == _marktype.MarkTypeID
                                            select mt)
                                            .FirstOrDefault();
 
                updatedMarkType.MarkTypeID = _marktype.MarkTypeID;
                updatedMarkType.MarkType1 = _marktype.MarkType1;

                vvsSys.SaveChanges();
            }
            return true;
        }

        //DELETE ---> BicyclePart
        [System.Web.Http.Route("api/MarkTypes/DeleteMarkType")]
        [System.Web.Mvc.HttpDelete]
        public void DeleteMarkType(int id)
        {
            MarkType MarkTypeToDelete = (db.MarkType
                                        .Where(o => o.MarkTypeID == id))
                                        .FirstOrDefault();

            db.MarkType.Remove(MarkTypeToDelete);
            db.SaveChanges();
        }



// --- SCAFFOLDED APPROACH BELOW IF NEEDED...



        /*        // GET: api/MarkTypes
                public IQueryable<MarkType> GetMarkType()
                {
                    return db.MarkType;
                }

                // GET: api/MarkTypes/5
                [ResponseType(typeof(MarkType))]
                public async Task<IHttpActionResult> GetMarkType(int id)
                {
                    MarkType markType = await db.MarkType.FindAsync(id);
                    if (markType == null)
                    {
                        return NotFound();
                    }

                    return Ok(markType);
                }

                // PUT: api/MarkTypes/5
                [ResponseType(typeof(void))]
                public async Task<IHttpActionResult> PutMarkType(int id, MarkType markType)
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }

                    if (id != markType.MarkTypeID)
                    {
                        return BadRequest();
                    }

                    db.Entry(markType).State = EntityState.Modified;

                    try
                    {
                        await db.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!MarkTypeExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return StatusCode(HttpStatusCode.NoContent);
                }

                // POST: api/MarkTypes
                [ResponseType(typeof(MarkType))]
                public async Task<IHttpActionResult> PostMarkType(MarkType markType)
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }

                    db.MarkType.Add(markType);
                    await db.SaveChangesAsync();

                    return CreatedAtRoute("DefaultApi", new { id = markType.MarkTypeID }, markType);
                }

                // DELETE: api/MarkTypes/5
                [ResponseType(typeof(MarkType))]
                public async Task<IHttpActionResult> DeleteMarkType(int id)
                {
                    MarkType markType = await db.MarkType.FindAsync(id);
                    if (markType == null)
                    {
                        return NotFound();
                    }

                    db.MarkType.Remove(markType);
                    await db.SaveChangesAsync();

                    return Ok(markType);
                }

                protected override void Dispose(bool disposing)
                {
                    if (disposing)
                    {
                        db.Dispose();
                    }
                    base.Dispose(disposing);
                }

                private bool MarkTypeExists(int id)
                {
                    return db.MarkType.Count(e => e.MarkTypeID == id) > 0;
                }
        */
    }
}