﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using System.Security.Claims;

namespace VVS_API_CF.Controllers
{
    [System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]

    public class BicyclesController : ApiController
    {
        private vvsContext db = new vvsContext();

        [System.Web.Http.Route("api/Bicycles/GetBicycles")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public List<dynamic> GetBicycles()
        {
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = Convert.ToInt32(claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value);

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            var SchoolID = db.AdminSchool.Where(AS => AS.UserID == UserID).FirstOrDefault().SchoolID;

            return getBicyclesReturnList(
                db.Bicycle
                .Include(c => c.BicycleBrand)
                .Include(c => c.BicycleStatu)
                .ToList()
                );

        }
        private List<dynamic> getBicyclesReturnList(List<Bicycle> forClient)
        {
            List<dynamic> dynamicBicycles = new List<dynamic>();
            foreach (Bicycle bicycle in forClient)


            {
                dynamic dynamicBicycle = new ExpandoObject();
                dynamicBicycle.BicycleID = bicycle.BicycleID;
                dynamicBicycle.BicyleCode = bicycle.BicyleCode;
                dynamicBicycle.BicycleBrandID = bicycle.BicycleBrandID;
                dynamicBicycle.BicycleBrandName = db.BicycleBrand.Where(xx => xx.BicycleBrandID == bicycle.BicycleBrandID).Select(y => y.BicycleBrandName).FirstOrDefault();
                dynamicBicycle.BicycleStatusID = bicycle.BicycleStatusID;
                dynamicBicycle.BicycleStatus = db.BicycleStatus.Where(xx => xx.BicycleStatusID == bicycle.BicycleStatusID).Select(x => x.BicycleStatus).FirstOrDefault();
                dynamicBicycle.DateAdded = bicycle.DateAdded.Value.ToString("yyyy-MM-dd");
                
    

                //dynamicStudent.BicycleCode =  db.BicycleStudent.Include(bs => bs.Bicycle).Where(bs => bs.StudentID == student.StudentID).FirstOrDefault().Bicycle.BicyleCode.GetValueOrDefault();

                dynamicBicycles.Add(dynamicBicycle);
            }

            return dynamicBicycles;
        }
        [System.Web.Http.Route("api/Bicycles/GetBicycleBrands")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetBicycleBrands()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getBicycleBrandsReturnList(db.BicycleBrand.ToList());

        }
        private List<dynamic> getBicycleBrandsReturnList(List<BicycleBrand> forClient)
        {
            List<dynamic> dynamicBicycleBrands = new List<dynamic>();
            foreach (BicycleBrand bicyclebrand in forClient)


            {
                dynamic dynamicBicycleBrand = new ExpandoObject();
                dynamicBicycleBrand.BicycleBrandID = bicyclebrand.BicycleBrandID;
                dynamicBicycleBrand.BicycleBrandName = bicyclebrand.BicycleBrandName;



                // dynamicStudent.BicycleCode =  db.BicycleStudent.Include(bs => bs.Bicycle).Where(bs => bs.StudentID == student.StudentID).FirstOrDefault().Bicycle.BicyleCode.GetValueOrDefault();

                dynamicBicycleBrands.Add(dynamicBicycleBrand);
            }

            return dynamicBicycleBrands;
        }

        [System.Web.Http.Route("api/Bicycles/GetBicycleStatus")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetBicycleStatus()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getBicycleStatusReturnList(db.BicycleStatus.ToList());

        }
        private List<dynamic> getBicycleStatusReturnList(List<BicycleStatu> forClient)
        {
            List<dynamic> dynamicStatus = new List<dynamic>();
            foreach (BicycleStatu bicycleStatu in forClient)


            {
                dynamic dynamicBicycleStatu = new ExpandoObject();
                dynamicBicycleStatu.BicycleStatusID = bicycleStatu.BicycleStatusID;
                dynamicBicycleStatu.BicycleStatus = bicycleStatu.BicycleStatus;



                // dynamicStudent.BicycleCode =  db.BicycleStudent.Include(bs => bs.Bicycle).Where(bs => bs.StudentID == student.StudentID).FirstOrDefault().Bicycle.BicyleCode.GetValueOrDefault();

                dynamicStatus.Add(dynamicBicycleStatu);
            }

            return dynamicStatus;
        }

        // GET: api/Bicycles
        public IQueryable<Bicycle> GetBicycle()
        {
            return db.Bicycle;
        }

        // GET: api/Bicycles/5
        [ResponseType(typeof(Bicycle))]
        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route(Name ="GetBicycle")]
        public async Task<IHttpActionResult> GetBicycle(int id)
        {
            Bicycle bicycle = await db.Bicycle.FindAsync(id);
            if (bicycle == null)
            {
                return NotFound();
            }

            return Ok(bicycle);
        }

        [System.Web.Http.Route("api/Bicycles/GetBicycleAssign")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public List<BicycleStudentViewModel> GetBicycleAssign()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;

            List<BicycleStudentViewModel> BicycleStudentListVM = new List<BicycleStudentViewModel>();

            var BicycleStudentList = db.BicycleStudent
                .Include(bs => bs.Bicycle)
                .Include(bs => bs.Bicycle.BicycleStatu)
                .Include(bs => bs.Student)
                
                .Where(bs => bs.Bicycle.BicycleStatu.BicycleStatus != Bicycle.BicycleStatus.Broken.ToString())
                .OrderByDescending(bs => bs.BicycleID)
                .ToList();

            var BicycleList = db.Bicycle
                .Include(b => b.BicycleStatu)
                .Where(b => b.BicycleStatu.BicycleStatus != Bicycle.BicycleStatus.Broken.ToString()).ToList();

            //var StudentList = db.Student.ToList();

            for (int i = 0; i < BicycleList.Count(); i++)
            {
                

                //Get the bicycleID from the List
                var CurrentBicycleID = BicycleList[i].BicycleID;

                //Get the Current Bicycle from the Bicycle List
                var CurrentBicycle = BicycleList.Find(bl => bl.BicycleID == CurrentBicycleID);

                //Look for the bicycle in the BicycleStudent Table
                //var CurrentBS = BicycleStudentList.Find(bsl => bsl.BicycleID == CurrentBicycleID);
                var CurrentBS = BicycleStudentList
                    .Where(bsl => bsl.BicycleID == CurrentBicycleID)
                    .FirstOrDefault();

             

                //Get the current Status of the bicycle
                //var CurrentBicyleStatus = db.BicycleStatus.Find(CurrentBicycleID);

                //If CurrentBS has a value in it, then populate the view model with the appropriate data
                if (CurrentBS != null && CurrentBS.DateUnassigned == null)
                {
                    BicycleStudentViewModel bicycleStudentViewModel = new BicycleStudentViewModel();
                    bicycleStudentViewModel.BicycleCode = (int)CurrentBS.Bicycle.BicyleCode;
                    bicycleStudentViewModel.StudentName = CurrentBS.Student.StudentName;
                    bicycleStudentViewModel.StudentSurname = CurrentBS.Student.StudentSurname;
                    bicycleStudentViewModel.BicycleStatus = CurrentBS.Bicycle.BicycleStatu.BicycleStatus;
                    bicycleStudentViewModel.DateAssigned = CurrentBS.DateAssigned.Value.ToString("yyyy-MM-dd");

                    BicycleStudentListVM.Add(bicycleStudentViewModel);
                }
                //If CurrentBS does not have a value in it, then populate the VM with data from the Bicycle List without student Data
                else
                {
                    BicycleStudentViewModel bicycleStudentViewModel = new BicycleStudentViewModel();
                    bicycleStudentViewModel.BicycleCode = (int)CurrentBicycle.BicyleCode.GetValueOrDefault();
                    bicycleStudentViewModel.StudentName = "";
                    bicycleStudentViewModel.StudentSurname = "";
                    bicycleStudentViewModel.BicycleStatus = CurrentBicycle.BicycleStatu.BicycleStatus;

                    BicycleStudentListVM.Add(bicycleStudentViewModel);
                }

            }

            return BicycleStudentListVM;

        }


        // PUT: api/Bicycles/5
        [System.Web.Http.Route("api/Bicycles/PutBicycle")]
        [System.Web.Mvc.HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBicycle(int id, Bicycle bicycle)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bicycle.BicycleID)
            {
                return BadRequest();
            }

            if (bicycle.BicycleStatusID == 3)
            {
                bicycle.TagID = null;

                var bicycleStudent = db.BicycleStudent.Where(b => b.BicycleID == bicycle.BicycleID).FirstOrDefault();
                if (bicycleStudent != null)
                {
                    bicycleStudent.DateUnassigned = DateTime.Now;
                    db.Entry(bicycleStudent).State = EntityState.Modified;
                }

            }

            db.Entry(bicycle).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BicycleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [System.Web.Http.Route("api/Bicycles/PostBicycle")]
        [System.Web.Mvc.HttpPost]

        // POST: api/Bicycles
        [ResponseType(typeof(Bicycle))]
        public async Task<IHttpActionResult> PostBicycle(Bicycle bicycle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var CheckBicycle = await db.Bicycle.Where(b => b.BicyleCode == bicycle.BicyleCode).FirstOrDefaultAsync();

            if (CheckBicycle == null)
            {
                bicycle.BicycleStatusID = 2;
                bicycle.DateAdded = DateTime.Now;


                db.Bicycle.Add(bicycle);
                await db.SaveChangesAsync();

                return CreatedAtRoute("GetBicycle", new { id = bicycle.BicycleID }, bicycle);
            }
            else
            {
                return Conflict();
            }

        }


        [System.Web.Http.Route("api/Bicycles/UnassignBicycle")]
        [System.Web.Http.HttpGet]
        public async Task<IHttpActionResult> UnassignBicycle(int BicycleCode)
        {
            Bicycle bicycle = await db.Bicycle.Where(b => b.BicyleCode == BicycleCode).FirstOrDefaultAsync();

            bicycle.BicycleStatusID = 2;

            var bicycleStudent = await db.BicycleStudent.Where(bs => bs.BicycleID == bicycle.BicycleID).FirstOrDefaultAsync();

            //Setting a date unassigned for the bicycle is how we unassign a bicycle from a student
            bicycleStudent.DateUnassigned = DateTime.Now;

            //db.Bicycle.Add(bicycle);
            db.Entry(bicycle).State = EntityState.Modified;

            await db.SaveChangesAsync();

            db.BicycleStudent.Add(bicycleStudent);

            await db.SaveChangesAsync();

            return Ok();
        }

        [System.Web.Http.Route("api/Bicycles/AssignBicycle")]
        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> AssignBicycle(int BicycleCode , int StudentID)
        {
            Bicycle bicycle = await db.Bicycle.Where(b => b.BicyleCode == BicycleCode).FirstOrDefaultAsync();

            //var bicycleStudent = await db.BicycleStudent.Where(bs => bs.BicycleID == bicycle.BicycleID).FirstOrDefaultAsync();

            var bicycleStudent = new BicycleStudent();
            bicycleStudent.BicycleID = bicycle.BicycleID;
            bicycleStudent.StudentID = StudentID;
            bicycleStudent.DateAssigned = DateTime.Now;

            db.BicycleStudent.Add(bicycleStudent);
            await db.SaveChangesAsync();

            bicycle.BicycleStatusID = 1;

            db.Entry(bicycle).State = EntityState.Modified;
            await db.SaveChangesAsync();

            this.ScheduleMaintenance(bicycleStudent);

            return Ok();
        }

        [System.Web.Http.Route("api/Bicycles/DeleteBicycle")]
        [System.Web.Mvc.HttpDelete]
        // DELETE: api/Bicycles/5
        [ResponseType(typeof(Bicycle))]
        public async Task<IHttpActionResult> DeleteBicycle(int id)
        {
            Bicycle bicycle = await db.Bicycle.FindAsync(id);
            if (bicycle == null)
            {
                return NotFound();
            }

            db.Bicycle.Remove(bicycle);
            await db.SaveChangesAsync();

            return Ok(bicycle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BicycleExists(int id)
        {
            return db.Bicycle.Count(e => e.BicycleID == id) > 0;
        }

        private void ScheduleMaintenance(BicycleStudent bicycleStudent){

            var school = db.Student.Where(y => y.StudentID == bicycleStudent.StudentID).Select(x => x.SchoolID).FirstOrDefault();
            var mechanicSchool = db.MechanicSchool.Where(x => x.SchoolID == school).FirstOrDefault();
            var mechanicWorkday = db.WorkDay.Where(x => x.WorkDayID == mechanicSchool.WorkDayID).Select(y => y.WorkDay1).FirstOrDefault();

            var allTypes = db.MaintenanceType.ToList();

            DateTime date = DateTime.Now.Date;

            for (int i = 0; i < 7; i++)
            {
                DateTime testDate = date.AddDays(i);
                if (testDate.DayOfWeek.ToString() == mechanicWorkday)
                {
                    date = testDate;
                    break;
                }
            }

            MaintenanceJob maintenanceJob = new MaintenanceJob();
            maintenanceJob.BicycleID = bicycleStudent.BicycleID;
            maintenanceJob.MaintenanceJobStatusID = 1;
            maintenanceJob.DateScheduled = date;
            maintenanceJob.Assigned = true;
            maintenanceJob.CheckInID = ScheduleCheckIn(maintenanceJob);
            db.MaintenanceJob.Add(maintenanceJob);
            db.SaveChanges();

            foreach (var type in allTypes)
            {
                MaintenanceList newSheduledList = new MaintenanceList();
                newSheduledList.MaintenanceListStatusID = 3;
                newSheduledList.MaintenanceJobID = maintenanceJob.MaintenanceJobID;
                newSheduledList.MaintenanceTypeID = type.MaintenanceTypeID;
                db.MaintenanceList.Add(newSheduledList);
                db.SaveChanges();
            }
          
        }

        public int ScheduleCheckIn(MaintenanceJob maintenanceJob)
        {
            var student = db.BicycleStudent.Where(x => x.BicycleID == maintenanceJob.BicycleID && x.DateUnassigned == null).FirstOrDefault();
            var school = db.Student.Where(y => y.StudentID == student.StudentID).Select(x => x.SchoolID).FirstOrDefault();
            var mechanicSchool = db.MechanicSchool.Where(x => x.SchoolID == school).FirstOrDefault();

            CheckIn dynamicCheckIn = new CheckIn();
            dynamicCheckIn.JobTypeID = 2;
            dynamicCheckIn.CheckInStatusID = 1;
            dynamicCheckIn.CheckInDate = null;
            dynamicCheckIn.MechanicSchoolID = mechanicSchool.MechanicSchoolID;

            db.CheckIn.Add(dynamicCheckIn);
            db.SaveChanges();

            try
            {
                var studentNumberToSend = db.BicycleStudent.Include(ee => ee.Bicycle).Include(ee => ee.Student).Where(ee => ee.BicycleID == maintenanceJob.BicycleID).Select(ee => ee.Student.PhoneNumber).FirstOrDefault();
                var studentNameToSend = db.BicycleStudent.Include(ee => ee.Bicycle).Include(ee => ee.Student).Where(ee => ee.BicycleID == maintenanceJob.BicycleID).Select(ee => ee.Student.StudentName + " " + ee.Student.StudentSurname).FirstOrDefault();
                var studbikecode = db.Bicycle.Where(bc => bc.BicycleID == maintenanceJob.BicycleID).Select(bc => bc.BicyleCode).FirstOrDefault();

                // Find your Account Sid and Token at twilio.com/console
                // DANGER! This is insecure. See http://twil.io/secure
                const string accountSid = "AC0d9e20e4cc743b4494969bfcd9613f85";
                const string authToken = "47a721b61b23a3ace55346b7feef8715";


                string manipNum, StudCell;

                if (studentNumberToSend.Substring(0, 1) == "0")
                {
                    //removing the 0 in 0XXXXXXXXX to make it +27XXXXXXXXX
                    manipNum = studentNumberToSend.Remove(0, 1);
                    StudCell = "+27" + manipNum;
                }
                else if ((studentNumberToSend.Substring(0, 3) == "+27"))
                {
                    manipNum = studentNumberToSend;
                    StudCell = manipNum;
                }
                else
                {
                    manipNum = studentNumberToSend;
                    StudCell = "+27" + manipNum;
                }

                TwilioClient.Init(accountSid, authToken);

                dynamic textval = new ExpandoObject();
                textval.txt = "Hello " + studentNameToSend +
                                " You have a maintenance job scheduled for your bicycle " + studbikecode +
                                " scheduled for " + maintenanceJob.DateScheduled +
                                ".\nPlease bring the bicycle on this day to be maintained\n -#Bikes4ERP";

                var message = MessageResource.Create(
                    from: new Twilio.Types.PhoneNumber("+12056229145"),
                    body: textval.txt,
                    to: new Twilio.Types.PhoneNumber(StudCell)
                );
                /*("+27670414551")
                  with Code: " + maintenanceJob.Bicycle.BicyleCode*/
                Console.WriteLine(message.Sid);
            }
            catch (Exception err)
            {
                Console.WriteLine(err);
            }

            return dynamicCheckIn.CheckInID;

        }

    }
}