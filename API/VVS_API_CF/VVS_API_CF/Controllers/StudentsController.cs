﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;

namespace VVS_API_CF.Controllers
{

    [System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StudentsController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/Students
       

        [System.Web.Http.Route("api/Students/GetStudents")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public List<dynamic> GetStudents()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = Convert.ToInt32(claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value);

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            var SchoolID = db.AdminSchool.Where(AS => AS.UserID == UserID).FirstOrDefault().SchoolID;

            return getStudentsReturnList(db.Student.Include( b => b.BicycleStudents).Include( b => b.Guardian ).Where( s => s.SchoolID == SchoolID ).ToList());
           
        }
        private List<dynamic> getStudentsReturnList(List<Student> forClient)
        {
            List<dynamic> dynamicStudents = new List<dynamic>();
            foreach (Student student in forClient)

         
            {
                dynamic dynamicStudent = new ExpandoObject();
                dynamicStudent.StudentID = student.StudentID;
                dynamicStudent.VillageID = student.VillageID;
                dynamicStudent.SchoolID = student.SchoolID;
                dynamicStudent.GuardianID = student.GuardianID;
                dynamicStudent.StudentDoB = student.StudentDOB.Value.ToString("yyyy-MM-dd");
                dynamicStudent.Grade = student.Grade;
                dynamicStudent.PhoneNumber = student.PhoneNumber;
                dynamicStudent.StudentName = student.StudentName;
                dynamicStudent.StudentSurname = student.StudentSurname;
                dynamicStudent.Guardian = student.Guardian;
               // dynamicStudent.Bike = db.BicycleStudent.Include(ss => ss.Bicycle).Where(bc => bc.StudentID == student.StudentID).FirstOrDefault().S
               dynamicStudent.BicycleCode =  db.BicycleStudent.Include(bs => bs.Bicycle).Where(bs => bs.StudentID == student.StudentID).Select(xx => xx.Bicycle.BicyleCode).FirstOrDefault();

                // var student = db.BicycleStudent.Include(bs => bs.Student).Where(bs => bs.BicycleID == bike.BicycleID).FirstOrDefault().Student;

                dynamicStudents.Add(dynamicStudent);
            }

            return dynamicStudents;
        }

        //[System.Web.Http.Route("api/Students/GetStuGua")]
        //[System.Web.Mvc.HttpGet]
        //public List<dynamic> GetStuGus()
        //{
        //    //return db.Users;
        //    vvsContext db = new vvsContext();
        //    db.Configuration.ProxyCreationEnabled = false;
        //    // return getStudentsandGuardiansReturnList(db.Student.Include( c => c.BicycleStudents).ToList());
        //    return getStudentsReturnList(db.Student.Include(b => b.BicycleStudents).ToList());


        //}
        //private List<dynamic> getStudentsandGuardiansReturnList(List<StuGuaViewModel> forClient)
        //{
        //    List<dynamic> dynamicStuGuas = new List<dynamic>();
        //    foreach (StuGuaViewModel stuGuaViewModel in forClient)
        //    {
        //        dynamic dynamicStuGua = new ExpandoObject();
        //        dynamicStuGua.StudentID = stuGuaViewModel.Students.StudentID;
        //        dynamicStuGua.VillageID = stuGuaViewModel.Students.VillageID;
        //        dynamicStuGua.SchoolID = stuGuaViewModel.Students.SchoolID;
        //        dynamicStuGua.GuardianID = stuGuaViewModel.Students.GuardianID;
        //        dynamicStuGua.StudentDoB = stuGuaViewModel.Students.StudentDOB;
        //        dynamicStuGua.Grade = stuGuaViewModel.Students.Grade;
        //        dynamicStuGua.PhoneNumber = stuGuaViewModel.Students.PhoneNumber;
        //        dynamicStuGua.StudentName = stuGuaViewModel.Students.StudentName;
        //        dynamicStuGua.StudentSurname = stuGuaViewModel.Students.StudentSurname;
        //        //dynamicStuGua.GuardianName = db.Guardian.Include(xx => xx.Students).Where(xx => xx.GuardianID == stuGuaViewModel.Students.GuardianID).FirstOrDefault().GuardianName;
        //        //dynamicStuGua.GuardianEmail = db.Guardian.Include(xx => xx.Students).Where(xx => xx.GuardianID == stuGuaViewModel.Students.GuardianID).FirstOrDefault().GuardianEmail;
        //        //dynamicStuGua.GuardianEmail = db.Guardian.Include(xx => xx.Students).Where(xx => xx.GuardianID == stuGuaViewModel.Students.GuardianID).FirstOrDefault().GuardianPhone;



        //        // dynamicStudent.BicycleCode =  db.BicycleStudent.Include(bs => bs.Bicycle).Where(bs => bs.StudentID == student.StudentID).FirstOrDefault().Bicycle.BicyleCode.GetValueOrDefault();

        //        dynamicStuGuas.Add(dynamicStuGua);
        //    }

        //    return dynamicStuGuas;
        //}


        //The purpose of this endpoint is to return a list of students
        //specific to the School Administrator that is logged in at the time
        //The function should also returnb a list of students that do not have a bicycle.
        [System.Web.Http.Route("api/Students/GetStudentList")]
        [System.Web.Mvc.HttpGet]
        [ResponseType(typeof(Bicycle))]
        public IHttpActionResult GetStudentList()
        //public List<Student> GetStudentList()
        {
            db.Configuration.ProxyCreationEnabled = false;

            // Get the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value;

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            var SchoolAdministrator = db.AdminSchool.Where(sa => sa.UserID.ToString() == UserID).FirstOrDefault();

            var StudentsWithoutBicycle = new List<Student>();

            if (SchoolAdministrator != null)
            {
                var StudentList =  db.Student.Where(s => s.SchoolID == SchoolAdministrator.SchoolID).ToList();

                foreach (var student in StudentList)
                {
                    student.StudentFullName = student.StudentName + " " + student.StudentSurname;
                    //var LastGuardian = await db.Guardian.OrderByDescending(g => g.GuardianID).FirstOrDefaultAsync();

                    //Get a list of all the bicycle student entries where the schoolID
                    //is the same as the School the School administrator is linked to
                    var bicycleStudent = db.BicycleStudent
                        .Where(bs => bs.StudentID == student.StudentID)
                        //.OrderByDescending(bs => bs.BicycleStudentID)
                        .ToList();

                    //Used when looping through the bicycle student list
                    var DoesStudentHaveBike = false;

                    //If bicycle student list has items in it else just add the student to the list
                    // of students without a bicycle
                    if (bicycleStudent.Count > 0)
                    {
                        //Loop through each bicycle student object and check if the student has been unassigned from the 
                        //bicycle or not, change DoesStudentHaveBike accordingly.
                        foreach (var bs in bicycleStudent)
                        {
                            //If the student has not been unassigned from the bicycle then 
                            //DoesStudentHaveBike should be set to true
                            if (bs.DateUnassigned == null)
                            {
                                DoesStudentHaveBike = true;
                            }
                            else
                            {
                                DoesStudentHaveBike = false;
                            }
                        }

                        //After going through the loop, if student does not have a bike
                        if (DoesStudentHaveBike == false)
                        {
                            
                            StudentsWithoutBicycle.Add(student);
                        }

                    }
                    else
                    {
                        StudentsWithoutBicycle.Add(student);
                    }

                }

                //If there are students without a bike then return the list
                //Otherwise return that all students have a bicycle
                if (StudentsWithoutBicycle.Count > 0)
                {
                    return Ok(StudentsWithoutBicycle);
                }
                else
                {
                    //return NotFound("All students have bicycles");
                    return NotFound();
                }

            }
            else
            {
                //return HttpStatusCode.NotFound;
                //throw new HttpResponseException(HttpStatusCode.NotFound);
                return NotFound();
            }

        }

        [System.Web.Http.Route("api/Students/GetStuGua")]
        [System.Web.Mvc.HttpGet]

        public StuGuaViewModel GetStuGua(int id)

        {
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;

            StuGuaViewModel StuGua = new StuGuaViewModel();


            StuGua.Students = db.Student.Where(xx => xx.StudentID == id).FirstOrDefault();
          
            StuGua.Guardians = db.Guardian.Where(xx => xx.GuardianID == StuGua.Students.GuardianID).FirstOrDefault();
          //  StuGua.Guardians = db.Guardian.Include(bb => bb.)
   
           // Locations.Suburbs = db.Suburb.ToList();

            return StuGua;
        }
        public IQueryable<Student> GetStudent()
        {
            return db.Student;
        }

        // GET: api/Students/5
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> GetStudent(int id)
        {
            Student student = await db.Student.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        // PUT: api/Students/5
        [System.Web.Http.Route("api/Students/PutStudent")]
        [System.Web.Mvc.HttpPut]

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStudent(StuGuaViewModel stuGuaViewModel)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //  if (studentid != stuGua.Students.StudentID && guardianid != stuGua.Guardians.GuardianID)
            //    if (id != student.StudentID)
            //  {
            //      return BadRequest();
            //  }

            //  db.Entry(student).State = EntityState.Modified;
            ////  db.Entry(stuGua.Guardians).State = EntityState.Modified;


            //  try
            //  {
            //      await db.SaveChangesAsync();
            //  }
            //  catch (DbUpdateConcurrencyException)
            //  {
            //      if (!StudentExists(id))
            //      {
            //          return NotFound();
            //      }
            //      else
            //      {
            //          throw;
            //      }
            //  }
            db.Configuration.ProxyCreationEnabled = false;

            //var CurrentStudent = await db.Student.Where(s => s.StudentID == stuGuaViewModel.Students.StudentID).FirstOrDefaultAsync();
            //var CurrentGuardian = await db.Guardian.Where(g => g.GuardianID == stuGuaViewModel.Guardians.GuardianID).FirstOrDefaultAsync();

            var CurrentStudent = stuGuaViewModel.Students;
            CurrentStudent.StudentFullName = stuGuaViewModel.Students.StudentName + " " + stuGuaViewModel.Students.StudentSurname;
            var CurrentGuardian = stuGuaViewModel.Guardians;

            db.Entry(CurrentStudent).State = EntityState.Modified;
            await db.SaveChangesAsync();

            db.Entry(CurrentGuardian).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.OK);
        }

        // POST: api/Students
        [ResponseType(typeof(Student))]
        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> PostStudent(StuGuaViewModel stuGua)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Guardian.Add(stuGua.Guardians);

            await db.SaveChangesAsync();

            var LastGuardian = await db.Guardian.OrderByDescending(g => g.GuardianID).FirstOrDefaultAsync();

            stuGua.Students.GuardianID = LastGuardian.GuardianID;
            stuGua.Students.StudentFullName = stuGua.Students.StudentName + " " + stuGua.Students.StudentSurname;

            db.Student.Add(stuGua.Students);

            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = stuGua.Students.StudentID }, stuGua.Students);
        }

        // DELETE: api/Students/5
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> DeleteStudent(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Student student = await db.Student.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            var guardian = db.Guardian.Find(student.GuardianID);


            db.Student.Remove(student);
            await db.SaveChangesAsync();

            db.Guardian.Remove(guardian);
            await db.SaveChangesAsync();

            return Ok(student);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StudentExists(int studentid)
        {
            return db.Student.Count(e => e.StudentID == studentid) > 0;
        }

        private bool GuardianExists(int guardianid)
        {
            return db.Guardian.Count(e => e.GuardianID == guardianid) > 0;
        }

        [System.Web.Http.Route("api/Students/GetLocations")]
        [System.Web.Mvc.HttpPost]

        public LocationViewModel GetLocations()
        {
            db.Configuration.ProxyCreationEnabled = false;

            LocationViewModel Locations = new LocationViewModel();
            Locations.Villages = db.Village.ToList();
            Locations.Countries = db.Country.ToList();
            Locations.Cities = db.City.ToList();
            Locations.Provinces = db.Province.ToList();
            Locations.Suburbs = db.Suburb.ToList();
            Locations.Schools = db.School.ToList();

            return Locations;
        }   
    }


}