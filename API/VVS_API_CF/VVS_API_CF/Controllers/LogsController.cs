﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class LogsController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/Logs
        [System.Web.Http.Route("api/Logss/GetLogEntries")]
        [System.Web.Mvc.HttpGet]

        public List<dynamic> GetLogEntries()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getGetLogEntriesList(db.Log.Include(xx => xx.Tag).ToList());
        }
        private List<dynamic> getGetLogEntriesList(List<Log> forClient)
        {
            List<dynamic> dynamicLogEntries = new List<dynamic>();
            foreach (Log log in forClient)
            {
                dynamic dynamicLogEntry = new ExpandoObject();

                dynamicLogEntry.LogID = log.LogID;
                dynamicLogEntry.TagID = log.TagID;
                dynamicLogEntry.TagSerialNumber = db.Tag.Where(xx => xx.TagID == log.TagID).Select(xx => xx.TagSerialNumber).FirstOrDefault();
                dynamicLogEntry.DateTime = log.DateTime.Value.ToString("yyyy-MM-dd HH:mm");
                //dynamicLogEntry.StudentName = db.Bicycle.Where(xx => xx.TagID == log.TagID).Include(xx => xx.BicycleStudents).Where(xx => xx.BicycleID == db.)
                dynamicLogEntry.StudentName = db.BicycleStudent.Include(a => a.Student).Include(b => b.Bicycle).Where(aa => aa.Bicycle.TagID == log.TagID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();


                
                dynamicLogEntries.Add(dynamicLogEntry);
            }

            return dynamicLogEntries;
        }
        public IQueryable<Log> GetLog()
        {
            return db.Log;
        }

        // GET: api/Logs/5
        [ResponseType(typeof(Log))]
        public async Task<IHttpActionResult> GetLog(int id)
        {
            Log log = await db.Log.FindAsync(id);
            if (log == null)
            {
                return NotFound();
            }

            return Ok(log);
        }

        // PUT: api/Logs/5


 
 

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogExists(int id)
        {
            return db.Log.Count(e => e.LogID == id) > 0;
        }
    }
}