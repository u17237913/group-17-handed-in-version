﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    public class PartTypesController : ApiController
    {
        private vvsContext db = new vvsContext();

        //----------------------------------------ALL Part Types API END-POINTS BELOW----------------------------------------------

        //GET ---> Part Types
        [System.Web.Http.Route("api/PartTypes/GetAllPartTypes")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllPartTypes()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getPartTypesReturnList(db.PartType.ToList());
        }

        private List<dynamic> getPartTypesReturnList(List<PartType> forBicycle)
        {
            List<dynamic> dynamicPartTypes = new List<dynamic>();
            foreach (PartType parttype in forBicycle)
            {
                dynamic dynamicPartType = new ExpandoObject();

                dynamicPartType.PartTypeID = parttype.PartTypeID;
                dynamicPartType.PartType1 = parttype.PartType1;

                dynamicPartTypes.Add(dynamicPartType);
            }
            return dynamicPartTypes;
        }

        //ADD ---> Part Type
        [System.Web.Http.Route("api/PartTypes/AddPartType")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddPartType([FromBody] PartType parttype)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.PartType.Add(parttype);
            db.SaveChanges();

            return getPartTypesReturnList(db.PartType.ToList());
        }

        //UPDATE ---> Part Type
        [System.Web.Http.Route("api/PartTypes/UpdatePartType")]
        [System.Web.Mvc.HttpPost]
        public bool UpdatePartType([FromBody] PartType _parttype)
        {
            using (vvsContext vvsSys = new vvsContext())
            {
                PartType updatedPartType = (from mt in vvsSys.PartType
                                                    where mt.PartTypeID == _parttype.PartTypeID
                                                    select mt)
                                            .FirstOrDefault();

                updatedPartType.PartTypeID = _parttype.PartTypeID;
                updatedPartType.PartType1 = _parttype.PartType1;

                vvsSys.SaveChanges();
            }
            return true;
        }

        //DELETE ---> BicyclePart
        [System.Web.Http.Route("api/PartTypes/DeletePartType")]
        [System.Web.Mvc.HttpDelete]
        public void DeletePartType(int id)
        {
            PartType PartTypeToDelete = (db.PartType
                                        .Where(o => o.PartTypeID == id))
                                        .FirstOrDefault();

            db.PartType.Remove(PartTypeToDelete);
            db.SaveChanges();
        }



        // --- SCAFFOLDED APPROACH BELOW IF NEEDED...



/*      // GET: api/PartTypes
        public IQueryable<PartType> GetPartType()
        {
            return db.PartType;
        }

        // GET: api/PartTypes/5
        [ResponseType(typeof(PartType))]
        public async Task<IHttpActionResult> GetPartType(int id)
        {
            PartType partType = await db.PartType.FindAsync(id);
            if (partType == null)
            {
                return NotFound();
            }

            return Ok(partType);
        }

        // PUT: api/PartTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPartType(int id, PartType partType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partType.PartTypeID)
            {
                return BadRequest();
            }

            db.Entry(partType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PartTypes
        [ResponseType(typeof(PartType))]
        public async Task<IHttpActionResult> PostPartType(PartType partType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartType.Add(partType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = partType.PartTypeID }, partType);
        }

        // DELETE: api/PartTypes/5
        [ResponseType(typeof(PartType))]
        public async Task<IHttpActionResult> DeletePartType(int id)
        {
            PartType partType = await db.PartType.FindAsync(id);
            if (partType == null)
            {
                return NotFound();
            }

            db.PartType.Remove(partType);
            await db.SaveChangesAsync();

            return Ok(partType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartTypeExists(int id)
        {
            return db.PartType.Count(e => e.PartTypeID == id) > 0;
        }
*/
    }
}