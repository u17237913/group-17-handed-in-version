﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MaintenanceListsController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/MaintenanceLists ie: the stuff inside MaintenanceJobs
        [System.Web.Http.Route("api/MaintenanceLists/GetMaintenanceLists")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetMaintenanceLists(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<MaintenanceList> mainList = db.MaintenanceList.Include(s => s.MaintenanceListStatu).Include(z => z.MaintenanceType).Include(x => x.MaintenanceJob).Include(y => y.MaintenanceType.MaintenanceTasks).Where(x => x.MaintenanceJobID == id).ToList();
            return getMaintenanceListLists(mainList);
        }
        private List<dynamic> getMaintenanceListLists(List<MaintenanceList> forClient)
        {
            List<dynamic> dynamicMTs = new List<dynamic>();
            foreach (MaintenanceList mt in forClient)
            {
                dynamic dynamicMT = new ExpandoObject();
                dynamicMT.MaintenanceListID = mt.MaintenanceListID;
                dynamicMT.MaintenanceListStatusID = mt.MaintenanceListStatusID;
                dynamicMT.MaintenanceJobID = mt.MaintenanceJobID;
                dynamicMT.MaintenanceTypeID = mt.MaintenanceTypeID;
                dynamicMT.MaintenanceListStatus = mt.MaintenanceListStatu.MaintenanceListStatus;
                dynamicMT.MaintenanceJobDate = mt.MaintenanceJob.DateScheduled;
                dynamicMT.MaintenanceType = mt.MaintenanceType.MaintenanceType1;
                var myMaintenanceTask = mt.MaintenanceType.MaintenanceTasks;
                dynamicMT.MaintenanceTask = myMaintenanceTask.Where(y => y.MaintenanceTypeID == mt.MaintenanceTypeID).Select(z => z.MaintenanceTask1);

                dynamicMTs.Add(dynamicMT);
            }
            return dynamicMTs;
        }

        // GET: api/MaintenanceLists/5
        [ResponseType(typeof(MaintenanceList))]
        public async Task<IHttpActionResult> GetMaintenanceList(int id)
        {
            MaintenanceList maintenanceList = await db.MaintenanceList.FindAsync(id);
            if (maintenanceList == null)
            {
                return NotFound();
            }

            return Ok(maintenanceList);
        }

        // PUT: api/MaintenanceLists/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMaintenanceList(int id, MaintenanceList maintenanceList)
        {
            maintenanceList.MaintenanceJob = db.MaintenanceJob.Where(x => x.MaintenanceJobID == maintenanceList.MaintenanceJobID).FirstOrDefault();
            maintenanceList.MaintenanceListStatu = db.MaintenanceListStatus.Where(x => x.MaintenanceListStatusID == maintenanceList.MaintenanceListStatusID).FirstOrDefault();
            maintenanceList.MaintenanceType = db.MaintenanceType.Where(x => x.MaintenanceTypeID == maintenanceList.MaintenanceTypeID).FirstOrDefault();

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            if (id != maintenanceList.MaintenanceListID)
            {
                return BadRequest();
            }
            
            db.Entry(maintenanceList).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaintenanceListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            this.ScheduleMaintenance(id);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MaintenanceLists
        [ResponseType(typeof(MaintenanceList))]
        public async Task<IHttpActionResult> PostMaintenanceList(MaintenanceList maintenanceList)
        {
            maintenanceList.MaintenanceJob = db.MaintenanceJob.Where(x => x.MaintenanceJobID == maintenanceList.MaintenanceJobID).FirstOrDefault();
            maintenanceList.MaintenanceListStatu = db.MaintenanceListStatus.Where(x => x.MaintenanceListStatusID == maintenanceList.MaintenanceListStatusID).FirstOrDefault();
            maintenanceList.MaintenanceType = db.MaintenanceType.Where(x => x.MaintenanceTypeID == maintenanceList.MaintenanceTypeID).FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MaintenanceList.Add(maintenanceList);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = maintenanceList.MaintenanceListID }, maintenanceList);
        }

        // DELETE: api/MaintenanceLists/5
        [ResponseType(typeof(MaintenanceList))]
        public async Task<IHttpActionResult> DeleteMaintenanceList(int id)
        {
            MaintenanceList maintenanceList = await db.MaintenanceList.FindAsync(id);
            if (maintenanceList == null)
            {
                return NotFound();
            }

            db.MaintenanceList.Remove(maintenanceList);
            await db.SaveChangesAsync();

            return Ok(maintenanceList);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MaintenanceListExists(int id)
        {
            return db.MaintenanceList.Count(e => e.MaintenanceListID == id) > 0;
        }


        [System.Web.Http.Route("api/MaintenanceLists/ScheduleMaintenance")]
        [System.Web.Mvc.HttpPost]
        public void ScheduleMaintenance(int id)
        {
            int typeID = (int)db.MaintenanceList.Where(x => x.MaintenanceListID == id).Select(y => y.MaintenanceTypeID).FirstOrDefault();

            int freq = (int)db.MaintenanceType.Where(x => x.MaintenanceTypeID == typeID).Select(y => y.Frequency).FirstOrDefault();

            int jobID = (int)db.MaintenanceList.Where(x => x.MaintenanceListID == id).Select(y => y.MaintenanceJobID).FirstOrDefault();

            MaintenanceList currentMaintenance = db.MaintenanceList.Where(x => x.MaintenanceListID == id).FirstOrDefault();

            DateTime newdate = DateTime.Now.AddMonths(freq).Date;
            DateTime olddate = new DateTime();
            olddate = db.MaintenanceJob.Where(y => y.DateScheduled == newdate).Select(z => z.DateScheduled).FirstOrDefault() ?? DateTime.Now;

            List<int?> bicycleIDCheck1 = db.MaintenanceJob.Where(y => y.DateScheduled == newdate).Select(z => z.BicycleID).ToList();
            int? bicycleIDCheck2 = db.MaintenanceJob.Where(x => x.MaintenanceJobID == jobID).Select(y => y.BicycleID).FirstOrDefault();

            if (newdate.Date != olddate.Date && !bicycleIDCheck1.Contains(bicycleIDCheck2))
            {
                MaintenanceJob maintenanceJob = new MaintenanceJob();
                maintenanceJob.BicycleID = db.MaintenanceJob.Where(x => x.MaintenanceJobID == jobID).Select(y => y.BicycleID).FirstOrDefault();
                maintenanceJob.MaintenanceJobStatusID = 3;
                maintenanceJob.DateScheduled = newdate;
                maintenanceJob.Assigned = true;
                maintenanceJob.CheckInID = ScheduleCheckIn(maintenanceJob);
                db.MaintenanceJob.Add(maintenanceJob);
                db.SaveChanges();
            }

            MaintenanceList newSheduledList = new MaintenanceList();
            newSheduledList.MaintenanceListStatusID = 1;
            newSheduledList.MaintenanceJobID = db.MaintenanceJob.Where(y => y.DateScheduled == newdate).Select(z => z.MaintenanceJobID).FirstOrDefault();
            newSheduledList.MaintenanceTypeID = typeID;
            //newSheduledList.MaintenanceJob = db.MaintenanceJob.Where(y => y.MaintenanceJobID == jobID).FirstOrDefault();
            //newSheduledList.MaintenanceListStatu = db.MaintenanceListStatus.Where(y => y.MaintenanceListStatusID == 1).FirstOrDefault();
            //newSheduledList.MaintenanceType = db.MaintenanceType.Where(x => x.MaintenanceTypeID == typeID).FirstOrDefault();
            db.MaintenanceList.Add(newSheduledList);
            db.SaveChanges();
        }

        [System.Web.Http.Route("api/MaintenanceLists/UpdateMaintenanceJobStatus")]
        [System.Web.Mvc.HttpPost]
        public void UpdateMaintenanceJobStatus([FromBody] int id)
        {
            MaintenanceJob maintenanceJob = db.MaintenanceJob.Where(x => x.MaintenanceJobID == id).FirstOrDefault();
            maintenanceJob.MaintenanceJobStatusID = 2;
            db.SaveChanges();
        }

        public int ScheduleCheckIn(MaintenanceJob maintenanceJob)
        {
            var student = db.BicycleStudent.Where(x => x.BicycleID == maintenanceJob.BicycleID && x.DateUnassigned == null).FirstOrDefault();
            var school = db.Student.Where(y => y.StudentID == student.StudentID).Select(x => x.SchoolID).FirstOrDefault();
            var mechanicSchool = db.MechanicSchool.Where(x => x.SchoolID == school).FirstOrDefault();

            CheckIn dynamicCheckIn = new CheckIn();
            dynamicCheckIn.JobTypeID = 2;
            dynamicCheckIn.CheckInStatusID = 1;
            dynamicCheckIn.CheckInDate = null;
            dynamicCheckIn.MechanicSchoolID = mechanicSchool.MechanicSchoolID;

            db.CheckIn.Add(dynamicCheckIn);
            db.SaveChanges();

            try
            {
                var studentNumberToSend = db.BicycleStudent.Include(ee => ee.Bicycle).Include(ee => ee.Student).Where(ee => ee.BicycleID == maintenanceJob.BicycleID).Select(ee => ee.Student.PhoneNumber).FirstOrDefault();
                var studentNameToSend = db.BicycleStudent.Include(ee => ee.Bicycle).Include(ee => ee.Student).Where(ee => ee.BicycleID == maintenanceJob.BicycleID).Select(ee => ee.Student.StudentName + " " + ee.Student.StudentSurname).FirstOrDefault();
                var studbikecode = db.Bicycle.Where(bc => bc.BicycleID == maintenanceJob.BicycleID).Select(bc => bc.BicyleCode).FirstOrDefault();

                // Find your Account Sid and Token at twilio.com/console
                // DANGER! This is insecure. See http://twil.io/secure
                const string accountSid = "AC0d9e20e4cc743b4494969bfcd9613f85";
                const string authToken = "47a721b61b23a3ace55346b7feef8715";

                string manipNum, StudCell;

                if (studentNumberToSend.Substring(0, 1) == "0")
                {
                    //removing the 0 in 0XXXXXXXXX to make it +27XXXXXXXXX
                    manipNum = studentNumberToSend.Remove(0, 1);
                    StudCell = "+27" + manipNum;
                }
                else if ((studentNumberToSend.Substring(0, 3) == "+27"))
                {
                    manipNum = studentNumberToSend;
                    StudCell = manipNum;
                }
                else
                {
                    manipNum = studentNumberToSend;
                    StudCell = "+27" + manipNum;
                }

                TwilioClient.Init(accountSid, authToken);

                dynamic textval = new ExpandoObject();
                textval.txt = "Hello " + studentNameToSend +
                                " You have a maintenance job scheduled for your bicycle " + studbikecode +
                                " scheduled for " + maintenanceJob.DateScheduled +
                                ".\nPlease bring the bicycle on this day to be maintained\n -#Bikes4ERP";

                var message = MessageResource.Create(
                    from: new Twilio.Types.PhoneNumber("+12056229145"),
                    body: textval.txt,
                    to: new Twilio.Types.PhoneNumber(StudCell)
                );

                Console.WriteLine(message.Sid);

            }
            catch (Exception err)
            {
                Console.WriteLine(err);
            }

            return dynamicCheckIn.CheckInID;
        }

    }
}