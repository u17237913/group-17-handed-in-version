﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    public class FaultTypesController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/FaultTypes

        [System.Web.Http.Route("api/FaultTypes/GetFaultTypes")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetFaultTypes()
        {
            db.Configuration.ProxyCreationEnabled = false;

            List<FaultType> faultTypes = db.FaultType.ToList();
            return getFaultTypeList(faultTypes);
        }
        private List<dynamic> getFaultTypeList(List<FaultType> forClient)
        {

            List<dynamic> dynamicMTs = new List<dynamic>();

            foreach (FaultType mt in forClient)
            {
                dynamic dynamicMT = new ExpandoObject();
                dynamicMT.FaultTypeID = mt.FaultTypeID;
                dynamicMT.FaultType1 = mt.FaultType1;
                dynamicMT.PartNeeded = mt.PartNeeded;
               
                
               
             
                dynamicMTs.Add(dynamicMT);
            }
            return dynamicMTs;
        }

        // GET: api/FaultTypes/5
        [ResponseType(typeof(FaultType))]
        public async Task<IHttpActionResult> GetFaultType(int id)
        {
            FaultType faultType = await db.FaultType.FindAsync(id);
            if (faultType == null)
            {
                return NotFound();
            }

            return Ok(faultType);
        }

        // GET: api/FaultTypes/5
        [ResponseType(typeof(FaultType))]
        [System.Web.Http.Route("api/FaultTypes/GetFaultTypeByPart")]
        [System.Web.Mvc.HttpGet]
        public async Task<IHttpActionResult> GetFaultTypeByPart(int PartID)
        {
            db.Configuration.ProxyCreationEnabled = false;
          
            var faultType = await db.PartFault.Include(pf => pf.FaultType).Where(pf => pf.PartID == PartID).Select( pf => pf.FaultType ).FirstOrDefaultAsync();

            if (faultType == null)
            {
                return NotFound();
            }

            return Ok(faultType);
        }

        // PUT: api/FaultTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFaultType(int id, FaultType faultType)
        {

            int pfid = 0;
            //create the partfault object
            PartFault mypartfault = new PartFault();
            //looping thru to find partID
            foreach (Part mypart in db.Part)
            {
                if (mypart.PartName == faultType.PartNeeded)
                {
                    mypartfault.PartID = mypart.PartID;
                }

            }

            foreach (PartFault mpf in db.PartFault)
            {
                if (mpf.FaultTypeID == faultType.FaultTypeID)
                {

                    mypartfault.PartFaultID = mpf.PartFaultID;
                    pfid = mypartfault.PartFaultID;
                }

            }


            mypartfault.FaultTypeID = faultType.FaultTypeID;

            PartFaultsController pfController = new PartFaultsController();

            await pfController.PutPartFault(pfid, mypartfault);


            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != faultType.FaultTypeID)
            {
                return BadRequest();
            }

            db.Entry(faultType).State = EntityState.Modified;
            

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FaultTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        private bool PartFaultExists(int id)
        {
            return db.PartFault.Count(e => e.PartFaultID == id) > 0;
        }

        // POST: api/FaultTypes
        [ResponseType(typeof(FaultType))]
        public async Task<IHttpActionResult> PostFaultType(FaultType faultType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FaultType.Add(faultType);
            await db.SaveChangesAsync();

            //return CreatedAtRoute("DefaultApi", new { id = faultType.FaultTypeID }, faultType);

            //create the partfault object
            PartFault mypartfault = new PartFault();
            //looping thru to find partID
            foreach (Part mypart in db.Part)
            {
                if (mypart.PartName == faultType.PartNeeded)
                {
                    mypartfault.PartID = mypart.PartID;
                }

            }

            mypartfault.FaultTypeID = faultType.FaultTypeID;

            //rufi here is where youll put the image
            //mypartfault.FaultImage = "null";

            //posting a Part fault as well
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartFault.Add(mypartfault);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = mypartfault.PartFaultID }, mypartfault);
        }

        // DELETE: api/FaultTypes/5
        [ResponseType(typeof(FaultType))]
        public async Task<IHttpActionResult> DeleteFaultType(int id)
        {
            int partfaultID = 0;
            //looping thru to find partID
            foreach (PartFault mypartfault in db.PartFault)
            {
                if (mypartfault.FaultTypeID == id)
                {
                    partfaultID = mypartfault.PartFaultID;
                }

            }

            PartFault partFault = await db.PartFault.FindAsync(partfaultID);
            if (partFault == null)
            {
                return NotFound();
            }

            db.PartFault.Remove(partFault);
            await db.SaveChangesAsync();

        



            FaultType faultType = await db.FaultType.FindAsync(id);
            if (faultType == null)
            {
                return NotFound();
            }

            db.FaultType.Remove(faultType);
            await db.SaveChangesAsync();

            return Ok(faultType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FaultTypeExists(int id)
        {
            return db.FaultType.Count(e => e.FaultTypeID == id) > 0;
        }


       

    }
}