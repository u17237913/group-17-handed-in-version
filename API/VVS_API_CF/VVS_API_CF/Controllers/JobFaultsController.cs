﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Http.OData.Query;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JobFaultsController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/JobFaults
        [System.Web.Http.Route("api/JobFaults/GetJobFaults")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetJobFaults(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getJobFaultLists(db.JobFault.Include(s => s.Part).Include(z => z.JobFaultStatu).Include(z => z.Section).Where(rj => rj.RepairJobID == id).ToList());
        }
        private List<dynamic> getJobFaultLists(List<JobFault> forClient)
        {
            List<dynamic> dynamicMTs = new List<dynamic>();
            foreach (JobFault mt in forClient)
            {
                dynamic dynamicMT = new ExpandoObject();
                dynamicMT.JobFaultID = mt.JobFaultID;
                dynamicMT.PartFaultID = mt.PartFaultID;
                dynamicMT.PartID = mt.PartID;
                if (mt.PartID == null)
                {
                    dynamicMT.Part = "Unknown";
                }
                else
                {
                    //dynamicMT.Part = mt.Part.PartName.ToString();
                    dynamicMT.Part = mt.Part.PartName;
                }
                //dynamicMT.SectionPartID = mt.SectionPartID;
                if (mt.SectionID == null)
                {
                    dynamicMT.Section = "Unknown";
                }
                else
                {
                    //dynamicMT.Section = db.Section.Where(x => x.SectionID == mt.SectionID).Select(y => y.SectionName).FirstOrDefault().ToString();
                    dynamicMT.Section = db.Section.Where(x => x.SectionID == mt.SectionID).Select(y => y.SectionName).FirstOrDefault();

                }
                dynamicMT.RepairJobID = mt.RepairJobID;
                dynamicMT.JobFaultStatusID = mt.JobFaultStatusID;
                dynamicMT.JobFaultStatus = mt.JobFaultStatu.JobFaultStatus;
                //dynamicMT.SectionID = mt.Section_SectionID;
                if (mt.PartFaultID == null)
                {
                    dynamicMT.Notes = mt.Notes;
                }
                else
                {
                    dynamicMT.Notes = db.PartFault.Include(x => x.FaultType).Where(y => y.PartFaultID == mt.PartFaultID).Select(z => z.FaultType.FaultType1).FirstOrDefault();
                }

                dynamicMTs.Add(dynamicMT);
            }
            return dynamicMTs;
        }


        // GET: api/JobFaults/5
        [ResponseType(typeof(JobFault))]
        public async Task<IHttpActionResult> GetJobFault(int id)
        {
            JobFault jobFault = await db.JobFault.FindAsync(id);
            if (jobFault == null)
            {
                return NotFound();
            }

            return Ok(jobFault);
        }

      
        // PUT: api/JobFaults/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutJobFault(int id, JobFault jobFault)
        {
            db.Configuration.ProxyCreationEnabled = false;

            jobFault.Part = db.Part.Where(pp => pp.PartID == jobFault.PartID).FirstOrDefault();
            jobFault.Section = db.Section.Where(pp => pp.SectionID == jobFault.SectionID).FirstOrDefault();
            jobFault.RepairJob = db.RepairJob.Where(pp => pp.RepairJobID == jobFault.RepairJobID).FirstOrDefault();
            jobFault.PartFault = db.PartFault.Where(pp => pp.PartFaultID == jobFault.PartFaultID).FirstOrDefault();
            jobFault.JobFaultStatu = db.JobFaultStatus.Where(pp => pp.JobFaultStatusID == jobFault.JobFaultStatusID).FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != jobFault.JobFaultID)
            {
                return BadRequest();
            }

            db.Entry(jobFault).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobFaultExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JobFaults
        [ResponseType(typeof(JobFault))]
        public async Task<IHttpActionResult> PostJobFault(JobFault jobFault)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.JobFault.Add(jobFault);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = jobFault.JobFaultID }, jobFault);
        }

        // DELETE: api/JobFaults/5
        [ResponseType(typeof(JobFault))]
        public async Task<IHttpActionResult> DeleteJobFault(int id)
        {
            JobFault jobFault = await db.JobFault.FindAsync(id);
            if (jobFault == null)
            {
                return NotFound();
            }

            db.JobFault.Remove(jobFault);
            await db.SaveChangesAsync();

            return Ok(jobFault);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobFaultExists(int id)
        {
            return db.JobFault.Count(e => e.JobFaultID == id) > 0;
        }

        [System.Web.Http.Route("api/JobFaults/UpdateJobStatus")]
        [System.Web.Mvc.HttpPost]
        public void UpdateJobStatus([FromBody] int id)
        {
            RepairJob repairJob = db.RepairJob.Where(x => x.RepairJobID == id).FirstOrDefault();
            repairJob.RepairJobStatusID = 2;
            db.SaveChanges();
        }
    }
}