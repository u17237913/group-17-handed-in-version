﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    public class CheckInsController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/CheckIns
        public IQueryable<CheckIn> GetCheckIn()
        {
            return db.CheckIn;
        }

        // GET: api/CheckIns/5
        [System.Web.Http.Route(Name = "GetCheckIn")]
        [ResponseType(typeof(CheckIn))]
        public async Task<IHttpActionResult> GetCheckIn(int id, CheckIn checkIn)
        {
            //CheckIn checkIn = await db.CheckIn.FindAsync(id);
            if (checkIn == null)
            {
                return NotFound();
            }

            return Ok(checkIn);
        }

        // PUT: api/CheckIns/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCheckIn(int id, CheckIn checkIn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != checkIn.CheckInID)
            {
                return BadRequest();
            }

            db.Entry(checkIn).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckInExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CheckIns
        [System.Web.Mvc.HttpPost]
        [ResponseType(typeof(CheckIn))]
        public async Task<IHttpActionResult> PostCheckIn(CheckIn checkIn , int BicycleID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var SchoolID = db.BicycleStudent.Include( bs => bs.Student ).Where(bs => bs.BicycleID == BicycleID).Select(bs => bs.Student.SchoolID).FirstOrDefault();

            if (SchoolID.HasValue)
            {
                var MechanicSchoolID = db.MechanicSchool.Where(ms => ms.SchoolID == SchoolID).Select(ms => ms.MechanicSchoolID).FirstOrDefault();
                if (MechanicSchoolID > 0)
                {
                    checkIn.MechanicSchoolID = MechanicSchoolID;
                }
            }


            db.CheckIn.Add(checkIn);
            await db.SaveChangesAsync();

            return CreatedAtRoute("GetCheckIn", new { id = checkIn.CheckInID }, checkIn);
        }

        // DELETE: api/CheckIns/5
        [ResponseType(typeof(CheckIn))]
        public async Task<IHttpActionResult> DeleteCheckIn(int id)
        {
            CheckIn checkIn = await db.CheckIn.FindAsync(id);
            if (checkIn == null)
            {
                return NotFound();
            }

            db.CheckIn.Remove(checkIn);
            await db.SaveChangesAsync();

            return Ok(checkIn);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CheckInExists(int id)
        {
            return db.CheckIn.Count(e => e.CheckInID == id) > 0;
        }
    }
}