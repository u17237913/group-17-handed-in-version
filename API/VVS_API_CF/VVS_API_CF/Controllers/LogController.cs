﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;

namespace VVS_API_CF.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class LogController : ApiController
    {

        vvsContext db = new vvsContext();

        [Route("api/log/addLogEntry")]
        [HttpPost]
        public void addTagLog([FromBody] TagRead tagRead)
        {
            if (tagRead != null)
            {
                var ScannedTag = db.Tag.Where(x => x.TagSerialNumber == tagRead.TagSerial).Select(y => y.TagID).FirstOrDefault();

                Log dynamicLog = new Log();
                dynamicLog.TagID = ScannedTag;
                dynamicLog.AntennaID = tagRead.AntennaID;
                dynamicLog.DateTime = tagRead.TimeTagged;


                db.Configuration.ProxyCreationEnabled = false;
                db.Log.Add(dynamicLog);
                addAttendance(ScannedTag, tagRead.AntennaID);
                db.SaveChanges();
            }

        }

        [Route("api/log/addAttendance")]
        [HttpPost]
        public async void addAttendance(int TagID, int AntennaID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            DateTime currentDate = DateTime.Now.Date;
            string dateToday = currentDate.ToString("d");
            DayOfWeek day = DateTime.Now.DayOfWeek;
            string dayToday = day.ToString();


            //If day of the week is between Monday and Friday
            if ((dayToday == DayOfWeek.Monday.ToString()) ||
                (dayToday == DayOfWeek.Tuesday.ToString()) ||
                (dayToday == DayOfWeek.Wednesday.ToString()) ||
                (dayToday == DayOfWeek.Thursday.ToString()) ||
                (dayToday == DayOfWeek.Friday.ToString())
                )
            {
                //Student
                var bicycle = db.Bicycle.Include(pp => pp.Tag).Where(rr => rr.TagID == TagID).Select(tt => tt.BicycleID).FirstOrDefault();
                var bicyclestudent = db.BicycleStudent.Include(zz => zz.Student).Where(qq => qq.BicycleID == bicycle).Select(yy => yy.StudentID).FirstOrDefault();
                var CurrentStudent = db.Student.Include(ww => ww.BicycleStudents).Where(aa => aa.StudentID == bicyclestudent).FirstOrDefault();


                //DbFunctions.TruncateTime(l.DateTime.Value)
                var AttendanceList = db.Attendance.Where(a => DbFunctions.TruncateTime(a.Date.Value) == currentDate).ToList();
                var StudentAttendanceList = db.StudentAttendance.Where(sa => sa.StudentID == CurrentStudent.StudentID).ToList();

                bool DidStudentAttend = await this.DidStudentAttend(AttendanceList, StudentAttendanceList);

                if (!DidStudentAttend)
                {
                    // var studentSchool = db.Student.Include(ww => ww.BicycleStudents).Where(aa => aa.StudentID == bicyclestudent).Select(ss => ss.SchoolID).FirstOrDefault();
                    //Antenna
                    var SchoolID = db.Route.Where(qq => qq.Position2 == AntennaID).Select(ww => ww.SchoolID).FirstOrDefault();
                                                           
                    if (SchoolID == CurrentStudent.SchoolID)
                    {
                        StudentAttendance dynamicAttendance = new StudentAttendance();
                        Attendance dynamicAttendancedate = new Attendance();
                        // dynamicAttendance.AttendanceID = db.Attendance.Where(zz => zz.Date == currentdate).Select(ww => ww.AttendanceID).FirstOrDefault();
                        dynamicAttendancedate.Date = DateTime.Now;
                        dynamicAttendance.AttendanceID = dynamicAttendancedate.AttendanceID;
                        dynamicAttendance.StudentID = CurrentStudent.StudentID;
                        db.StudentAttendance.Add(dynamicAttendance);
                        db.Attendance.Add(dynamicAttendancedate);
                        db.SaveChanges();
                    }

                }

            }

        }

        public async Task<bool> DidStudentAttend( List<Attendance> AttendanceList, List<StudentAttendance> StudentAttendanceList)
        {
            foreach (var studentAttendance in StudentAttendanceList)
            {
                foreach (var attendance in AttendanceList)
                {
                    if (attendance.AttendanceID == studentAttendance.AttendanceID)
                    {
                        return true;
                    }
                }

            }
            return false;
        }
    }
    }
