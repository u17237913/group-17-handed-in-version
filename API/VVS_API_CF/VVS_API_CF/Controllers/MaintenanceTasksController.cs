﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MaintenanceTasksController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/MaintenanceTasks
        [System.Web.Http.Route("api/MaintenanceTasks/GetMaintenanceTasks")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetMaintenanceTasks()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getMaintenanceTaskList(db.MaintenanceTask.Include(s => s.Part).Include(z => z.MaintenanceType).ToList());
        }
        private List<dynamic> getMaintenanceTaskList(List<MaintenanceTask> forClient)
        {
            List<dynamic> dynamicMTs = new List<dynamic>();
            foreach (MaintenanceTask mt in forClient)
            {
                dynamic dynamicMT = new ExpandoObject();
                dynamicMT.ID = mt.MaintenanceTaskID;
                dynamicMT.MaintenanceTaskDescription = mt.MaintenanceTaskDescription;
                dynamicMT.MaintenanceTask = mt.MaintenanceTask1;
                dynamicMT.MaintenanceTypeID = mt.MaintenanceTypeID;
                dynamicMT.MaintenanceTaskType = mt.MaintenanceType.MaintenanceType1;
                dynamicMT.PartID = mt.PartID;
                dynamicMT.Part = mt.Part.PartName;
               

                dynamicMTs.Add(dynamicMT);
            }
            return dynamicMTs;
        }


        // GET: api/MaintenanceTasks/5
        [System.Web.Http.Route("api/MaintenanceTasks/GetMaintenanceTask")]
        [System.Web.Mvc.HttpGet]
        [ResponseType(typeof(MaintenanceTask))]
        public async Task<IHttpActionResult> GetMaintenanceTask(int id)
        {
            MaintenanceTask maintenanceTask = await db.MaintenanceTask.FindAsync(id);
            if (maintenanceTask == null)
            {
                return NotFound();
            }

            return Ok(maintenanceTask);
        }

        // PUT: api/MaintenanceTasks/5

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMaintenanceTask(int id, MaintenanceTask maintenanceTask)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != maintenanceTask.MaintenanceTaskID)
            {
                return BadRequest();
            }

            db.Entry(maintenanceTask).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaintenanceTaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MaintenanceTasks
        //[System.Web.Http.Route("api/MaintenanceTasks/PostMaintenanceTask")]
        //[System.Web.Mvc.HttpPost]
        [ResponseType(typeof(MaintenanceTask))]
        public async Task<IHttpActionResult> PostMaintenanceTask(MaintenanceTask maintenanceTask)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MaintenanceTask.Add(maintenanceTask);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = maintenanceTask.MaintenanceTaskID }, maintenanceTask);
        }

        // DELETE: api/MaintenanceTasks/5
        [ResponseType(typeof(MaintenanceTask))]
        public async Task<IHttpActionResult> DeleteMaintenanceTask(int id)
        {
            MaintenanceTask maintenanceTask = await db.MaintenanceTask.FindAsync(id);
            if (maintenanceTask == null)
            {
                return NotFound();
            }

            db.MaintenanceTask.Remove(maintenanceTask);
            await db.SaveChangesAsync();

            return Ok(maintenanceTask);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MaintenanceTaskExists(int id)
        {
            return db.MaintenanceTask.Count(e => e.MaintenanceTaskID == id) > 0;
        }

        //GetMaintenanceTypeList
        [System.Web.Http.Route("api/MaintenanceTasks/GetMaintenanceTypes")]
        [System.Web.Mvc.HttpGet]

        
        public List<dynamic> GetMaintenanceTypes()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getMaintenanceTypesList(db.MaintenanceType.ToList());
        }
        private List<dynamic> getMaintenanceTypesList(List<MaintenanceType> forClient)
        {
            List<dynamic> dynamicTypes = new List<dynamic>();
            foreach (MaintenanceType type in forClient)
            {
                dynamic dynamicType = new ExpandoObject();
                dynamicType.MaintenanceTypeID = type.MaintenanceTypeID;
                dynamicType.MaintenanceType = type.MaintenanceType1;
                dynamicType.Frequency = type.Frequency;
               


                dynamicTypes.Add(dynamicType);
            }
            return dynamicTypes;
        }

        //GetMaintenanceTypeList
        [System.Web.Http.Route("api/MaintenanceTasks/GetParts")]
        [System.Web.Mvc.HttpGet]


        public List<dynamic> GetParts()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getPartsList(db.Part.ToList());
        }
        private List<dynamic> getPartsList(List<Part> forClient)
        {
            List<dynamic> dynamicParts = new List<dynamic>();
            foreach (Part part in forClient)
            {
                dynamic dynamicPart = new ExpandoObject();
                dynamicPart.PartID = part.PartID;
                dynamicPart.PartName = part.PartName;
               



                dynamicParts.Add(dynamicPart);
            }
            return dynamicParts;
        }

    }
}