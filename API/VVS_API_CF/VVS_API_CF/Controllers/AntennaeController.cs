﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;

namespace VVS_API_CF.Controllers


{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AntennaeController : ApiController
    {
        private vvsContext db = new vvsContext();

        [System.Web.Http.Route("api/Antennas/GetAntennas")]
        [System.Web.Mvc.HttpGet]

        public List<dynamic> GetAntennas()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getAntennasList(db.Antenna.ToList());
        }
        private List<dynamic> getAntennasList(List<Antenna> forClient)
        {
            List<dynamic> dynamicAntennas = new List<dynamic>();
            foreach (Antenna antenna in forClient)
            {
                dynamic dynamicAntenna = new ExpandoObject();

                dynamicAntenna.AntennaID = antenna.AntennaID;
                dynamicAntenna.AntennaAddress = antenna.AntennaAddress;
                dynamicAntenna.AntennaStatusID = antenna.AntennaStatusID;
                dynamicAntenna.AntennaStatu = db.AntennaStatus.Where(xx => xx.AntennaStatusID == antenna.AntennaStatusID).Select(y => y.AntennaStatus).FirstOrDefault();
                dynamicAntenna.CoOrdinates = antenna.CoOrdinates;
                dynamicAntenna.DateInstalled = antenna.DateInstalled.Value.ToString("yyyy-MM-dd");
                dynamicAntenna.SuburbID = antenna.SuburbID;
                dynamicAntenna.SuburbName = db.Suburb.Where(xx => xx.SuburbID == antenna.SuburbID).Select(y => y.SuburbName).FirstOrDefault();

                dynamicAntennas.Add(dynamicAntenna);
            }

            return dynamicAntennas;
        }


        [System.Web.Http.Route("api/Antennas/GetAntennaStatus")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetAntennaStatus()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getAntennaStatusList(db.AntennaStatus.ToList());
        }
        private List<dynamic> getAntennaStatusList(List<AntennaStatu> forClient)
        {
            List<dynamic> dynamicAntennaStatuss = new List<dynamic>();
            foreach (AntennaStatu antennastatus in forClient)
            {
                dynamic dynamicAntennaStatus = new ExpandoObject();

                dynamicAntennaStatus.AntennaStatusID = antennastatus.AntennaStatusID;
                dynamicAntennaStatus.AntennaStatus = antennastatus.AntennaStatus;


                dynamicAntennaStatuss.Add(dynamicAntennaStatus);
            }

            return dynamicAntennaStatuss;
        }

        public LocationViewModel GetLocations()
        {
            db.Configuration.ProxyCreationEnabled = false;

            LocationViewModel Locations = new LocationViewModel();
            Locations.Villages = db.Village.ToList();
            Locations.Countries = db.Country.ToList();
            Locations.Cities = db.City.ToList();
            Locations.Provinces = db.Province.ToList();
            Locations.Suburbs = db.Suburb.ToList();

            return Locations;
        }

        // GET: api/Antennae
    
        public IQueryable<Antenna> GetAntenna()
        {
            return db.Antenna;
        }

        // GET: api/Antennae/5
        [ResponseType(typeof(Antenna))]
        public async Task<IHttpActionResult> GetAntenna(int id)
        {
            Antenna antenna = await db.Antenna.FindAsync(id);
            if (antenna == null)
            {
                return NotFound();
            }

            return Ok(antenna);
        }
        [System.Web.Http.Route("api/Antennas/PutAntenna")]
        [System.Web.Mvc.HttpPut]
        // PUT: api/Antennae/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAntenna(int id, Antenna antenna)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != antenna.AntennaID)
            {
                return BadRequest();
            }

            db.Entry(antenna).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AntennaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Antennae
        //[System.Web.Http.Route("api/Antennas/PostAntenna")]
        //[System.Web.Mvc.HttpPost]
        [ResponseType(typeof(Antenna))]
        public async Task<IHttpActionResult> PostAntenna(Antenna antenna)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


           
            db.Antenna.Add(antenna);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = antenna.AntennaID }, antenna);
        }

        // DELETE: api/Antennae/5
        [System.Web.Http.Route("api/Antennas/DeleteAntenna")]
        [System.Web.Mvc.HttpDelete]
        [ResponseType(typeof(Antenna))]
        public async Task<IHttpActionResult> DeleteAntenna(int id)
        {
            Antenna antenna = await db.Antenna.FindAsync(id);
            if (antenna == null)
            {
                return NotFound();
            }

            db.Antenna.Remove(antenna);
            await db.SaveChangesAsync();

            return Ok(antenna);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AntennaExists(int id)
        {
            return db.Antenna.Count(e => e.AntennaID == id) > 0;
        }
    }
}