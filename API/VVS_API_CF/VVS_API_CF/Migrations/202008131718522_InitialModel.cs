﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admin",
                c => new
                    {
                        AdminID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(),
                    })
                .PrimaryKey(t => t.AdminID)
                .ForeignKey("dbo.User", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AdminSchool",
                c => new
                    {
                        AdminSchoolID = c.Int(nullable: false, identity: true),
                        SchoolID = c.Int(),
                        UserID = c.Int(),
                        AdminID = c.Int(),
                        Position = c.String(),
                    })
                .PrimaryKey(t => t.AdminSchoolID)
                .ForeignKey("dbo.Admin", t => t.AdminID)
                .ForeignKey("dbo.School", t => t.SchoolID)
                .Index(t => t.SchoolID)
                .Index(t => t.AdminID);
            
            CreateTable(
                "dbo.School",
                c => new
                    {
                        SchoolID = c.Int(nullable: false, identity: true),
                        VillageID = c.Int(),
                        SchoolName = c.String(),
                        SchoolTelephone = c.String(),
                        SchoolEmail = c.String(),
                    })
                .PrimaryKey(t => t.SchoolID)
                .ForeignKey("dbo.Village", t => t.VillageID)
                .Index(t => t.VillageID);
            
            CreateTable(
                "dbo.MechanicSchool",
                c => new
                    {
                        MechanicSchoolID = c.Int(nullable: false, identity: true),
                        MechanicID = c.Int(),
                        SchoolID = c.Int(),
                        WorkDayID = c.Int(),
                        UserID = c.Int(),
                    })
                .PrimaryKey(t => t.MechanicSchoolID)
                .ForeignKey("dbo.Mechanic", t => t.MechanicID)
                .ForeignKey("dbo.School", t => t.SchoolID)
                .ForeignKey("dbo.WorkDay", t => t.WorkDayID)
                .Index(t => t.MechanicID)
                .Index(t => t.SchoolID)
                .Index(t => t.WorkDayID);
            
            CreateTable(
                "dbo.CheckIn",
                c => new
                    {
                        CheckInID = c.Int(nullable: false, identity: true),
                        MechanicSchoolID = c.Int(),
                        CheckInStatusID = c.Int(),
                        JobTypeID = c.Int(),
                        CheckInDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.CheckInID)
                .ForeignKey("dbo.CheckInStatu", t => t.CheckInStatusID)
                .ForeignKey("dbo.JobType", t => t.JobTypeID)
                .ForeignKey("dbo.MechanicSchool", t => t.MechanicSchoolID)
                .Index(t => t.MechanicSchoolID)
                .Index(t => t.CheckInStatusID)
                .Index(t => t.JobTypeID);
            
            CreateTable(
                "dbo.CheckInStatu",
                c => new
                    {
                        CheckInStatusID = c.Int(nullable: false, identity: true),
                        CheckInStatus = c.String(),
                    })
                .PrimaryKey(t => t.CheckInStatusID);
            
            CreateTable(
                "dbo.JobType",
                c => new
                    {
                        JobTypeID = c.Int(nullable: false, identity: true),
                        JobType1 = c.String(),
                    })
                .PrimaryKey(t => t.JobTypeID);
            
            CreateTable(
                "dbo.MaintenanceJob",
                c => new
                    {
                        MaintenanceJobID = c.Int(nullable: false, identity: true),
                        BicycleID = c.Int(),
                        MaintenanceJobStatusID = c.Int(),
                        CheckInID = c.Int(),
                        DateScheduled = c.DateTime(),
                        Assigned = c.Boolean(),
                    })
                .PrimaryKey(t => t.MaintenanceJobID)
                .ForeignKey("dbo.Bicycle", t => t.BicycleID)
                .ForeignKey("dbo.CheckIn", t => t.CheckInID)
                .ForeignKey("dbo.MaintenanceJobStatu", t => t.MaintenanceJobStatusID)
                .Index(t => t.BicycleID)
                .Index(t => t.MaintenanceJobStatusID)
                .Index(t => t.CheckInID);
            
            CreateTable(
                "dbo.Bicycle",
                c => new
                    {
                        BicycleID = c.Int(nullable: false, identity: true),
                        BicycleStatusID = c.Int(),
                        DateAdded = c.DateTime(),
                        TagID = c.Int(),
                        QRCode = c.String(),
                        BicycleBrandID = c.Int(),
                    })
                .PrimaryKey(t => t.BicycleID)
                .ForeignKey("dbo.BicycleBrand", t => t.BicycleBrandID)
                .ForeignKey("dbo.BicycleStatu", t => t.BicycleStatusID)
                .ForeignKey("dbo.Tag", t => t.TagID)
                .Index(t => t.BicycleStatusID)
                .Index(t => t.TagID)
                .Index(t => t.BicycleBrandID);
            
            CreateTable(
                "dbo.BicycleBrand",
                c => new
                    {
                        BicycleBrandID = c.Int(nullable: false, identity: true),
                        BicycleBrandName = c.String(),
                    })
                .PrimaryKey(t => t.BicycleBrandID);
            
            CreateTable(
                "dbo.BicyclePart",
                c => new
                    {
                        BicyclePartID = c.Int(nullable: false, identity: true),
                        BicycleID = c.Int(),
                        PartID = c.Int(),
                    })
                .PrimaryKey(t => t.BicyclePartID)
                .ForeignKey("dbo.Bicycle", t => t.BicycleID)
                .ForeignKey("dbo.Part", t => t.PartID)
                .Index(t => t.BicycleID)
                .Index(t => t.PartID);
            
            CreateTable(
                "dbo.Part",
                c => new
                    {
                        PartID = c.Int(nullable: false, identity: true),
                        PartTypeID = c.Int(),
                        PartName = c.String(),
                        PartDescription = c.String(),
                        PartImage = c.Binary(),
                    })
                .PrimaryKey(t => t.PartID)
                .ForeignKey("dbo.PartType", t => t.PartTypeID)
                .Index(t => t.PartTypeID);
            
            CreateTable(
                "dbo.MaintenanceTask",
                c => new
                    {
                        MaintenanceTaskID = c.Int(nullable: false, identity: true),
                        PartID = c.Int(),
                        MaintenanceTaskDescription = c.String(),
                        MaintenanceTask1 = c.String(),
                        MaintenanceTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.MaintenanceTaskID)
                .ForeignKey("dbo.MaintenanceType", t => t.MaintenanceTypeID)
                .ForeignKey("dbo.Part", t => t.PartID)
                .Index(t => t.PartID)
                .Index(t => t.MaintenanceTypeID);
            
            CreateTable(
                "dbo.MaintenanceType",
                c => new
                    {
                        MaintenanceTypeID = c.Int(nullable: false, identity: true),
                        MaintenanceType1 = c.String(),
                        Frequency = c.Int(),
                    })
                .PrimaryKey(t => t.MaintenanceTypeID);
            
            CreateTable(
                "dbo.MaintenanceList",
                c => new
                    {
                        MaintenanceListID = c.Int(nullable: false, identity: true),
                        MaintenanceListStatusID = c.Int(),
                        MaintenanceJobID = c.Int(),
                        MaintenanceTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.MaintenanceListID)
                .ForeignKey("dbo.MaintenanceJob", t => t.MaintenanceJobID)
                .ForeignKey("dbo.MaintenanceListStatu", t => t.MaintenanceListStatusID)
                .ForeignKey("dbo.MaintenanceType", t => t.MaintenanceTypeID)
                .Index(t => t.MaintenanceListStatusID)
                .Index(t => t.MaintenanceJobID)
                .Index(t => t.MaintenanceTypeID);
            
            CreateTable(
                "dbo.MaintenanceListStatu",
                c => new
                    {
                        MaintenanceListStatusID = c.Int(nullable: false, identity: true),
                        MaintenanceListStatus = c.String(),
                    })
                .PrimaryKey(t => t.MaintenanceListStatusID);
            
            CreateTable(
                "dbo.PartFault",
                c => new
                    {
                        PartFaultID = c.Int(nullable: false, identity: true),
                        PartID = c.Int(),
                        FaultTypeID = c.Int(),
                        FaultImage = c.Binary(),
                    })
                .PrimaryKey(t => t.PartFaultID)
                .ForeignKey("dbo.FaultType", t => t.FaultTypeID)
                .ForeignKey("dbo.Part", t => t.PartID)
                .Index(t => t.PartID)
                .Index(t => t.FaultTypeID);
            
            CreateTable(
                "dbo.FaultType",
                c => new
                    {
                        FaultTypeID = c.Int(nullable: false, identity: true),
                        FaultType1 = c.String(),
                        PartNeeded = c.String(),
                    })
                .PrimaryKey(t => t.FaultTypeID);
            
            CreateTable(
                "dbo.JobFault",
                c => new
                    {
                        JobFaultID = c.Int(nullable: false, identity: true),
                        PartFaultID = c.Int(),
                        SectionPartID = c.Int(),
                        RepairJobID = c.Int(),
                        JobFaultStatusID = c.Int(),
                        SectionID = c.Int(),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.JobFaultID)
                .ForeignKey("dbo.JobFaultStatu", t => t.JobFaultStatusID)
                .ForeignKey("dbo.SectionPart", t => t.SectionPartID)
                .ForeignKey("dbo.Section", t => t.SectionID)
                .ForeignKey("dbo.PartFault", t => t.PartFaultID)
                .ForeignKey("dbo.RepairJob", t => t.RepairJobID)
                .Index(t => t.PartFaultID)
                .Index(t => t.SectionPartID)
                .Index(t => t.RepairJobID)
                .Index(t => t.JobFaultStatusID)
                .Index(t => t.SectionID);
            
            CreateTable(
                "dbo.JobFaultStatu",
                c => new
                    {
                        JobFaultStatusID = c.Int(nullable: false, identity: true),
                        JobFaultStatus = c.String(),
                    })
                .PrimaryKey(t => t.JobFaultStatusID);
            
            CreateTable(
                "dbo.JobFaultTask",
                c => new
                    {
                        JobFaultTaskID = c.Int(nullable: false, identity: true),
                        JobFaultID = c.Int(),
                        FaultTaskID = c.Int(),
                        PartUsed = c.Boolean(),
                    })
                .PrimaryKey(t => t.JobFaultTaskID)
                .ForeignKey("dbo.FaultTask", t => t.FaultTaskID)
                .ForeignKey("dbo.JobFault", t => t.JobFaultID)
                .Index(t => t.JobFaultID)
                .Index(t => t.FaultTaskID);
            
            CreateTable(
                "dbo.FaultTask",
                c => new
                    {
                        FaultTaskID = c.Int(nullable: false, identity: true),
                        FaultTaskDescription = c.String(),
                        SectionPartID = c.Int(),
                        FaultTask1 = c.String(),
                    })
                .PrimaryKey(t => t.FaultTaskID)
                .ForeignKey("dbo.SectionPart", t => t.SectionPartID)
                .Index(t => t.SectionPartID);
            
            CreateTable(
                "dbo.SectionPart",
                c => new
                    {
                        SectionPartID = c.Int(nullable: false, identity: true),
                        SectionID = c.Int(),
                        PartID = c.Int(),
                    })
                .PrimaryKey(t => t.SectionPartID)
                .ForeignKey("dbo.Part", t => t.PartID)
                .ForeignKey("dbo.Section", t => t.SectionID)
                .Index(t => t.SectionID)
                .Index(t => t.PartID);
            
            CreateTable(
                "dbo.Section",
                c => new
                    {
                        SectionID = c.Int(nullable: false, identity: true),
                        SectionName = c.String(),
                        SectionDescription = c.String(),
                        SectionImage = c.Binary(),
                    })
                .PrimaryKey(t => t.SectionID);
            
            CreateTable(
                "dbo.BicycleSection",
                c => new
                    {
                        BicycleSectionID = c.Int(nullable: false, identity: true),
                        BicycleID = c.Int(),
                        SectionID = c.Int(),
                    })
                .PrimaryKey(t => t.BicycleSectionID)
                .ForeignKey("dbo.Bicycle", t => t.BicycleID)
                .ForeignKey("dbo.Section", t => t.SectionID)
                .Index(t => t.BicycleID)
                .Index(t => t.SectionID);
            
            CreateTable(
                "dbo.RepairJob",
                c => new
                    {
                        RepairJobID = c.Int(nullable: false, identity: true),
                        RepairJobStatusID = c.Int(),
                        CheckInID = c.Int(),
                        BicycleID = c.Int(),
                        DateReported = c.DateTime(),
                        Assigned = c.Boolean(),
                        DateScheduled = c.DateTime(),
                    })
                .PrimaryKey(t => t.RepairJobID)
                .ForeignKey("dbo.Bicycle", t => t.BicycleID)
                .ForeignKey("dbo.CheckIn", t => t.CheckInID)
                .ForeignKey("dbo.RepairJobStatu", t => t.RepairJobStatusID)
                .Index(t => t.RepairJobStatusID)
                .Index(t => t.CheckInID)
                .Index(t => t.BicycleID);
            
            CreateTable(
                "dbo.RepairJobStatu",
                c => new
                    {
                        RepairJobStatusID = c.Int(nullable: false, identity: true),
                        RepairJobStatus = c.String(),
                    })
                .PrimaryKey(t => t.RepairJobStatusID);
            
            CreateTable(
                "dbo.PartType",
                c => new
                    {
                        PartTypeID = c.Int(nullable: false, identity: true),
                        PartType1 = c.String(),
                    })
                .PrimaryKey(t => t.PartTypeID);
            
            CreateTable(
                "dbo.BicycleStatu",
                c => new
                    {
                        BicycleStatusID = c.Int(nullable: false, identity: true),
                        BicycleStatus = c.String(),
                    })
                .PrimaryKey(t => t.BicycleStatusID);
            
            CreateTable(
                "dbo.BicycleStudent",
                c => new
                    {
                        BicycleStudentID = c.Int(nullable: false, identity: true),
                        BicycleID = c.Int(),
                        StudentID = c.Int(),
                        DateAssigned = c.DateTime(),
                        DateUnassigned = c.DateTime(),
                    })
                .PrimaryKey(t => t.BicycleStudentID)
                .ForeignKey("dbo.Bicycle", t => t.BicycleID)
                .ForeignKey("dbo.Student", t => t.StudentID)
                .Index(t => t.BicycleID)
                .Index(t => t.StudentID);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        StudentID = c.Int(nullable: false, identity: true),
                        SchoolID = c.Int(),
                        VillageID = c.Int(),
                        GuardianID = c.Int(),
                        StudentName = c.String(),
                        StudentSurname = c.String(),
                        StudentDOB = c.DateTime(),
                        PhoneNumber = c.String(),
                        Grade = c.Int(),
                    })
                .PrimaryKey(t => t.StudentID)
                .ForeignKey("dbo.Guardian", t => t.GuardianID)
                .ForeignKey("dbo.School", t => t.SchoolID)
                .ForeignKey("dbo.Village", t => t.VillageID)
                .Index(t => t.SchoolID)
                .Index(t => t.VillageID)
                .Index(t => t.GuardianID);
            
            CreateTable(
                "dbo.AttendanceNotification",
                c => new
                    {
                        NotificationID = c.Int(nullable: false, identity: true),
                        StudentID = c.Int(),
                        DateTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.NotificationID)
                .ForeignKey("dbo.Student", t => t.StudentID)
                .Index(t => t.StudentID);
            
            CreateTable(
                "dbo.Guardian",
                c => new
                    {
                        GuardianID = c.Int(nullable: false, identity: true),
                        GuardianName = c.String(),
                        GuardianEmail = c.String(),
                        GuardianPhone = c.String(),
                    })
                .PrimaryKey(t => t.GuardianID);
            
            CreateTable(
                "dbo.StudentAttendance",
                c => new
                    {
                        StudentAttendanceID = c.Int(nullable: false, identity: true),
                        AttendanceID = c.Int(),
                        StudentID = c.Int(),
                    })
                .PrimaryKey(t => t.StudentAttendanceID)
                .ForeignKey("dbo.Attendance", t => t.AttendanceID)
                .ForeignKey("dbo.Student", t => t.StudentID)
                .Index(t => t.AttendanceID)
                .Index(t => t.StudentID);
            
            CreateTable(
                "dbo.Attendance",
                c => new
                    {
                        AttendanceID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.AttendanceID);
            
            CreateTable(
                "dbo.StudentMark",
                c => new
                    {
                        StudentMarkID = c.Int(nullable: false, identity: true),
                        StudentID = c.Int(),
                        MarkTypeID = c.Int(),
                        Date = c.DateTime(),
                        Mark = c.Double(),
                    })
                .PrimaryKey(t => t.StudentMarkID)
                .ForeignKey("dbo.MarkType", t => t.MarkTypeID)
                .ForeignKey("dbo.Student", t => t.StudentID)
                .Index(t => t.StudentID)
                .Index(t => t.MarkTypeID);
            
            CreateTable(
                "dbo.MarkType",
                c => new
                    {
                        MarkTypeID = c.Int(nullable: false, identity: true),
                        MarkType1 = c.String(),
                    })
                .PrimaryKey(t => t.MarkTypeID);
            
            CreateTable(
                "dbo.Village",
                c => new
                    {
                        VillageID = c.Int(nullable: false, identity: true),
                        SuburbID = c.Int(),
                        VillageName = c.String(),
                    })
                .PrimaryKey(t => t.VillageID)
                .ForeignKey("dbo.Suburb", t => t.SuburbID)
                .Index(t => t.SuburbID);
            
            CreateTable(
                "dbo.Suburb",
                c => new
                    {
                        SuburbID = c.Int(nullable: false, identity: true),
                        CityID = c.Int(),
                        SuburbName = c.String(),
                    })
                .PrimaryKey(t => t.SuburbID)
                .ForeignKey("dbo.City", t => t.CityID)
                .Index(t => t.CityID);
            
            CreateTable(
                "dbo.Antenna",
                c => new
                    {
                        AntennaID = c.Int(nullable: false, identity: true),
                        AntennaStatusID = c.Int(),
                        CoOrdinates = c.String(),
                        DateInstalled = c.DateTime(),
                        SuburbID = c.Int(),
                    })
                .PrimaryKey(t => t.AntennaID)
                .ForeignKey("dbo.AntennaStatu", t => t.AntennaStatusID)
                .ForeignKey("dbo.Suburb", t => t.SuburbID)
                .Index(t => t.AntennaStatusID)
                .Index(t => t.SuburbID);
            
            CreateTable(
                "dbo.AntennaStatu",
                c => new
                    {
                        AntennaStatusID = c.Int(nullable: false, identity: true),
                        AntennaStatus = c.String(),
                    })
                .PrimaryKey(t => t.AntennaStatusID);
            
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        LogID = c.Int(nullable: false, identity: true),
                        TagID = c.Int(),
                        AntennaID = c.Int(),
                        DateTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.LogID)
                .ForeignKey("dbo.Antenna", t => t.AntennaID)
                .ForeignKey("dbo.Tag", t => t.TagID)
                .Index(t => t.TagID)
                .Index(t => t.AntennaID);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        TagStatusID = c.Int(),
                        DateInstalled = c.DateTime(),
                    })
                .PrimaryKey(t => t.TagID)
                .ForeignKey("dbo.TagStatu", t => t.TagStatusID)
                .Index(t => t.TagStatusID);
            
            CreateTable(
                "dbo.TagStatu",
                c => new
                    {
                        TagStatusID = c.Int(nullable: false, identity: true),
                        TagStatus = c.String(),
                    })
                .PrimaryKey(t => t.TagStatusID);
            
            CreateTable(
                "dbo.RouteAntenna",
                c => new
                    {
                        RouteAntennaID = c.Int(nullable: false, identity: true),
                        RouteID = c.Int(),
                        AntennaID = c.Int(),
                        Position = c.Int(),
                    })
                .PrimaryKey(t => t.RouteAntennaID)
                .ForeignKey("dbo.Antenna", t => t.AntennaID)
                .ForeignKey("dbo.Route", t => t.RouteID)
                .Index(t => t.RouteID)
                .Index(t => t.AntennaID);
            
            CreateTable(
                "dbo.Route",
                c => new
                    {
                        RouteID = c.Int(nullable: false, identity: true),
                        SchoolID = c.Int(),
                    })
                .PrimaryKey(t => t.RouteID)
                .ForeignKey("dbo.School", t => t.SchoolID)
                .Index(t => t.SchoolID);
            
            CreateTable(
                "dbo.City",
                c => new
                    {
                        CityID = c.Int(nullable: false, identity: true),
                        ProvinceID = c.Int(),
                        CityName = c.String(),
                    })
                .PrimaryKey(t => t.CityID)
                .ForeignKey("dbo.Province", t => t.ProvinceID)
                .Index(t => t.ProvinceID);
            
            CreateTable(
                "dbo.Province",
                c => new
                    {
                        ProvinceID = c.Int(nullable: false, identity: true),
                        CountryID = c.Int(),
                        ProvinceName = c.String(),
                    })
                .PrimaryKey(t => t.ProvinceID)
                .ForeignKey("dbo.Country", t => t.CountryID)
                .Index(t => t.CountryID);
            
            CreateTable(
                "dbo.Country",
                c => new
                    {
                        CountryID = c.Int(nullable: false, identity: true),
                        CountryName = c.String(),
                    })
                .PrimaryKey(t => t.CountryID);
            
            CreateTable(
                "dbo.MaintenanceJobStatu",
                c => new
                    {
                        MaintenanceJobStatusID = c.Int(nullable: false, identity: true),
                        MaintenanceJobStatus = c.String(),
                    })
                .PrimaryKey(t => t.MaintenanceJobStatusID);
            
            CreateTable(
                "dbo.MaintenanceNotification",
                c => new
                    {
                        NotificationID = c.Int(nullable: false, identity: true),
                        MaintenanceJobID = c.Int(),
                        DateTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.NotificationID)
                .ForeignKey("dbo.MaintenanceJob", t => t.MaintenanceJobID)
                .Index(t => t.MaintenanceJobID);
            
            CreateTable(
                "dbo.Mechanic",
                c => new
                    {
                        MechanicID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        Qualification = c.String(),
                    })
                .PrimaryKey(t => t.MechanicID)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserTypeID = c.Int(),
                        Username = c.String(),
                        Password = c.String(),
                        Firstname = c.String(),
                        Surname = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.UserType", t => t.UserTypeID)
                .Index(t => t.UserTypeID);
            
            CreateTable(
                "dbo.AuditTrail",
                c => new
                    {
                        AuditTrailID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        DateTime = c.DateTime(),
                        TransactionDescription = c.String(),
                    })
                .PrimaryKey(t => t.AuditTrailID)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.UserType",
                c => new
                    {
                        UserTypeID = c.Int(nullable: false, identity: true),
                        UserType1 = c.String(),
                        UserPermissions = c.String(),
                    })
                .PrimaryKey(t => t.UserTypeID);
            
            CreateTable(
                "dbo.WorkDay",
                c => new
                    {
                        WorkDayID = c.Int(nullable: false, identity: true),
                        WorkDay1 = c.String(),
                    })
                .PrimaryKey(t => t.WorkDayID);
            
            CreateTable(
                "dbo.BusinessRule",
                c => new
                    {
                        BusinessRuleID = c.Int(nullable: false, identity: true),
                        BusinessRule1 = c.String(),
                        Period = c.DateTime(),
                    })
                .PrimaryKey(t => t.BusinessRuleID);
            
            CreateTable(
                "dbo.Contract",
                c => new
                    {
                        ContractID = c.Int(nullable: false, identity: true),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ContractID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MechanicSchool", "WorkDayID", "dbo.WorkDay");
            DropForeignKey("dbo.MechanicSchool", "SchoolID", "dbo.School");
            DropForeignKey("dbo.User", "UserTypeID", "dbo.UserType");
            DropForeignKey("dbo.Mechanic", "UserID", "dbo.User");
            DropForeignKey("dbo.AuditTrail", "UserID", "dbo.User");
            DropForeignKey("dbo.Admin", "UserID", "dbo.User");
            DropForeignKey("dbo.MechanicSchool", "MechanicID", "dbo.Mechanic");
            DropForeignKey("dbo.CheckIn", "MechanicSchoolID", "dbo.MechanicSchool");
            DropForeignKey("dbo.MaintenanceNotification", "MaintenanceJobID", "dbo.MaintenanceJob");
            DropForeignKey("dbo.MaintenanceJob", "MaintenanceJobStatusID", "dbo.MaintenanceJobStatu");
            DropForeignKey("dbo.MaintenanceJob", "CheckInID", "dbo.CheckIn");
            DropForeignKey("dbo.MaintenanceJob", "BicycleID", "dbo.Bicycle");
            DropForeignKey("dbo.Village", "SuburbID", "dbo.Suburb");
            DropForeignKey("dbo.Suburb", "CityID", "dbo.City");
            DropForeignKey("dbo.Province", "CountryID", "dbo.Country");
            DropForeignKey("dbo.City", "ProvinceID", "dbo.Province");
            DropForeignKey("dbo.Antenna", "SuburbID", "dbo.Suburb");
            DropForeignKey("dbo.Route", "SchoolID", "dbo.School");
            DropForeignKey("dbo.RouteAntenna", "RouteID", "dbo.Route");
            DropForeignKey("dbo.RouteAntenna", "AntennaID", "dbo.Antenna");
            DropForeignKey("dbo.Tag", "TagStatusID", "dbo.TagStatu");
            DropForeignKey("dbo.Log", "TagID", "dbo.Tag");
            DropForeignKey("dbo.Bicycle", "TagID", "dbo.Tag");
            DropForeignKey("dbo.Log", "AntennaID", "dbo.Antenna");
            DropForeignKey("dbo.Antenna", "AntennaStatusID", "dbo.AntennaStatu");
            DropForeignKey("dbo.Student", "VillageID", "dbo.Village");
            DropForeignKey("dbo.School", "VillageID", "dbo.Village");
            DropForeignKey("dbo.StudentMark", "StudentID", "dbo.Student");
            DropForeignKey("dbo.StudentMark", "MarkTypeID", "dbo.MarkType");
            DropForeignKey("dbo.StudentAttendance", "StudentID", "dbo.Student");
            DropForeignKey("dbo.StudentAttendance", "AttendanceID", "dbo.Attendance");
            DropForeignKey("dbo.Student", "SchoolID", "dbo.School");
            DropForeignKey("dbo.Student", "GuardianID", "dbo.Guardian");
            DropForeignKey("dbo.BicycleStudent", "StudentID", "dbo.Student");
            DropForeignKey("dbo.AttendanceNotification", "StudentID", "dbo.Student");
            DropForeignKey("dbo.BicycleStudent", "BicycleID", "dbo.Bicycle");
            DropForeignKey("dbo.Bicycle", "BicycleStatusID", "dbo.BicycleStatu");
            DropForeignKey("dbo.Part", "PartTypeID", "dbo.PartType");
            DropForeignKey("dbo.PartFault", "PartID", "dbo.Part");
            DropForeignKey("dbo.RepairJob", "RepairJobStatusID", "dbo.RepairJobStatu");
            DropForeignKey("dbo.JobFault", "RepairJobID", "dbo.RepairJob");
            DropForeignKey("dbo.RepairJob", "CheckInID", "dbo.CheckIn");
            DropForeignKey("dbo.RepairJob", "BicycleID", "dbo.Bicycle");
            DropForeignKey("dbo.JobFault", "PartFaultID", "dbo.PartFault");
            DropForeignKey("dbo.JobFaultTask", "JobFaultID", "dbo.JobFault");
            DropForeignKey("dbo.SectionPart", "SectionID", "dbo.Section");
            DropForeignKey("dbo.JobFault", "SectionID", "dbo.Section");
            DropForeignKey("dbo.BicycleSection", "SectionID", "dbo.Section");
            DropForeignKey("dbo.BicycleSection", "BicycleID", "dbo.Bicycle");
            DropForeignKey("dbo.SectionPart", "PartID", "dbo.Part");
            DropForeignKey("dbo.JobFault", "SectionPartID", "dbo.SectionPart");
            DropForeignKey("dbo.FaultTask", "SectionPartID", "dbo.SectionPart");
            DropForeignKey("dbo.JobFaultTask", "FaultTaskID", "dbo.FaultTask");
            DropForeignKey("dbo.JobFault", "JobFaultStatusID", "dbo.JobFaultStatu");
            DropForeignKey("dbo.PartFault", "FaultTypeID", "dbo.FaultType");
            DropForeignKey("dbo.MaintenanceTask", "PartID", "dbo.Part");
            DropForeignKey("dbo.MaintenanceTask", "MaintenanceTypeID", "dbo.MaintenanceType");
            DropForeignKey("dbo.MaintenanceList", "MaintenanceTypeID", "dbo.MaintenanceType");
            DropForeignKey("dbo.MaintenanceList", "MaintenanceListStatusID", "dbo.MaintenanceListStatu");
            DropForeignKey("dbo.MaintenanceList", "MaintenanceJobID", "dbo.MaintenanceJob");
            DropForeignKey("dbo.BicyclePart", "PartID", "dbo.Part");
            DropForeignKey("dbo.BicyclePart", "BicycleID", "dbo.Bicycle");
            DropForeignKey("dbo.Bicycle", "BicycleBrandID", "dbo.BicycleBrand");
            DropForeignKey("dbo.CheckIn", "JobTypeID", "dbo.JobType");
            DropForeignKey("dbo.CheckIn", "CheckInStatusID", "dbo.CheckInStatu");
            DropForeignKey("dbo.AdminSchool", "SchoolID", "dbo.School");
            DropForeignKey("dbo.AdminSchool", "AdminID", "dbo.Admin");
            DropIndex("dbo.AuditTrail", new[] { "UserID" });
            DropIndex("dbo.User", new[] { "UserTypeID" });
            DropIndex("dbo.Mechanic", new[] { "UserID" });
            DropIndex("dbo.MaintenanceNotification", new[] { "MaintenanceJobID" });
            DropIndex("dbo.Province", new[] { "CountryID" });
            DropIndex("dbo.City", new[] { "ProvinceID" });
            DropIndex("dbo.Route", new[] { "SchoolID" });
            DropIndex("dbo.RouteAntenna", new[] { "AntennaID" });
            DropIndex("dbo.RouteAntenna", new[] { "RouteID" });
            DropIndex("dbo.Tag", new[] { "TagStatusID" });
            DropIndex("dbo.Log", new[] { "AntennaID" });
            DropIndex("dbo.Log", new[] { "TagID" });
            DropIndex("dbo.Antenna", new[] { "SuburbID" });
            DropIndex("dbo.Antenna", new[] { "AntennaStatusID" });
            DropIndex("dbo.Suburb", new[] { "CityID" });
            DropIndex("dbo.Village", new[] { "SuburbID" });
            DropIndex("dbo.StudentMark", new[] { "MarkTypeID" });
            DropIndex("dbo.StudentMark", new[] { "StudentID" });
            DropIndex("dbo.StudentAttendance", new[] { "StudentID" });
            DropIndex("dbo.StudentAttendance", new[] { "AttendanceID" });
            DropIndex("dbo.AttendanceNotification", new[] { "StudentID" });
            DropIndex("dbo.Student", new[] { "GuardianID" });
            DropIndex("dbo.Student", new[] { "VillageID" });
            DropIndex("dbo.Student", new[] { "SchoolID" });
            DropIndex("dbo.BicycleStudent", new[] { "StudentID" });
            DropIndex("dbo.BicycleStudent", new[] { "BicycleID" });
            DropIndex("dbo.RepairJob", new[] { "BicycleID" });
            DropIndex("dbo.RepairJob", new[] { "CheckInID" });
            DropIndex("dbo.RepairJob", new[] { "RepairJobStatusID" });
            DropIndex("dbo.BicycleSection", new[] { "SectionID" });
            DropIndex("dbo.BicycleSection", new[] { "BicycleID" });
            DropIndex("dbo.SectionPart", new[] { "PartID" });
            DropIndex("dbo.SectionPart", new[] { "SectionID" });
            DropIndex("dbo.FaultTask", new[] { "SectionPartID" });
            DropIndex("dbo.JobFaultTask", new[] { "FaultTaskID" });
            DropIndex("dbo.JobFaultTask", new[] { "JobFaultID" });
            DropIndex("dbo.JobFault", new[] { "SectionID" });
            DropIndex("dbo.JobFault", new[] { "JobFaultStatusID" });
            DropIndex("dbo.JobFault", new[] { "RepairJobID" });
            DropIndex("dbo.JobFault", new[] { "SectionPartID" });
            DropIndex("dbo.JobFault", new[] { "PartFaultID" });
            DropIndex("dbo.PartFault", new[] { "FaultTypeID" });
            DropIndex("dbo.PartFault", new[] { "PartID" });
            DropIndex("dbo.MaintenanceList", new[] { "MaintenanceTypeID" });
            DropIndex("dbo.MaintenanceList", new[] { "MaintenanceJobID" });
            DropIndex("dbo.MaintenanceList", new[] { "MaintenanceListStatusID" });
            DropIndex("dbo.MaintenanceTask", new[] { "MaintenanceTypeID" });
            DropIndex("dbo.MaintenanceTask", new[] { "PartID" });
            DropIndex("dbo.Part", new[] { "PartTypeID" });
            DropIndex("dbo.BicyclePart", new[] { "PartID" });
            DropIndex("dbo.BicyclePart", new[] { "BicycleID" });
            DropIndex("dbo.Bicycle", new[] { "BicycleBrandID" });
            DropIndex("dbo.Bicycle", new[] { "TagID" });
            DropIndex("dbo.Bicycle", new[] { "BicycleStatusID" });
            DropIndex("dbo.MaintenanceJob", new[] { "CheckInID" });
            DropIndex("dbo.MaintenanceJob", new[] { "MaintenanceJobStatusID" });
            DropIndex("dbo.MaintenanceJob", new[] { "BicycleID" });
            DropIndex("dbo.CheckIn", new[] { "JobTypeID" });
            DropIndex("dbo.CheckIn", new[] { "CheckInStatusID" });
            DropIndex("dbo.CheckIn", new[] { "MechanicSchoolID" });
            DropIndex("dbo.MechanicSchool", new[] { "WorkDayID" });
            DropIndex("dbo.MechanicSchool", new[] { "SchoolID" });
            DropIndex("dbo.MechanicSchool", new[] { "MechanicID" });
            DropIndex("dbo.School", new[] { "VillageID" });
            DropIndex("dbo.AdminSchool", new[] { "AdminID" });
            DropIndex("dbo.AdminSchool", new[] { "SchoolID" });
            DropIndex("dbo.Admin", new[] { "UserID" });
            DropTable("dbo.Contract");
            DropTable("dbo.BusinessRule");
            DropTable("dbo.WorkDay");
            DropTable("dbo.UserType");
            DropTable("dbo.AuditTrail");
            DropTable("dbo.User");
            DropTable("dbo.Mechanic");
            DropTable("dbo.MaintenanceNotification");
            DropTable("dbo.MaintenanceJobStatu");
            DropTable("dbo.Country");
            DropTable("dbo.Province");
            DropTable("dbo.City");
            DropTable("dbo.Route");
            DropTable("dbo.RouteAntenna");
            DropTable("dbo.TagStatu");
            DropTable("dbo.Tag");
            DropTable("dbo.Log");
            DropTable("dbo.AntennaStatu");
            DropTable("dbo.Antenna");
            DropTable("dbo.Suburb");
            DropTable("dbo.Village");
            DropTable("dbo.MarkType");
            DropTable("dbo.StudentMark");
            DropTable("dbo.Attendance");
            DropTable("dbo.StudentAttendance");
            DropTable("dbo.Guardian");
            DropTable("dbo.AttendanceNotification");
            DropTable("dbo.Student");
            DropTable("dbo.BicycleStudent");
            DropTable("dbo.BicycleStatu");
            DropTable("dbo.PartType");
            DropTable("dbo.RepairJobStatu");
            DropTable("dbo.RepairJob");
            DropTable("dbo.BicycleSection");
            DropTable("dbo.Section");
            DropTable("dbo.SectionPart");
            DropTable("dbo.FaultTask");
            DropTable("dbo.JobFaultTask");
            DropTable("dbo.JobFaultStatu");
            DropTable("dbo.JobFault");
            DropTable("dbo.FaultType");
            DropTable("dbo.PartFault");
            DropTable("dbo.MaintenanceListStatu");
            DropTable("dbo.MaintenanceList");
            DropTable("dbo.MaintenanceType");
            DropTable("dbo.MaintenanceTask");
            DropTable("dbo.Part");
            DropTable("dbo.BicyclePart");
            DropTable("dbo.BicycleBrand");
            DropTable("dbo.Bicycle");
            DropTable("dbo.MaintenanceJob");
            DropTable("dbo.JobType");
            DropTable("dbo.CheckInStatu");
            DropTable("dbo.CheckIn");
            DropTable("dbo.MechanicSchool");
            DropTable("dbo.School");
            DropTable("dbo.AdminSchool");
            DropTable("dbo.Admin");
        }
    }
}
