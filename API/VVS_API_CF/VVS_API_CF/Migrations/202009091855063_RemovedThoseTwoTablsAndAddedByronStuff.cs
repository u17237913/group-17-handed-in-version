﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedThoseTwoTablsAndAddedByronStuff : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.JobFault", name: "Section_SectionID", newName: "SectionID");
            RenameIndex(table: "dbo.JobFault", name: "IX_Section_SectionID", newName: "IX_SectionID");
            AddColumn("dbo.AuditTrail", "TransactionType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AuditTrail", "TransactionType");
            RenameIndex(table: "dbo.JobFault", name: "IX_SectionID", newName: "IX_Section_SectionID");
            RenameColumn(table: "dbo.JobFault", name: "SectionID", newName: "Section_SectionID");
        }
    }
}
