﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedTheContractTableToBeDocumentToSupportAfullStandaloneCRUD : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Document",
                c => new
                    {
                        DocumentID = c.Int(nullable: false, identity: true),
                        DocumentName = c.String(),
                        DocumentPath = c.String(),
                    })
                .PrimaryKey(t => t.DocumentID);
            
            DropTable("dbo.Contract");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Contract",
                c => new
                    {
                        ContractID = c.Int(nullable: false, identity: true),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ContractID);
            
            DropTable("dbo.Document");
        }
    }
}
