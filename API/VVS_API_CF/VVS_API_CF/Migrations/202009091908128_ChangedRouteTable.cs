﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedRouteTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Route", "Position1", c => c.Int(nullable: false));
            AddColumn("dbo.Route", "Position2", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Route", "Position2");
            DropColumn("dbo.Route", "Position1");
        }
    }
}
