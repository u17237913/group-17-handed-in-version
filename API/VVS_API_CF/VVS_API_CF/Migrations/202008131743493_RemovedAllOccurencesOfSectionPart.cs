﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedAllOccurencesOfSectionPart : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FaultTask", "SectionPartID", "dbo.SectionPart");
            DropForeignKey("dbo.JobFault", "SectionPartID", "dbo.SectionPart");
            DropForeignKey("dbo.SectionPart", "PartID", "dbo.Part");
            DropForeignKey("dbo.SectionPart", "SectionID", "dbo.Section");
            DropIndex("dbo.JobFault", new[] { "SectionPartID" });
            DropIndex("dbo.FaultTask", new[] { "SectionPartID" });
            DropIndex("dbo.SectionPart", new[] { "SectionID" });
            DropIndex("dbo.SectionPart", new[] { "PartID" });
            RenameColumn(table: "dbo.JobFault", name: "SectionID", newName: "Section_SectionID");
            RenameIndex(table: "dbo.JobFault", name: "IX_SectionID", newName: "IX_Section_SectionID");
            AddColumn("dbo.JobFault", "PartID", c => c.Int(nullable: false));
            AddColumn("dbo.FaultTask", "PartID", c => c.Int(nullable: false));
            CreateIndex("dbo.JobFault", "PartID");
            CreateIndex("dbo.FaultTask", "PartID");
            AddForeignKey("dbo.FaultTask", "PartID", "dbo.Part", "PartID", cascadeDelete: true);
            AddForeignKey("dbo.JobFault", "PartID", "dbo.Part", "PartID", cascadeDelete: true);
            DropColumn("dbo.FaultTask", "SectionPartID");
            DropTable("dbo.SectionPart");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SectionPart",
                c => new
                    {
                        SectionPartID = c.Int(nullable: false, identity: true),
                        SectionID = c.Int(),
                        PartID = c.Int(),
                    })
                .PrimaryKey(t => t.SectionPartID);
            
            AddColumn("dbo.FaultTask", "SectionPartID", c => c.Int());
            DropForeignKey("dbo.JobFault", "PartID", "dbo.Part");
            DropForeignKey("dbo.FaultTask", "PartID", "dbo.Part");
            DropIndex("dbo.FaultTask", new[] { "PartID" });
            DropIndex("dbo.JobFault", new[] { "PartID" });
            DropColumn("dbo.FaultTask", "PartID");
            DropColumn("dbo.JobFault", "PartID");
            RenameIndex(table: "dbo.JobFault", name: "IX_Section_SectionID", newName: "IX_SectionID");
            RenameColumn(table: "dbo.JobFault", name: "Section_SectionID", newName: "SectionID");
            CreateIndex("dbo.SectionPart", "PartID");
            CreateIndex("dbo.SectionPart", "SectionID");
            CreateIndex("dbo.FaultTask", "SectionPartID");
            CreateIndex("dbo.JobFault", "SectionPartID");
            AddForeignKey("dbo.SectionPart", "SectionID", "dbo.Section", "SectionID");
            AddForeignKey("dbo.SectionPart", "PartID", "dbo.Part", "PartID");
            AddForeignKey("dbo.JobFault", "SectionPartID", "dbo.SectionPart", "SectionPartID");
            AddForeignKey("dbo.FaultTask", "SectionPartID", "dbo.SectionPart", "SectionPartID");
        }
    }
}
