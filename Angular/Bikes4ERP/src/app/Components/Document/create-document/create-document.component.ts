import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, NgModel } from '@angular/forms';
import { DocumentService } from 'src/app/Services/Document/document.service';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Document } from 'src/app/Classes/Document/document';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';
import { take } from 'rxjs/operators';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: 'app-create-document',
  templateUrl: './create-document.component.html',
  styleUrls: ['./create-document.component.css']
})
export class CreateDocumentComponent implements OnInit {
// @ViewChild('DocumentUploaded') IDRef : NgModel;
@Input() progress

  constructor(
    private formBuilder: FormBuilder,
    private DocumentService: DocumentService,
    private uploadService: FileUploadServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  
//DocumentForm : any;
  ID: number;
  data = false;
  message: string;
  usertype : boolean = false;
  // ngModel: any;

    //declare the max file size
  public MAX_SIZE: number = 10048576; //BE CAREFUL, extra zero included!

  //declare a file variable, this is for the initial upload
  theFile: any = null;

  //variable for error messages - note
  messages: string[] = [];

  //Initialize FormGroup ting
  DocumentForm = new FormGroup({
    DocumentName: new FormControl(''),
    image: new FormControl(''),
  });

  ngOnInit(): void {
    //For the Create object
    this.DocumentForm = this.formBuilder.group({
      DocumentName: ['', [Validators.required]],
      //image: ['', Validators.required, requiredFileType('png')],
      image: ['', [RxwebValidators.extension({extensions:['pdf']})
    ]],
    });

    var ut = localStorage.getItem("UserType");

    if(ut == "Administrator") {
      this.usertype == false;
    }
    else { this.router.navigate(['/homepage']); }
  } //ngOnInit

//this event is called from the html file on the file upload element
  onFileChange(event) {
    this.theFile = null;

    //check that there is a file and the file is not 0 bits
    if (event.target.files && event.target.files.length > 0) {
      //Disallow the uploading of files larger than 10MB.
      if (event.target.files[0].size < this.MAX_SIZE) {
        //set theFile property
        this.theFile = event.target.files[0];
      }
    }
  } //OnFileChange

  // validateFile() {
  //   let path = this.IDRef.model;
  //   let patharray = path.split('/');
  //   let filename = patharray[patharray.length -1];
  //   let extension = filename.split('.').pop();
  //   if(extension != 'pdf') {
  //     this.IDRef.control.setErrors({'filetype': true});
  //   }
  // }

  //this function is now called when the submit button is clicked
  uploadFile(): void {
    this.readAndUploadFile(this.theFile);
  }

  //this function converts to base 64 and makes the service call, which sends stuff to the API bla bla bla
  private readAndUploadFile(theFile: any) {
    //file to upload model
    let file = new FileToUpload();

    //Set File Information
    file.fileName = theFile.name;

    //Use FileReader() object to get file to upload <-!-!-> NOTE: FileReader only works with newer browsers
    let reader = new FileReader();

    //Setup onload event for reader
    reader.onload = () => {
      //Store base64 encoded representation of file
      file.fileAsBase64 = reader.result.toString();

      //POST to server
      this.uploadService.uploadDocument(file).subscribe((respondation) => {
        //this.messages.push('Upload complete');
      });
    };

    //Read the file
    reader.readAsDataURL(theFile);

    //Nav back to the list
    // this.router.navigate(['/document']);
  }

  onFormSubmit(DocumentForm) {
    const document = DocumentForm.value;
    this.CreateDocument(document);
  } //OnFormSubmit

  CreateDocument(document: Document) {
    //inject the services
    debugger;
    this.DocumentService.addDocument(document).subscribe(() => {
      this.data = true;
      this.uploadFile();

      this.message = 'Document has been added bro! good job...';
      this.DocumentForm.reset;
      console.log(this.DocumentForm.value + 'we adding');
      //this.router.navigate(['/document']);
    }); //addDocument
  } //CreateDocument

  GoToList() {
    this.router.navigate(['/document']);
  } //GoToList

} //OnInit