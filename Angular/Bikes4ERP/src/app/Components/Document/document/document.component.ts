import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DocumentService } from 'src/app/Services/Document/document.service';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Document } from 'src/app/Classes/Document/document';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  DocumentArray: Document[] = [];
  data = false;
  mySubscription: any;
  usertype : boolean = false;
  sos : string;
  fileToUpload: File = null;
  thefile: FileToUpload = new FileToUpload();

  //search stuff
  public TempArray: Document[] = [];
  public SearchArray: Document[] = [];
  isArrayStored: boolean = false;

  constructor(private router: Router, private DocumentService: DocumentService, private uploadService: FileUploadServiceService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
// debugger
    this.DocumentService.getDocuments().subscribe(data => data.forEach(element => { 
      var document = new Document();
      document.DocumentID = element.DocumentID;
      document.DocumentName = element.DocumentName;
      document.DocumentPath = element.DocumentPath;

        this.DocumentArray.push(document);
      })
    );

    var ut = localStorage.getItem("UserType");

    if(ut == "School Administrator") {
      this.usertype = true;
    }
    else if(ut == "Administrator") {
      this.usertype == false;
    }
    else { this.router.navigate(['/homepage']); }

    //Blink. lol
    setTimeout(( ) => { 
      window.location.reload() }, 2000);

  } //ngOnInit

  DownloadDoc(ids : number) {
    //inject service call to get current object details
    this.uploadService.getDocument(ids).subscribe((data) => {
      this.thefile = data;

      //this.Base64File = data.fileAsBase64;
      this.sos = this.thefile.fileName + ".pdf";

      //this.message = 'Document has been successfully loaded boiii !';
      //stuff to download the file
      const linkSource = `data:application/pdf;base64,${this.thefile.fileAsBase64}`;
      const downloadLink = document.createElement("a");
      const fileName = this.thefile.fileName;
  
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
      });//GetDocument
    }

  EditDocument(ids: number) {
    this.router.navigate(['/viewdocument', { id: ids }]);
  } //EditDocument

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    } //Endif
  } //ngOnDestroy

  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.DocumentArray.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.DocumentArray = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.DocumentArray.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.DocumentArray.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.DocumentName.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.DocumentArray = this.SearchArray;
    } else {
      this.DocumentArray = this.TempArray.slice();
      this.SearchArray = [];
    }
  }

} //OnInit