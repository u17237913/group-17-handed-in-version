import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { DocumentService } from 'src/app/Services/Document/document.service';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';
import { take } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Document } from 'src/app/Classes/Document/document';

@Component({
  selector: 'app-view-document',
  templateUrl: './view-document.component.html',
  styleUrls: ['./view-document.component.css']
})
export class ViewDocumentComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private DocumentService: DocumentService,
    private uploadService: FileUploadServiceService,
    private sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ID: number;
  data = false;
  message: string;
  sos : string;
  fileToUpload: File = null;
  thefile: FileToUpload = new FileToUpload();
  oldData: any;
  usertype : boolean = false;

  //declare the max file size
  public MAX_SIZE: number = 10048576; //BE CAREFUL, extra zero included!

  //declare a file variable, this is for the initial upload
  theFile: any;

  //If a doc from the API is available, then set this to true so it can
  //be used in the transformer function
  isDocumentAvailable = false;
  Base64File: string;

  //variable for error messages - note
  messages: string[] = [];

  DocumentForm = new FormGroup({
    DocumentID: new FormControl(),
    DocumentName: new FormControl(),
    // DocumentPath: new FormControl(),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.ID = id;
    });

    var ut = localStorage.getItem("UserType");

    if(ut == "Administrator") {
      this.usertype == false;
    }
    else { this.router.navigate(['/homepage']); }

    //inject service call to get current object details
    this.DocumentService.getDocuments().subscribe((data) =>
      data.forEach((element) => {
        if (element.DocumentID == this.ID) {
          this.DocumentForm = this.formBuilder.group({
            DocumentID: [element.DocumentID, Validators.required],
            DocumentName: [element.DocumentName, [Validators.required]],
            // DocumentPath: [element.DocumentPath, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

  } //ngOnInit

  //this event is called from the html file on the file upload element
  onFileChange(event) {
    this.theFile = null;

    //check that there is a file and the file is not 0 bits
    if (event.target.files && event.target.files.length > 0) {
      //Disallow the uploading of files larger than 10MB.
      if (event.target.files[0].size < this.MAX_SIZE) {
        //set theFile property
        this.theFile = event.target.files[0];

      } else {
        //Display error message <-!-!-> watchout, this allows for multiple files...
        //this.messages.push(
          alert('File: ' + event.target.files[0].name + 'is too large to upload.');
        //);
      }
    }
  } //OnFileChange

  //This function will convert base 64 string to a doc and return it
  transformer() {
    if (this.isDocumentAvailable) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        'data:application/pdf;base64,' + this.Base64File
      );
    }
    // else { this.imageUrl = '../../../assets/images/img-placeholder.jpg'; }
  }

  //this function is now called when the submit button is clicked
  uploadFile(): void {
    this.readAndUploadFile(this.theFile, this.ID);
  }

  //this function converts to base 64 and makes the service call, which sends stuff to the API bla bla bla
  private readAndUploadFile(theFile: any, id: number) {
    //file to upload model
    let file = new FileToUpload();

    //Set File Information
    file.fileName = theFile.name;

    //Use FileReader() object to get file to upload <-!-!-> NOTE: FileReader only works with newer browsers
    let reader = new FileReader();

    //Setup onload event for reader
    reader.onload = () => {
      //Store base64 encoded representation of file
      file.fileAsBase64 = reader.result.toString();

      //POST to server
      this.uploadService.updateDocument(file, id).subscribe((respondation) => {
        this.messages.push('Upload complete');
      });
    };

    //Read the file
    reader.readAsDataURL(theFile);

    //Nav back to the list
    this.router.navigate(['/document']);
  }

  onFormSubmit(DocumentForm) {
    const document = DocumentForm.value;
    this.EditDocument(document);
  } //OnFormSubmit

  EditDocument(document: Document) {
    this.DocumentService.UpdateDocument(document, this.oldData).subscribe(() => {
      this.data = true;
      if (this.theFile != null) {
        this.uploadFile();
      } else {
        //this.router.navigate(['/document']);
      }

      this.message = 'Document has been successfully updated boiii !';
      this.DocumentForm.reset;
      console.log(this.DocumentForm.value + 'we adding');

      this.router.navigate(['/document']);
    }); //UpdateDocument
  } //EditDocument

  DownloadDoc() {
    //inject service call to get current object details
    this.uploadService.getDocument(this.ID).subscribe((data) => {
      this.thefile = data;

      this.Base64File = data.fileAsBase64;
      this.sos = this.thefile.fileName + ".pdf";

      this.message = 'Document has been successfully loaded boiii !';
      //stuff to download the file
      const linkSource = `data:application/pdf;base64,${this.thefile.fileAsBase64}`;
      const downloadLink = document.createElement("a");
      const fileName = this.thefile.fileName;
  
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
      });//GetDocument

}

  DeleteDocument(id: number) {
    id = this.ID;
    this.DocumentService.DeleteDocument(id, this.oldData).subscribe(() => {
      this.data = true;

      this.router.navigate(['/document']);
    });
  } //DeleteDocument

  GoToList() {
    this.router.navigate(['/document']);
  } //GoToList

} //OnInit