import { Component, OnInit } from '@angular/core';
import { Qrscan } from '../qr-scan/qrscan.service';
import { QRCodeModule } from 'angular2-qrcode';
import { FormsModule } from '@angular/forms';
import { BarcodeFormat } from '@zxing/library';
import { from } from 'rxjs';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { Injectable } from '@angular/core';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { ActivatedRoute } from '@angular/router';
import { Router, ParamMap } from '@angular/router';
import { getWeekYearWithOptions } from 'date-fns/fp';
import * as Bootstrap from '../../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import { CheckIn } from 'src/app/Classes/CheckIn/check-in';
import { RepairJob } from 'src/app/Classes/RepairJob/repair-job';
import { Location } from '@angular/common';

interface BicycleJobData {
  JobType: string;
  BicycleID: number;
  Error: string;
}

@Component({
  selector: 'app-qr-scan',
  templateUrl: './qr-scan.component.html',
  styleUrls: ['./qr-scan.component.css'],
})
export class QrScanComponent implements OnInit {
  qrdata: string;
  public level: 'L' | 'M' | 'Q' | 'H';
  public width: number;
  public height: number;
  display = false;
  href: string;
  message: string;
  elementType: 'url' | 'canvas' | 'img' = 'url';

  scannedID: number;

  //bicycleJobData : BicycleJobData = null;

  //QR Reader
  availableDevices: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo = null;
  formatsEnabled: BarcodeFormat = BarcodeFormat.QR_CODE;
  hasDevices: boolean;
  hasPermission: boolean;

  // downloadQRcode(){
  //   this.href = document.getElementsByTagName('img')[0].src;
  // }

  constructor(
    private qrService: Qrscan,
    private router: Router,
    private location: Location
  ) {
    this.level = 'H';
    this.width = 100;
  }

  ngOnInit(): void {

    //Play the audio feed
    this.playAudio();
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/ScanQRCode.mp3";
    audio.load();
    audio.play();
  }

  getQRcode(resultString: number) {
    this.qrService.scannedID = resultString;
    this.scannedID = resultString;

    var bicycleJobData: BicycleJobData = {
      JobType: '',
      Error: '',
      BicycleID: 0,
    };

    this.qrService.getQRcode().subscribe((bicycle) => {
      if (!bicycle.Error) {
        bicycleJobData.BicycleID = bicycle.BicycleID;
        bicycleJobData.JobType = bicycle.JobType;
      } else {
        bicycleJobData.Error = bicycle.Error;
      }
      console.clear();
      console.log(bicycle);

      if (!bicycleJobData.Error) {
        if (bicycleJobData.JobType == 'Maintenance') {
          this.message = 'Check-In Successful';
          var myModal = new Bootstrap.Modal(document.getElementById('modal2'));
          myModal.show();
          //this.router.navigate(['joblist'])
        } else {
          // this.router.navigate(['Viewjob'])
          this.message = 'Check-In Successful';
          var myModal = new Bootstrap.Modal(document.getElementById('modal2'));
          myModal.show();
          //this.router.navigate(['joblist', { id : this.scannedID }])
        }
      } // If !this.bicycleJobData.Error
      else {
        bicycleJobData.JobType == 'No Job';

        //this.router.navigate(['bicycleblueprint'])
        this.message = 'Check-In Failed';
        var myModal = new Bootstrap.Modal(
          document.getElementById('exampleModalCenter')
        );
        myModal.show();
        //this.router.navigate(['bicycleblueprint', { id : this.scannedID }])
      }
    }); // End of Subscription

    function redirect1() {}
  } // getQRcode

  NavigateToJobListID() {
    var newCheckIn = new CheckIn();
    newCheckIn.JobTypeID = 1;
    newCheckIn.CheckInStatusID = 1;

    this.qrService.CreateCheckIn(newCheckIn, this.scannedID).subscribe(
      (checkIn) => {
        console.clear();
        console.log(checkIn);

        var newRepairJob = new RepairJob();
        newRepairJob.RepairJobStatusID = 1;
        newRepairJob.CheckInID = checkIn.CheckInID;
        newRepairJob.BicycleID = this.scannedID;
        newRepairJob.DateReported = new Date().toDateString();
        newRepairJob.Assigned = true;

        this.qrService
          .CreateRepairJob(newRepairJob)
          .subscribe((newRepairJob) => {
            this.router.navigate([
              'bicycleblueprint',
              { RepairJobID: newRepairJob.RepairJobID },
            ]);
          });
      },
      (error) => {}
    );
  }

  NavigateToJobList() {
    this.router.navigate(['joblist', { id: this.scannedID }]);
  }

  // NavaigateToBBP(){
  //     this.router.navigate(['bicycleblueprint'])
  // }

  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }

  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }

  goBack() {
    this.location.back();
  }
}
