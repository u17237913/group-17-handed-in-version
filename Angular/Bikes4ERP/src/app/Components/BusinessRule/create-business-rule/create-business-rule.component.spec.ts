import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBusinessRuleComponent } from './create-business-rule.component';

describe('CreateBusinessRuleComponent', () => {
  let component: CreateBusinessRuleComponent;
  let fixture: ComponentFixture<CreateBusinessRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBusinessRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBusinessRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
