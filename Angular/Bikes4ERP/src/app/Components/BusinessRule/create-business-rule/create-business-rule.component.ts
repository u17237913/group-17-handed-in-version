import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { BusinessRuleService } from 'src/app/Services/BusinessRule/business-rule.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BusinessRule } from 'src/app/Classes/BusinessRule/business-rule';

@Component({
  selector: 'app-create-business-rule',
  templateUrl: './create-business-rule.component.html',
  styleUrls: ['./create-business-rule.component.css']
})
export class CreateBusinessRuleComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private BusinessRuleService: BusinessRuleService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
//BusinessRuleForm : any;
  ID: number;
  data = false;
  message: string;

  //Initialize FormGroup ting
  BusinessRuleForm = new FormGroup({
    BusinessRule1: new FormControl(''),
    Value: new FormControl(''),
  });

  ngOnInit(): void {
    //For the Create object
    this.BusinessRuleForm = this.formBuilder.group({
      BusinessRule1: ['', [Validators.required]],
      Value: ['', [Validators.required]],
    });
  } //ngOnInit

  onFormSubmit(BusinessRuleForm) {
    // debugger;
    const businessrule = BusinessRuleForm.value;
    this.CreateBusinessRule(businessrule);
  } //OnFormSubmit

  CreateBusinessRule(businessrule: BusinessRule) {
    //inject the services
    // debugger;
    this.BusinessRuleService.addBusinessRule(businessrule).subscribe(() => {
      this.data = true;

      this.message = 'BusinessRule has been added bro! good job...';
      this.BusinessRuleForm.reset;
      console.log(this.BusinessRuleForm.value + 'we adding');

      this.router.navigate(['/businessrule']);
    }); //addBusinessRule
  } //CreateBusinessRule

  GoToList() {
    this.router.navigate(['/businessrule']);
  } //GoToList
} //OnInit