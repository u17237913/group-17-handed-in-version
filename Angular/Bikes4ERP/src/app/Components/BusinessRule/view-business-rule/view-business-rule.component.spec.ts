import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBusinessRuleComponent } from './view-business-rule.component';

describe('ViewBusinessRuleComponent', () => {
  let component: ViewBusinessRuleComponent;
  let fixture: ComponentFixture<ViewBusinessRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBusinessRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBusinessRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
