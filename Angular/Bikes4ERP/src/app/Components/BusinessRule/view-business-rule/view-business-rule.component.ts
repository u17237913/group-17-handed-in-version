import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { BusinessRuleService } from 'src/app/Services/BusinessRule/business-rule.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BusinessRule } from 'src/app/Classes/BusinessRule/business-rule';

@Component({
  selector: 'app-view-business-rule',
  templateUrl: './view-business-rule.component.html',
  styleUrls: ['./view-business-rule.component.css']
})
export class ViewBusinessRuleComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private BusinessRuleService: BusinessRuleService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ID: number;
  data = false;
  message: string;
  oldData: any;

  BusinessRuleForm = new FormGroup({
    BusinessRuleID: new FormControl(),
    BusinessRule1: new FormControl(),
    Value: new FormControl(),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.ID = id;
    });

    //inject service call to get current object details
    this.BusinessRuleService.getBusinessRules().subscribe((data) =>
      data.forEach((element) => {
        if (element.BusinessRuleID == this.ID) {
          this.BusinessRuleForm = this.formBuilder.group({
            BusinessRuleID: [element.BusinessRuleID, Validators.required],
            BusinessRule1: [element.BusinessRule1, [Validators.required]],
            Value: [element.Value, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

  } //ngOnInit

  onFormSubmit(BusinessRuleForm) {
    const businessrule = BusinessRuleForm.value;
    this.EditBusinessRule(businessrule);
  } //OnFormSubmit

  EditBusinessRule(businessrule: BusinessRule) {
    this.BusinessRuleService.UpdateBusinessRule(businessrule, this.oldData).subscribe(() => {
      this.data = true;

      this.message = 'Document has been successfully updated boiii !';
      this.BusinessRuleForm.reset;
      console.log(this.BusinessRuleForm.value + 'we adding');

      this.router.navigate(['/businessrule']);
    }); //UpdateBusinessRule
  } //EditBusinessRule

  DeleteBusinessRule(id: number) {
    id = this.ID;
    this.BusinessRuleService.DeleteBusinessRule(id, this.oldData).subscribe(() => {
      this.data = true;

      this.router.navigate(['/businessrule']);
    });
  } //DeleteBusinessRule

  GoToList() {
    this.router.navigate(['/businessrule']);
  } //GoToList

} //OnInit