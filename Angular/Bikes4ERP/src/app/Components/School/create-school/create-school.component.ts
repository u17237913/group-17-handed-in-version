import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SchoolService } from 'src/app/Services/School/school.service';
import { School } from 'src/app/Classes/School/school';
import { Village } from 'src/app/Classes/Village/village';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';

@Component({
  selector: 'app-create-school',
  templateUrl: './create-school.component.html',
  styleUrls: ['./create-school.component.css'],
})
export class CreateSchoolComponent implements OnInit {
  LocationList: LocationViewModel[] = [];
  SchoolList: School[] = [];
  VillageList: Village[] = [];
  Form: any;
  data = false;
  message: string;
  public values = [];
  ngModel: any;

  constructor(
    public route: Router,
    public schoolService: SchoolService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // this.GetVillages();

    this.GetLocations();

    this.schoolService.SchoolForm = {
      ID: 0,
      Village: 1,
      SchoolTelephone: '',
      SchoolEmail: '',
      SchoolName: '',
    }; // this.service
  }

  SaveSchool() {
    this.schoolService
      .AddSchool(this.schoolService.SchoolForm)
      .subscribe((res) => {});
  }
  GetLocations() {
    this.schoolService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    });
  }

  onFormSubmit(SchoolForm) {
    // this.CreateSchool(SchoolForm);
  }

  // CreateSchool(school: School) {
  //   this.schoolService.AddSchool(school).subscribe(() => {
  //     this.data = true;
  //     this.message = 'Saved';
  //     //  this.onFormSubmit.reset();
  //   });
  // }
  GoBack() {
    this.router.navigate(['/school']);
  }
}
