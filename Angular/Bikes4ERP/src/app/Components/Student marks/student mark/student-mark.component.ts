import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router'
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';
import { StudentMark } from 'src/app/Classes/StudentMark/student-mark.js';
import { StudentMarksService } from 'src/app/Services/StudentMarks/student-marks.service.js';
import { Student } from 'src/app/Classes/Student/student.js';
import { StudentService } from 'src/app/Services/Student/student.service.js';
import { StudentMarksVM } from 'src/app/Classes/StudentMark/student-marks-vm.js';

@Component({
  selector: 'app-student-mark',
  templateUrl: './student-mark.component.html',
  styleUrls: ['./student-mark.component.css']
})
export class StudentMarkComponent implements OnInit {

  MarkVM : StudentMarksVM [] = [];
  public values = [];
  StudentMarkForm : StudentMark;
  public TempArray: StudentMarksVM[] = [];
  public SearchArray: StudentMarksVM[] = [];
  isArrayStored: boolean = false;

  constructor (public route : Router, private studentmarkService : StudentMarksService, public studentservice : StudentService) { }

  ngOnInit(): void {
    this.GetAllStudents();
  }

  GetAllStudents() {
    this.studentmarkService.GetStudentAllMarks().subscribe((data) => {
      this.MarkVM = data as StudentMarksVM[];
      console.log(this.MarkVM, 'list');
    });
  }

  // GetMarks (){
  //   this.studentmarkService.GetStudentMarks().subscribe((data) =>{
  //     this.StudentMarkList = data as StudentMark[];
  //   })
  // }

  CaptureMark(ids : number){
    this.route.navigate(['/capturemark', {id : ids}])
  }

  SearchStudents(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.MarkVM.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.MarkVM = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.MarkVM.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.MarkVM.forEach((student) => {
        var SearchString: string = event.target.value;
        if (
          student.StudentName.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(student);
        }
      }); // For each
*/

      this.MarkVM = this.SearchArray;
    } else {
      this.MarkVM = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}