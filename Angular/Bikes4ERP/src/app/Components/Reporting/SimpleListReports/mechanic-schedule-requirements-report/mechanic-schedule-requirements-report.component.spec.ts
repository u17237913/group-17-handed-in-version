import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicScheduleRequirementsReportComponent } from './mechanic-schedule-requirements-report.component';

describe('MechanicScheduleRequirementsReportComponent', () => {
  let component: MechanicScheduleRequirementsReportComponent;
  let fixture: ComponentFixture<MechanicScheduleRequirementsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicScheduleRequirementsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicScheduleRequirementsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
