import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartsUsedReportComponent } from './parts-used-report.component';

describe('PartsUsedReportComponent', () => {
  let component: PartsUsedReportComponent;
  let fixture: ComponentFixture<PartsUsedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartsUsedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartsUsedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
