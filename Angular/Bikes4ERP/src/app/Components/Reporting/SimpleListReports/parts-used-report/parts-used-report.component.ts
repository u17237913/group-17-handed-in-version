import { Component, OnInit } from '@angular/core';
import { ReportingServiceService } from 'src/app/Services/Reporting/reporting-service.service';
import * as XLSX from 'xlsx';
import { School } from 'src/app/Classes/School/school';
import { take } from 'rxjs/operators';
import { jsPDF } from "jspdf";
import 'jspdf-autotable';
import { User } from 'src/app/Services/user.model';

import * as autoTable from 'jspdf-autotable'

import "jspdf/dist/polyfills.es.js";

@Component({
  selector: 'app-parts-used-report',
  templateUrl: './parts-used-report.component.html',
  styleUrls: ['./parts-used-report.component.css']
})
export class PartsUsedReportComponent implements OnInit {
  title = 'VVS - Parts Used Report';
  start : Date;
  end : Date;
  selectedUser : any;
  showErrorMessage : boolean = false;
  TableData : any;
  LilTableData : any;
  totalJobs : number;
  liltotalJobs : number;
  Users : User[] = [];
  SchoolID : any;
  counter : any;

  constructor(private ReportingService : ReportingServiceService) { }

  ngOnInit(): void { 
    var tableWithData = document.getElementById('table');
    var liltable2WithData = document.getElementById('table2');

    tableWithData.hidden = true;
    liltable2WithData.hidden = true;
    this.GetMechanics();
    this.counter = 0;

  }

  GetMechanics() {
    this.ReportingService.getMechanics().pipe(take(1)).subscribe((Mechanic) => {
      this.Users = Mechanic;
      console.log(this.Users);
    })
  }
  // GetSchools() {
  //   this.ReportingService.getSchools().pipe(take(1)).subscribe((School) => {
  //     this.Schools = School;
  //   })
  // }

  /*name of the excel-file which will be downloaded. */ 
  excelfileName= 'PartsUsedReport.xlsx';  

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    let element2 = document.getElementById('table2');
    const ws2: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element2);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    //Testing if there are values for the report tables or not
    if (this.totalJobs > 0) { XLSX.utils.book_append_sheet(wb, ws, 'Main Table');
      if (this.liltotalJobs > 0) { XLSX.utils.book_append_sheet(wb, ws2, 'Sub table'); }

      /* save to file */
      XLSX.writeFile(wb, this.excelfileName);
    }
    else if (this.totalJobs == 0){ 
      alert('No Parts Used Report Data within the chosen dates.\n Please capture new dates and try again.'); 
    }
  }//exportexcel

  downloadPDF() {
    //this.ReportingService.getPartsUsedReportData(this.start, this.end).subscribe((res) => {

      let doc = new jsPDF('l', 'pt', 'a4');
  
     //Gets the height and width of the PDF document
      //var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
      //var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

      let finalY = 100;

      doc.setFontSize(20)
      doc.text("Parts Used Report", (150/3)-5,15)

      doc.setFontSize(14)

        //@ts-ignore
        doc.autoTable({startY : finalY + 15, html:'#table',useCss:true/*, head: [
        ['Job', 'Bicycle Code', 'Student Name', 'Job Code', 'Date Completed']]*/})

        //@ts-ignore
          finalY = doc.autoTable.previous.finalY;

          doc.save('PartsUsedReport.pdf');
      //});
  }

  GenerateReport() {
    //check if an option is selected otherwise Show error message
    if(this.start == undefined || this.end == undefined) {
      this.showErrorMessage == true;
    } else {
      this.showErrorMessage = false;

      this.ReportingService.getPartsUsedReportData(this.start, this.end, this.selectedUser).subscribe((res) => {
        console.log(res);
        this.TableData = res['TableData'];
        this.totalJobs = res['TableData'].length;
      })

      this.ReportingService.getPartsUsedRepLilTable(this.start, this.end).subscribe((res) => {
        console.log(res);
        this.LilTableData = res['LilTableData'];
        this.liltotalJobs = res['LilTableData'].length;
      })
    }

    //Testing if there are values for the report tables or not
      if (this.totalJobs > 0 && this.liltotalJobs > 0) {
      var tableWithData = document.getElementById('table');
      var liltable2WithData = document.getElementById('table2');

      tableWithData.hidden = false;
      liltable2WithData.hidden = false;
      }
      if (this.totalJobs > 0 && this.liltotalJobs == 0) {
        var tableWithData = document.getElementById('table');
        var liltable2WithData = document.getElementById('table2');

        tableWithData.hidden = false;
        liltable2WithData.hidden = true;
        console.log(this.liltotalJobs, this.totalJobs)
        }
      else if (this.totalJobs == 0 || this.LilTableData == 0) { 
        var tableWithData = document.getElementById('table');
        var liltable2WithData = document.getElementById('table2');

        tableWithData.hidden = true;
        liltable2WithData.hidden = true;

        alert('No Parts Used Report Data within the chosen dates.\n Please capture new dates and try again.'); }

      if(this.counter==0) {
        setTimeout(( ) => { 
          console.log('ItS BeEn 3 SeCoNdS');
          document.getElementById('generaterTing').click(); }, 3000);
        this.counter++;
      }
      // if (this.liltotalJobs > 0) {
      //   var liltable2WithData = document.getElementById('table2');

      //   liltable2WithData.hidden = false;
      //   }
      //   else { liltable2WithData.hidden = true; }
  }//EndOfGenerateReport

}//OnInit