import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicScheduleReportComponent } from './mechanic-schedule-report.component';

describe('MechanicScheduleReportComponent', () => {
  let component: MechanicScheduleReportComponent;
  let fixture: ComponentFixture<MechanicScheduleReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicScheduleReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicScheduleReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
