import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ReportingServiceService } from 'src/app/Services/Reporting/reporting-service.service';
import * as XLSX from 'xlsx';
//var jsPDF = require('jspdf');
// require('jspdf-autotable');

import { jsPDF } from "jspdf";
import 'jspdf-autotable';
// import * as jsPDF from 'jspdf';
import * as autoTable from 'jspdf-autotable'

import "jspdf/dist/polyfills.es.js";
import { User } from 'src/app/Services/user.model';
import { take } from 'rxjs/internal/operators/take';
//import html2canvas from 'html2canvas';
//import pdfMake from 'pdfmake/build/pdfmake';

@Component({
  selector: 'app-mechanic-schedule-report',
  templateUrl: './mechanic-schedule-report.component.html',
  styleUrls: ['./mechanic-schedule-report.component.css']
})
export class MechanicScheduleReportComponent implements OnInit {
  title = 'VVS - Mechanic Schedule Report';
  start : Date;
  end : Date;
  selectedUser : any;
  showErrorMessage : boolean = false;
  TableData : Object;
  totalJobs : number;
  Users : User[] = [];
  thaMech : string;
  counter : any;

  constructor(private ReportingService : ReportingServiceService) { }

  ngOnInit(): void { 
    var tableWithData = document.getElementById('table');

    tableWithData.hidden = true;
    this.GetMechanics();
this.counter = 0;
   }

   GetMechanics() {
    this.ReportingService.getMechanics().pipe(take(1)).subscribe((Mechanic) => {
      this.Users = Mechanic;
      console.log(this.Users);
    })
  }

  /*name of the excel-file which will be downloaded. */ 
  excelfileName= 'MechanicScheduleReport.xlsx';  

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    if (this.totalJobs > 0) {
    /* save to file */
    XLSX.writeFile(wb, this.excelfileName);
    }
    else if (this.totalJobs == 0){ alert('No Mechanic Schedule Report Data within the chosen dates to download.\n Please capture new dates and try again.'); }
  }

  //extract HTML element stuff data
  @ViewChild('htmlData') htmlData:ElementRef;

  downloadPDF() {
    if (this.totalJobs > 0) {
      let doc = new jsPDF('l', 'pt', 'a4');

      let finalY = 100;

      doc.setFontSize(20)
      doc.text("Mechanic Schedule Report", (150/3)-5,15)

      doc.setFontSize(14)

        //@ts-ignore
        doc.autoTable({startY : finalY + 15, html:'#table',useCss:true})
          
        //@ts-ignore
          finalY = doc.autoTable.previous.finalY;

        doc.save('MechanicScheduleReport.pdf');
    }
    else if (this.totalJobs == 0){ alert('No Mechanic Schedule Report Data within the chosen dates to download.\n Please capture new dates and try again.'); }
  }

  GenerateReport() {
      this.ReportingService.getMechanicScheduleReportData(this.start, this.end, this.selectedUser).subscribe((res) => {
        console.log(res);

        this.TableData = res['TableData'];
        this.totalJobs = res['TableData'].length;
        
        console.log("At VVS, We generate successes only. lol")
        })

    //Testing if there are values for the report tables or not
    if (this.totalJobs > 0) {
      var tableWithData = document.getElementById('table');

      tableWithData.hidden = false;
        console.log(this.totalJobs)
      }
      else if (this.totalJobs == 0){ alert('No Mechanic Schedule Report Data within the chosen dates.\n Please capture new dates and try again.'); }

      if(this.counter==0) {
        setTimeout(( ) => { 
          console.log('ItS BeEn 3 SeCoNdS');
          document.getElementById('generaterTing').click(); }, 3000);
        this.counter++;
      }
  }
}