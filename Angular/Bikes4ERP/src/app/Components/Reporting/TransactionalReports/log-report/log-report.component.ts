import { Component, OnInit } from '@angular/core';
import { ReportingServiceService } from 'src/app/Services/Reporting/reporting-service.service';
import * as XLSX from 'xlsx';
//import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-log-report',
  templateUrl: './log-report.component.html',
  styleUrls: ['./log-report.component.css']
})
export class LogReportComponent implements OnInit {
  title = 'VVS - Log Report';
  start : Date;
  end : Date;
  showErrorMessage : boolean = false;
  TableData : Object;
  totalage : any;
  totalJobs : number;
  counter : any;

//  downloadJsonHref: any;

  constructor(private ReportingService : ReportingServiceService/*, private sanitizer: DomSanitizer*/) { }

  ngOnInit(): void {
    var tableWithData = document.getElementById('table');

    tableWithData.hidden = true;
    this.counter = 0;

   }

  /*name of the excel-file which will be downloaded. */ 
  excelfileName= 'LogReport.xlsx';  

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    if (this.totalJobs > 0) {
    /* save to file */
    XLSX.writeFile(wb, this.excelfileName);
    }
    else if (this.totalJobs == 0){ alert('No Log Report Data within the chosen dates to download.\n Please capture new dates and try again.'); }
  }

/*  generateDownloadJsonUri() {
    var theJSON = JSON.stringify(this.resJsonResponse);
    var uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
    this.downloadJsonHref = uri;
}
  resJsonResponse(resJsonResponse: any) {
    throw new Error("Method not implemented.");
  }   */

  GenerateReport() {
    //check if an option is selected otherwise Show error message
    if(this.start == undefined || this.end == undefined) {
      this.showErrorMessage == true;
    } else {
      this.showErrorMessage = false;

      this.ReportingService.getLogReportData(this.start, this.end).subscribe((res) => {
        console.log(res);
        this.TableData = res['TableData'];
        this.totalJobs = res['TableData'].length;

        //Get the Total for the control break
        let totality = res['TableData'].map((z: { TotalTags: any; }) => z.TotalTags);
        const sum = totality.reduce((a: any,b: any) => a + b, 0);
        this.totalage = sum;

        console.log(this.totalage);
      })
    }

    //Testing if there are values for the report tables or not
    if (this.totalJobs > 0) {
      var tableWithData = document.getElementById('table');

      tableWithData.hidden = false;
        console.log(this.totalJobs)
      }
      else if (this.totalJobs == 0){ alert('No Log Report Data within the chosen dates.\n Please capture new dates and try again.'); }

      if(this.counter==0) {
        setTimeout(( ) => { 
          console.log('ItS BeEn 5 SeCoNdS');
          document.getElementById('generaterTing').click(); }, 5000);
        this.counter++;
      }
  }
}