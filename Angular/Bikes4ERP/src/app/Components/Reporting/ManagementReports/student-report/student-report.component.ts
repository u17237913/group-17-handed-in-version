import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ReportingServiceService } from 'src/app/Services/Reporting/reporting-service.service';
import { Chart } from 'chart.js';

// import * as Chart from 'chart.js';
import * as XLSX from 'xlsx';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-student-report',
  templateUrl: './student-report.component.html',
  styleUrls: ['./student-report.component.css']
})
export class StudentReportComponent implements OnInit {
  @ViewChild('canvas') canvas: { nativeElement: HTMLCanvasElement; }/*: ElementRef*/;
  public context: CanvasRenderingContext2D;

  title = 'VVS - Log Report';
  start : Date;
  end : Date;
  showErrorMessage : boolean = false;
  totalJobs: number;
  TableData : Object;
  ChartData : Object;
  passage : any;
  attendage : any;
  chart : Chart;
  counter : any;

  StudentList : any[] = [];

  constructor(private ReportingService : ReportingServiceService) { }

  ngOnInit(): void {
    var tableWithData = document.getElementById('table');
    var chartWithData = document.getElementById('chart');

    tableWithData.hidden = true;
    chartWithData.hidden = true;

    this.counter = 0;

  }

  /*name of the excel-file which will be downloaded. */ 
  excelfileName= 'StudentReport.xlsx';  

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    //Testing if there are values for the report tables or not
    if (this.totalJobs > 0) { XLSX.utils.book_append_sheet(wb, ws, 'Student Report Table');

      /* save to file */
      XLSX.writeFile(wb, this.excelfileName);
    }
    else if (this.totalJobs == 0){ 
      alert('No Student Report Data within the chosen dates.\n Please capture new dates and try again.'); 
    }
  }//exportexcel

  GenerateReport() {
    //We destroy the chart if it exists so we can create a new one without the old one still taking memory and being loaded in the DOM
    if(this.chart) {this.chart.destroy();}

    //check if an option is selected otherwise Show error message
    if(this.start == undefined || this.end == undefined) {
      this.showErrorMessage == true;
    } else {
      this.showErrorMessage = false;

      this.ReportingService.getStudentReportData(this.start, this.end).subscribe((res) => {
        console.log(res);
        this.TableData = res['TableData'];
        this.totalJobs = res['TableData'].length;

                  //res.TableData.forEach(element => {
          /*
                    element.Students.forEach(s => {
                      s.forEach(st => {
                        var CurrentStudent : any = {};
                        CurrentStudent.StudentName = st.StudentName;
                        CurrentStudent.Attendance = st.Attendance;
                        CurrentStudent.AverageMark = st.AverageMark;
                        CurrentStudent.Age = st.Age;
                        CurrentStudent.BicycleCode = st.BicycleCode;

                        var Check = this.StudentList.find( sl => sl.BicycleCode == CurrentStudent.BicycleCode );

                        if (Check == undefined) {
                          this.StudentList.push(CurrentStudent)
                        }
          //FILTER BY SCHOOL OKAY!
                      });
                    });
                    console.log(this.StudentList);
          */
                  //});

        //Get Data for the x-axis and the y-axis -!-!-> DATASET 1
        let keys = res['TableData'].map((z) => z.SchoolName);
        let Vals = res['TableData'].map((z) => z.OverallSchoolAverage);
        let Vals2 = res['TableData'].map((z) => z.AverageSchoolAttendance);
        console.log(keys);
        console.log(Vals);
        console.log(Vals2);

        //Get the Averages for the control break
        let passtotality = res['TableData'].map((z) => z.OverallSchoolAverage);
        const sum = passtotality.reduce((a: any,b: any) => a + b, 0);
        this.passage = (sum/passtotality.length)*length || 0;

        let attendtotality = res['TableData'].map((z) => z.AverageSchoolAttendance);
        const add = attendtotality.reduce((a: any,b: any) => a + b, 0);
        this.attendage = (add/attendtotality.length)*length || 0;

        console.log(this.passage);
        console.log(this.attendage);

        this.context = (<HTMLCanvasElement>this.canvas.nativeElement).getContext('2d');
        this.chart = new Chart(this.context, {
        type: 'bar',
        data: {
            labels: keys,
            datasets: [
              {
                label: 'Pass Rate',
                data: Vals,
                fill : false,
                barPercentage: 0.75,
                backgroundColor: 'rgba(0, 24, 68, 0.8)',
              borderColor: 'rgba(0, 24, 68, 1)',
              borderWidth: 1,
                // this dataset is drawn below
                order: 1
            }, {
                label: 'Attendance',
                data: Vals2,
                fill : false,
                barPercentage : 0.75,
                backgroundColor: 'rgba(205, 21, 67, 0.4)',
                borderColor: 'rgba(205, 21, 67, 1)',
                borderWidth: 1,
                type: 'bar',
                // this dataset is drawn on top
                order: 2
            }],
        },
        options: {
          legend: {
            display: true,
          },
          title: {
            display: true,
            text: 'Student report per School'
                },
          scales: {
              xAxes: [{
                  display: true,
                  gridLines: {
                      offsetGridLines: true
                  }
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  max: 100,
                }
              }],
            }
          }
        });
      });//EndSubscription
    }

    //Testing if there are values for the report tables or not
    if (this.totalJobs > 0) {
      var tableWithData = document.getElementById('table');
      var chartWithData = document.getElementById('chart');

      tableWithData.hidden = false;
      chartWithData.hidden = false;

      console.log(this.totalJobs)
      }
      else /*if (this.totalJobs <= 0)*/ { 
        var tableWithData = document.getElementById('table');
        var chartWithData = document.getElementById('chart');

        tableWithData.hidden = true;
        chartWithData.hidden = true;

        /*alert('No Student Report Data within the chosen dates.\n Please capture new dates and try again.');*/ }

      if(this.counter==0) {
        setTimeout(( ) => { 
          console.log('ItS BeEn 5 SeCoNdS');
          document.getElementById('generaterTing').click(); }, 3000);
        this.counter++;
      }
  }//EndOfGenerateReport
}//OnInit