import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBicyclePartComponent } from './create-bicycle-part.component';

describe('CreateBicyclePartComponent', () => {
  let component: CreateBicyclePartComponent;
  let fixture: ComponentFixture<CreateBicyclePartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBicyclePartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBicyclePartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
