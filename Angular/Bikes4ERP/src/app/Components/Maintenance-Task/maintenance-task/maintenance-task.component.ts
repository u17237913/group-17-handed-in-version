import { Component, OnInit } from '@angular/core';
import { MaintenanceTask } from 'src/app/Classes/MaintenanceTask/maintenance-task';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-maintenance-task',
  templateUrl: './maintenance-task.component.html',
  styleUrls: ['./maintenance-task.component.css'],
})
export class MaintenanceTaskComponent implements OnInit {
  MaintenanceTaskList: MaintenanceTask[];
  public values = [];
  MaintenanceTaskForm: MaintenanceTask;

  //search stuff
  public TempArray: MaintenanceTask[] = [];
  public SearchArray: MaintenanceTask[] = [];
  isArrayStored: boolean = false;

  constructor(
    private MaintenanceTaskService: MyServiceService,
    public router: Router
  ) {}

  ngOnInit(): void {
    console.log('hello');
    this.MaintenanceTaskService.GetMaintenanceTask().subscribe((data) => {
      this.MaintenanceTaskList = data as MaintenanceTask[];
      console.log(this.MaintenanceTaskList, 'list');
      console.log(data, 'data');
    });
  }

  ViewMaintenanceTask(id: number) {
    this.router.navigate(['/viewmaintenancetask', { ID: id }],{ skipLocationChange: true });
  }
  CreateMaintenanceTask() {
    this.router.navigate(['/createmaintenancetask']);
  }

  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.MaintenanceTaskList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.MaintenanceTaskList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.MaintenanceTaskList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.MaintenanceTaskList.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.MaintenanceTask.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.MaintenanceTaskList = this.SearchArray;
    } else {
      this.MaintenanceTaskList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }

}