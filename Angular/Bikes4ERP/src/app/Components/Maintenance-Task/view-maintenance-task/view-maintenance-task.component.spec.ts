import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMaintenanceTaskComponent } from './view-maintenance-task.component';

describe('ViewMaintenanceTaskComponent', () => {
  let component: ViewMaintenanceTaskComponent;
  let fixture: ComponentFixture<ViewMaintenanceTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMaintenanceTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMaintenanceTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
