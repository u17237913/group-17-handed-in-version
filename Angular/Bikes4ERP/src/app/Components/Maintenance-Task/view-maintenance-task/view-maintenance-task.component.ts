import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { take } from 'rxjs/operators';
import { MaintenanceType } from 'src/app/Classes/MaintenanceType/maintenance-type';
import { Part } from 'src/app/Classes/Part/part';
import { MaintenanceTask } from 'src/app/Classes/MaintenanceTask/maintenance-task';

@Component({
  selector: 'app-view-maintenance-task',
  templateUrl: './view-maintenance-task.component.html',
  styleUrls: ['./view-maintenance-task.component.css'],
})
export class ViewMaintenanceTaskComponent implements OnInit {
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public route: ActivatedRoute,
    public MaintenanceTaskService: MyServiceService
  ) {}

  MaintenanceTaskForm: any;
  MaintenanceTaskList: MaintenanceTask[];
  ID: Number;
  data = false;
  message: string;

  MaintenanceTypeID: any;
  PartID: any;

  MaintenanceTypes: MaintenanceType[] = [];
  Parts: Part[] = [];
  oldData: any;

  ngOnInit(): void {
    this.MaintenanceTaskForm = new FormGroup({
      MaintenanceTaskID: new FormControl(),
      MaintenanceTask1: new FormControl(),
      MaintenanceTaskDescription: new FormControl(),
      MaintenanceTypeID: new FormControl(),
      PartID: new FormControl(),
      MaintenanceType: new FormControl(),
      Part: new FormControl(),
    });

    this.getMaintenanceTypes();
    this.getParts();

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });

    this.MaintenanceTaskService.GetMaintenanceTask().subscribe((data) =>
      data.forEach((element) => {
        console.log(element.ID);
        if (element.ID == this.ID) {
          this.MaintenanceTaskForm = this.formbuilder.group({
            MaintenanceTaskID: [element.ID, Validators.required],
            MaintenanceTask1: [element.MaintenanceTask, [Validators.required]],
            MaintenanceTaskDescription: [
              element.MaintenanceTaskDescription,
              [Validators.required],
            ],
            MaintenanceTypeID: [
              element.MaintenanceTypeID,
              [Validators.required],
            ],
            PartID: [element.PartID, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );
  }
  onFormSubmit(MaintenanceTaskForm) {
    console.log();
    const MaintenanceTask = MaintenanceTaskForm.value;
    this.UpdateMaintenanceTask(this.ID, MaintenanceTask);
  }
  UpdateMaintenanceTask(ID: any, MaintenanceTask: MaintenanceTask) {
    // user.UserTypeID = this.UserTypeID;

    this.MaintenanceTaskService.UpdateMaintenanceTask(
      ID,
      MaintenanceTask, this.oldData
    ).subscribe(() => {
      this.data = true;
      this.message = 'User has been successfully updated boiii !';
      this.MaintenanceTaskForm.reset;
    });
  }

  getMaintenanceTypes() {
    this.MaintenanceTaskService.GetMaintenanceTypes()
      .pipe(take(1))
      .subscribe((MaintenanceTypes) => {
        this.MaintenanceTypes = MaintenanceTypes;
        // console.log(this.UserTypes);
      });
  }
  getParts() {
    this.MaintenanceTaskService.GetParts()
      .pipe(take(1))
      .subscribe((Parts) => {
        this.Parts = Parts;
        // console.log(this.UserTypes);
      });
  }

  DeleteMaintenanceTask(MaintenanceTaskID: any) {
    MaintenanceTaskID = this.ID;
    this.MaintenanceTaskService.DeleteMaintenanceTask(
      MaintenanceTaskID, this.oldData
    ).subscribe(() => {
      this.data = true;
    });
  }
  GoBackToList() {
    this.router.navigate(['/maintenancetask']);
  }
}
