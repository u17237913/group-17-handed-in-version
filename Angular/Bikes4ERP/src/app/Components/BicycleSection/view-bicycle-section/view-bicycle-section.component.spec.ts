import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBicycleSectionComponent } from './view-bicycle-section.component';

describe('ViewBicycleSectionComponent', () => {
  let component: ViewBicycleSectionComponent;
  let fixture: ComponentFixture<ViewBicycleSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBicycleSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBicycleSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
