import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BicycleSectionComponent } from './bicycle-section.component';

describe('BicycleSectionComponent', () => {
  let component: BicycleSectionComponent;
  let fixture: ComponentFixture<BicycleSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BicycleSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BicycleSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
