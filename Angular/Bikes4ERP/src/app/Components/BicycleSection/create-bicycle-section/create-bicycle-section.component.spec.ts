import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBicycleSectionComponent } from './create-bicycle-section.component';

describe('CreateBicycleSectionComponent', () => {
  let component: CreateBicycleSectionComponent;
  let fixture: ComponentFixture<CreateBicycleSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBicycleSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBicycleSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
