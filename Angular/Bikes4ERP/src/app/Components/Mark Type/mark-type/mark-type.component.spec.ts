import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkTypeComponent } from './mark-type.component';

describe('MarkTypeComponent', () => {
  let component: MarkTypeComponent;
  let fixture: ComponentFixture<MarkTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
