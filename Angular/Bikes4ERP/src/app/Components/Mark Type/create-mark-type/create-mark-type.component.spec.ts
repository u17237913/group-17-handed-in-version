import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMarkTypeComponent } from './create-mark-type.component';

describe('CreateMarkTypeComponent', () => {
  let component: CreateMarkTypeComponent;
  let fixture: ComponentFixture<CreateMarkTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMarkTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMarkTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
