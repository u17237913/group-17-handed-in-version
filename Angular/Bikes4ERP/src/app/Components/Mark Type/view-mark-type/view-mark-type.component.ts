import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { MarkTypeService } from 'src/app/Services/MarkType/mark-type.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { MarkType } from 'src/app/Classes/MarkType/mark-type';

@Component({
  selector: 'app-view-mark-type',
  templateUrl: './view-mark-type.component.html',
  styleUrls: ['./view-mark-type.component.css'],
})
export class ViewMarkTypeComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private MarkTypeService: MarkTypeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ID: number;
  data = false;
  message: string;
  oldData: any;

  MarkTypeForm = new FormGroup({
    MarkTypeID: new FormControl(),
    MarkType1: new FormControl(),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.ID = id;
    });

    //inject service call to get current object details
    this.MarkTypeService.getMarkTypes().subscribe((data) =>
      data.forEach((element) => {
        if (element.MarkTypeID == this.ID) {
          this.MarkTypeForm = this.formBuilder.group({
            MarkTypeID: [element.MarkTypeID, Validators.required],
            MarkType1: [element.MarkType1, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

  } //ngOnInit

  onFormSubmit(MarkTypeForm) {
    const marktype = MarkTypeForm.value;
    this.EditMarkType(marktype);
  } //OnFormSubmit

  EditMarkType(marktype: MarkType) {
    this.MarkTypeService.UpdateMarkType(marktype, this.oldData).subscribe(() => {
      this.data = true;

      this.message = 'Mark Type has been successfully updated boiii !';
      this.MarkTypeForm.reset;
      console.log(this.MarkTypeForm.value + 'we adding');

      this.router.navigate(['/marktype']);
    }); //UpdateMarkType
  } //EditMarkType

  DeleteMarkType(id: number) {
    id = this.ID;
    this.MarkTypeService.DeleteMarkType(id, this.oldData).subscribe(() => {
      this.data = true;

      this.router.navigate(['/marktype']);
    });
  } //DeleteMarkType

  GoToList() {
    this.router.navigate(['/marktype']);
  } //GoToList

} //OnInit