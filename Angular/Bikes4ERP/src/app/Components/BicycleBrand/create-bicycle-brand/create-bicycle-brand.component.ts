import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { BicycleBrandService } from 'src/app/Services/BicycleBrand/bicycle-brand.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand';

@Component({
  selector: 'app-create-bicycle-brand',
  templateUrl: './create-bicycle-brand.component.html',
  styleUrls: ['./create-bicycle-brand.component.css']
})
export class CreateBicycleBrandComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private BicycleBrandService: BicycleBrandService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
//BicycleBrandForm : any;
  ID: number;
  data = false;
  message: string;

  //Initialize FormGroup ting
  BicycleBrandForm = new FormGroup({
    BicycleBrandName: new FormControl(''),
  });

  ngOnInit(): void {
    //For the Create object
    this.BicycleBrandForm = this.formBuilder.group({
      BicycleBrandName: ['', [Validators.required]],
    });
  } //ngOnInit

  onFormSubmit(BicycleBrandForm) {
    //debugger;
    const bicyclebrand = BicycleBrandForm.value;
    this.CreateBicycleBrand(bicyclebrand);
  } //OnFormSubmit

  CreateBicycleBrand(bicyclebrand: BicycleBrand) {
    //inject the services
    //debugger;
    this.BicycleBrandService.addBicycleBrand(bicyclebrand).subscribe(() => {
      this.data = true;

      this.message = 'BicycleBrand has been added bro! good job...';
      this.BicycleBrandForm.reset;
      console.log(this.BicycleBrandForm.value + 'we adding');

      this.router.navigate(['/bicyclebrand']);
    }); //addBicycleBrand
  } //CreateBicycleBrand

  GoToList() {
    this.router.navigate(['/bicyclebrand']);
  } //GoToList
} //OnInit