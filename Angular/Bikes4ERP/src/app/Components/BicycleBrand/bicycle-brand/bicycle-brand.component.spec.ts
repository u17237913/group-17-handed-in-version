import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BicycleBrandComponent } from './bicycle-brand.component';

describe('BicycleBrandComponent', () => {
  let component: BicycleBrandComponent;
  let fixture: ComponentFixture<BicycleBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BicycleBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BicycleBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
