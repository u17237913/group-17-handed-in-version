import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { BicycleBrandService } from 'src/app/Services/BicycleBrand/bicycle-brand.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand';

@Component({
  selector: 'app-view-bicycle-brand',
  templateUrl: './view-bicycle-brand.component.html',
  styleUrls: ['./view-bicycle-brand.component.css']
})
export class ViewBicycleBrandComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private BicycleBrandService: BicycleBrandService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ID: number;
  data = false;
  message: string;
  oldData: any;

  BicycleBrandForm = new FormGroup({
    BicycleBrandID: new FormControl(),
    BicycleBrandName: new FormControl(),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.ID = id;
    });

    //inject service call to get current object details
    this.BicycleBrandService.getBicycleBrands().subscribe((data) =>
      data.forEach((element) => {
        if (element.BicycleBrandID == this.ID) {
          this.BicycleBrandForm = this.formBuilder.group({
            BicycleBrandID: [element.BicycleBrandID, Validators.required],
            BicycleBrandName: [element.BicycleBrandName, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

  } //ngOnInit

  onFormSubmit(BicycleBrandForm) {
    const bicyclebrand = BicycleBrandForm.value;
    this.EditBicycleBrand(bicyclebrand);
  } //OnFormSubmit

  EditBicycleBrand(bicyclebrand: BicycleBrand) {
    this.BicycleBrandService.UpdateBicycleBrand(bicyclebrand, this.oldData).subscribe(() => {
      this.data = true;

      this.message = 'Mark Type has been successfully updated boiii !';
      this.BicycleBrandForm.reset;
      console.log(this.BicycleBrandForm.value + 'we adding');

      this.router.navigate(['/bicyclebrand']);
    }); //UpdateBicycleBrand
  } //EditBicycleBrand

  DeleteBicycleBrand(id: number) {
    id = this.ID;
    this.BicycleBrandService.DeleteBicycleBrand(id, this.oldData).subscribe(() => {
      this.data = true;

      this.router.navigate(['/bicyclebrand']);
    });
  } //DeleteBicycleBrand

  GoToList() {
    this.router.navigate(['/bicyclebrand']);
  } //GoToList

} //OnInit