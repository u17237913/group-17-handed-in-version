import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignBicycleComponent } from './assign-bicycle.component';

describe('AssignBicycleComponent', () => {
  let component: AssignBicycleComponent;
  let fixture: ComponentFixture<AssignBicycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignBicycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignBicycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
