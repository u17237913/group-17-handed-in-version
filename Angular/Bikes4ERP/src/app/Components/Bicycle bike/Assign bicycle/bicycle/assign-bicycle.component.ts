import { Component, OnInit } from '@angular/core';
import { BicycleService } from 'src/app/Services/Bicycle/bicycle.service';
import { BicycleStudentViewModel } from 'src/app/Classes/Bicycle/bicycle-student-view-model';

import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-bicycle',
  templateUrl: './assign-bicycle.component.html',
  styleUrls: ['./assign-bicycle.component.css'],
})
export class AssignBicycleComponent implements OnInit {

  public BiycyleAssignList : BicycleStudentViewModel[] = [];
  public TempArray: BicycleStudentViewModel[] = [];
  public SearchArray: BicycleStudentViewModel[] = [];
  isArrayStored: boolean = false;

  constructor(
    private bicycleService: BicycleService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.bicycleService.GetBicycleAssign().subscribe((bal) => {
      this.BiycyleAssignList = bal;
      console.log(this.BiycyleAssignList);
    });
  }

  UnassignBicycle(id: number) {
    this.bicycleService.UnassignBicycle(id).subscribe((res) => {
      this.ShowModal();
      this.ngOnInit();
    });
  }

  AssignBicycle(bicycleCode: number) {
    this.router.navigate(['assignbicyclestudent', { id: bicycleCode }]);
  }

  ShowModal() {
    // var modal = document.getElementById('UnassignBicycle');
    // modal.style.removeProperty('z-index');
    var myModal = new Bootstrap.Modal(document.getElementById('AssignModel'));
    myModal.show();
  }

  CloseModal() {
    var modal = document.getElementById('AssignModel');
    modal.style.setProperty('z-index', '-1');
  }

  SearchBicycles(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.BiycyleAssignList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.BiycyleAssignList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.BiycyleAssignList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.BiycyleAssignList.forEach((bicycle) => {
        var SearchNumber: string = event.target.value;
        if (bicycle.BicycleCode.toString().startsWith(SearchNumber))
         {
          this.SearchArray.push(bicycle);
        }
      }); // For each
*/

      this.BiycyleAssignList = this.SearchArray;
    } else {
      this.BiycyleAssignList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}


