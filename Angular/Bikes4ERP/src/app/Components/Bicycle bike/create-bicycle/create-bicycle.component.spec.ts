import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBicycleComponent } from './create-bicycle.component';

describe('CreateBicycleComponent', () => {
  let component: CreateBicycleComponent;
  let fixture: ComponentFixture<CreateBicycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBicycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBicycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
