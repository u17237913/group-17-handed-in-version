import { Component, OnInit } from '@angular/core';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { BicycleService } from 'src/app/Services/Bicycle/bicycle.service';
import { Router } from '@angular/router';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand.js';

import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { hr } from 'date-fns/locale';

@Component({
  selector: 'app-create-bicycle',
  templateUrl: './create-bicycle.component.html',
  styleUrls: ['./create-bicycle.component.css'],
})
export class CreateBicycleComponent implements OnInit {
  BicycleList: Bicycle[] = [];
  BicycleForm: any;
  data = false;
  public values = [];
  ngModel: any;
  BicycleBrands: BicycleBrand[] = [];

  modalHeading = '';
  modalBody = '';
  elementType: 'url' | 'canvas' | 'img' = 'url';
  value: string;
  href: string;

  constructor(public route: Router, public bicycleService: BicycleService) {}

  ngOnInit(): void {
    this.GetBicycles();
    this.GetBicycleBrands();

    this.bicycleService.BicycleForm = {
      BicyleCode: '',
      BicycleBrandID: 0,
      
    };
  }

  BackList() {
    this.route.navigate['/bicycle'];
  }
  GetBicycleBrands() {
    this.bicycleService.GetBicycleBrands().subscribe((data) => {
      this.BicycleBrands = data as BicycleBrand[];
    });
  }

  // SaveBicycle() {
  //   this.bicycleService.AddBicycle(this.bicycleService.BicycleForm).subscribe(
  //     (res) => {
  //       // this.modalHeading = "Success"
  //       // this.modalBody = ""
  //     },
  //     (error) => {
  //       debugger;
  //       if (error.status == 409) {
  //         this.modalHeading = 'Oops!';
  //         this.modalBody =
  //           'This bicycle code already exists, please choose another.';
  //         this.ShowModal();
  //       }
  //     }
  //   );
  // }

  ShowModal() {
    // var modal = document.getElementById('UnassignBicycle');
    // modal.style.removeProperty('z-index');
  //  var myModal = new Bootstrap.Modal(document.getElementById('ViewQRCode'));
    var myModal = new Bootstrap.Modal(document.getElementById('CreateBicycle'));
    myModal.show();
  }
  GoBacktoCreateBicycle() {
    if (this.modalHeading == 'Success') {
      this.route.navigate(['/bicycle']);
    } else {
      this.route.navigate(['/createbicycle']);
    }
  }

  ShowQrCode(){
    var myModal = new Bootstrap.Modal(document.getElementById('ViewQRCode'));
    myModal.show();
  }

  // SaveBicycle() {
  //   this.bicycleService
  //     .AddBicycle(this.bicycleService.BicycleForm)
  //     .subscribe((res) => {});
  // }

  GetBicycles() {
    this.bicycleService.GetBicycles().subscribe((data) => {
      this.BicycleList = data;
      console.log(this.BicycleList);
    });
  }

  downloadQRcode() {
    //this.href = document.getElementsByTagName('img')[3].src;
    //var href = document.getElementsByTagName('img');
    //console.log(href);
    this.href = document.getElementsByClassName('qrcode')[0].getElementsByTagName('img')[0].src;
    console.log('something');
  }

  // CloseModal() {
  //   var modal = document.getElementById('AssignModel');
  //   modal.style.setProperty('z-index', '-1');
  // }

  onFormSubmit(BicycleForm) {
    this.CreateBicycle(BicycleForm);
  }



  CreateBicycle(bicycle: Bicycle) {
    this.bicycleService.AddBicycle(bicycle).subscribe(
      (res) => {
        this.modalHeading = 'Success';
        this.modalBody = 'You have successfuly created a bicycle';
        this.value = res.BicycleID.toString();
        //this.route.navigate(['/bicycle']);
        this.ShowModal();
      },
      (error) => {
        if (error.status == 409) {
          this.modalHeading = 'Error';
          this.modalBody = 'The Bicycle Code already exists. Please try again.';
          this.ShowModal();
        }
      }
    );
  }

  GoBack() {
    this.route.navigate(['/bicycle']);
  }
}
