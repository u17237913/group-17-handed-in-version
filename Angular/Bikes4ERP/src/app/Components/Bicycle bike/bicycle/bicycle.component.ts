import { Component, OnInit } from '@angular/core';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { BicycleService } from 'src/app/Services/Bicycle/bicycle.service';
import { Router } from '@angular/router';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand.js';

@Component({
  selector: 'app-bicycle',
  templateUrl: './bicycle.component.html',
  styleUrls: ['./bicycle.component.css'],
})
export class BicycleComponent implements OnInit {
  BicycleList: Bicycle[] = [];
  public values = [];
  BicycleForm: Bicycle;
  public TempArray: Bicycle[] = [];
  public SearchArray: Bicycle[] = [];
  isArrayStored: boolean = false;
  BicycleBrands : BicycleBrand []=[];

  constructor(public route: Router, private bicycleService: BicycleService) {}

  ngOnInit(): void {
    this.GetBicycles();
    // var Check = document.getElementById('BicycleCode') as HTMLInputElement;
    // Check.checked = true;
  }

  GetBicycles() {
    this.bicycleService.GetBicycles().subscribe((data) => {
      this.BicycleList = data as Bicycle[];
      console.log(this.BicycleList)
    });
  }

  CreateBicycle() {
    this.route.navigate(['/createbicycle']);
  }

  ViewBicycle(ids: number) {
    this.route.navigate(['viewbicycle', { id: ids }],{ skipLocationChange: true });
    // console.log(ids)
  }
  SearchOptions() {
    var myModal = new Bootstrap.Modal(
      document.getElementById('SearchOptionsModal')
    );
    myModal.show();
  }

  SearchBicycles(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.BicycleList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.BicycleList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.BicycleList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.BicycleList.forEach((bicycle) => {
        var SearchNumber: string = event.target.value;
        if (bicycle.BicyleCode.toString().startsWith(SearchNumber))
         {
          this.SearchArray.push(bicycle);
        }
      }); // For each
*/

      this.BicycleList = this.SearchArray;
    } else {
      this.BicycleList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}