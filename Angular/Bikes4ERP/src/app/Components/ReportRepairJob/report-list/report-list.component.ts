import { Component, OnInit } from '@angular/core';
import { Qrscan } from 'src/app/Components/qr-scan/qrscan.service';
import { Section } from 'src/app/Classes/Section/section';
import { Part } from 'src/app/Classes/Part/part';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { BicyclePartServiceService } from 'src/app/Services/BicyclePart/bicycle-part-service.service';
import { FaultTypeService } from 'src/app/Services/FaultType/fault-type.service';
import { PartFaultsComponent } from '../part-faults/part-faults.component';
import { FaultListVM } from 'src/app/Classes/FaultType/fault-list-vm';
import { Router } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.css'],
})
export class ReportListComponent implements OnInit {
  constructor(
    private qrService: Qrscan,
    private bicyclePartService: BicyclePartServiceService,
    private faultTypeService: FaultTypeService,
    private myService: MyServiceService,
    private router: Router,
    public audit: AuditTrailService
  ) {}

  public SectionList: Section[] = [];
  public PartList: Part[] = [];
  public FaultTypeList: FaultType[] = [];

  public FaultList: FaultListVM[] = [];
  public counter = 0;

  ngOnInit(): void {
    this.counter = 0;
    this.bicyclePartService.getSections().subscribe((sections) => {
      this.SectionList = sections;
      console.log(sections as Section[]);

      this.bicyclePartService.getParts().subscribe((parts) => {
        this.PartList = parts;
        console.log(parts as Part[]);

        this.faultTypeService.GetFaultTypes().subscribe((faultTypes) => {
          this.FaultTypeList = faultTypes;
          console.log(faultTypes as FaultType[]);

          this.qrService.CurrentJobFaultList.forEach((jobFault) => {
            console.log(jobFault);
            var FaultItem = new FaultListVM();
            //FaultItem.FaultID = jobFault.JobFaultID;
            FaultItem.FaultID = this.qrService.Counter;
            jobFault.ID = this.qrService.Counter;

            if (jobFault.SectionID != null) {
              FaultItem.SectionName = this.SectionList.find(
                (sl) => sl.SectionID == jobFault.SectionID
              ).SectionName;
            } else {
              FaultItem.SectionName = "Unknown";
            }

            if (jobFault.PartID != null) {
              FaultItem.PartName = this.PartList.find(
                (pl) => pl.PartID == jobFault.PartID
              ).PartName;
            } else {
              FaultItem.PartName = "Unknown";
            }

            if (jobFault.PartFaultID != null) {
              this.faultTypeService
                .GetFaultTypeByPart(jobFault.PartID)
                .subscribe((faultType) => {
                  FaultItem.FaultTypeName = faultType.FaultType1;
              });
            } else {
              FaultItem.FaultTypeName = jobFault.Notes;
            }

            this.FaultList.push(FaultItem);

            this.qrService.Counter = this.qrService.Counter + 1;
          });
        });
      });
    });

    //Play the audio feed - MORGAN FREEMAN
    setTimeout(( ) => { 
      console.log('ItS BeEn 2 SeCoNdS');
      this.playAddFaultAudio(); }, 2000);

    //Play the audio feed - JK, IT'S UNCLE RUFY
    setTimeout(( ) => { 
      console.log('ItS BeEn 12 SeCoNdS');
      this.playReportJobAudio(); }, 12000);
  }//ngOninit

  playAddFaultAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/AddFault.mp3";
    audio.load();
    audio.play();
  }

  playReportJobAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/ReportJob.mp3";
    audio.load();
    audio.play();
  }

  RemoveFault(ID: number) {
    // debugger;
    this.FaultList = this.FaultList.filter((fl) => fl.FaultID != ID);
    this.qrService.CurrentJobFaultList = this.qrService.CurrentJobFaultList.filter(
      (cjfl) => cjfl.ID != ID
    );
  }

  AddJobFault() {
    this.router.navigate([
      'bicycleblueprint',
      { RepairJobID: this.qrService.CurrentJobFault.RepairJobID },
    ]);
  }

  ReportJob() {
    this.myService
      .AddJobFaults(this.qrService.CurrentJobFaultList)
      .subscribe((res) => {});
  }

  goHome() {
    this.router.navigate(['/homepage']);
  }
}
