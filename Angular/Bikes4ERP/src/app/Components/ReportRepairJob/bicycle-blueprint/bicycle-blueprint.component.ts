import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BicycleService } from 'src/app/Services/Bicycle/bicycle.service';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { BicycleBluePrintService } from 'src/app/Services/Blueprint/bicycle-blue-print.service';
import { Qrscan } from 'src/app/Components/qr-scan/qrscan.service';
import { JobFault } from 'src/app/Classes/JobFault/job-fault';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { getElement } from '@syncfusion/ej2-base';

@Component({
  selector: 'app-bicycle-blueprint',
  templateUrl: './bicycle-blueprint.component.html',
  styleUrls: ['./bicycle-blueprint.component.css'],
})
export class BicycleBlueprintComponent implements OnInit {
  RepairJobID: number;
  PictureName: string;

  constructor(
    public route: ActivatedRoute,
    public bicycleService: BicycleService,
    public bicycleBluePrintService: BicycleBluePrintService,
    public router: Router,
    private qrService: Qrscan,
    private formBuilder: FormBuilder,
  ) {}

  unknownDetails = new FormGroup({
    notes: new FormControl(''),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('RepairJobID'));
      // this.route.paramMap.subscribe((params: ParamMap) => {
      let name = params.get('name');
      this.RepairJobID = id;
    });
    this.PictureName = name;

    this.qrService.CurrentJobFault = new JobFault();
    this.qrService.CurrentJobFault.RepairJobID = this.RepairJobID;

    //Play the audio feed
    setTimeout(( ) => { 
      console.log('ItS BeEn 2 SeCoNdS');
      this.playAudio(); }, 2000);
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/Section.mp3";
    audio.load();
    audio.play();
  }

  buildForm(): void {
    this.unknownDetails = this.formBuilder.group({
      notes: ['', [Validators.required]],
    });
  }

  // getsectionpart(){
  // //  this.PictureName = name;
  // //   this.bicycleBluePrintService.getsectionpart(this.PictureName).subscribe((data) => {
  // //     this.PictureName = data;

  // //     console.log(this.PictureName);
  // //     this.router.navigate(['/sectionblueprint', {pname : name}])
  // //   });
  // this.bicycleBluePrintService.PictureName.next("Gears");
  //   // this.PictureName = "Gears";
  //   // this.bicycleBluePrintService.getsectionpart(this.PictureName = "Gears").subscribe((data) => {
  //   //   this.PictureName = data;
  //   //   //this.bicycleBluePrintService.PictureName = "Gears";
  //   //   this.bicycleBluePrintService.PictureName.next("Gears");

  //   //   console.log(this.PictureName);
  //   //   console.log('Generic term');
  //   // });
  // }
  getsectionpart(id: number) {
    this.bicycleBluePrintService.getsectionpart(id).subscribe((data) => {
      this.PictureName = data;
      console.log(this.PictureName);
      this.qrService.CurrentJobFault.SectionID = id;
    });
    this.router.navigate(['/sectionblueprint', { ID: id }],{ skipLocationChange: true });
  }

  reportUnknown(unknownDetails){
    this.qrService.CurrentJobFault.SectionID = null;
    this.qrService.CurrentJobFault.PartID = null;
    this.qrService.CurrentJobFault.PartFaultID = null;
    this.qrService.CurrentJobFault.JobFaultStatusID = 3;
    this.qrService.CurrentJobFault.Notes = unknownDetails.value.notes;
    this.qrService.CurrentJobFaultList.push(this.qrService.CurrentJobFault);

    document.getElementById('disappearpliz').click();

    this.router.navigate(['/reportlist'],{ skipLocationChange: true });
  }

  // getsectionpart1(){
  //   this.PictureName = "Back Wheel";
  //   this.bicycleBluePrintService.getsectionpart( this.PictureName = "Back Wheel").subscribe((data) => {
  //     this.PictureName = data;
  //     console.log(this.PictureName);
  //   });
  // }

  // getsectionpart2(){
  //   this.PictureName = "Front Wheel";
  //   this.bicycleBluePrintService.getsectionpart( this.PictureName = "Front Wheel").subscribe((data) => {
  //     this.PictureName = data;
  //     console.log(this.PictureName);
  //   });
  // }

  // getsectionpart3(){
  //   this.PictureName = "Body";
  //   this.bicycleBluePrintService.getsectionpart( this.PictureName = "Body").subscribe((data) => {
  //     this.PictureName = data;
  //     console.log(this.PictureName);
  //   });
  // }

  // getsectionpart4(){
  //   this.PictureName = "Handle Bars";
  //   this.bicycleBluePrintService.getsectionpart( this.PictureName = "Handle Bars").subscribe((data) => {
  //     this.PictureName = data;
  //     console.log(this.PictureName);
  //   });
  // }

  // getsectionpart5(){
  //   this.PictureName = "Seat";
  //   this.bicycleBluePrintService.getsectionpart( this.PictureName = "Seat").subscribe((data) => {
  //     this.PictureName = data;
  //     console.log(this.PictureName);
  //   });
  // }
}
