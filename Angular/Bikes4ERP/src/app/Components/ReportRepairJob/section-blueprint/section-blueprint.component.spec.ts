import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionBlueprintComponent } from './section-blueprint.component';

describe('SectionBlueprintComponent', () => {
  let component: SectionBlueprintComponent;
  let fixture: ComponentFixture<SectionBlueprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionBlueprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionBlueprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
