import { Component, OnInit } from '@angular/core';
import { BicycleBluePrintService } from 'src/app/Services/Blueprint/bicycle-blue-print.service';
import { Part } from 'src/app/Classes/Part/part';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';
import { Qrscan } from 'src/app/Components/qr-scan/qrscan.service';
import { BicyclePart } from 'src/app/Classes/BicyclePart/bicycle-part';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';

@Component({
  selector: 'app-section-blueprint',
  templateUrl: './section-blueprint.component.html',
  styleUrls: ['./section-blueprint.component.css'],
})
export class SectionBlueprintComponent implements OnInit {
  TableData: PartFault[];
  PictureName: string;
  PictureName$: Subscription;
  ID: any;
  PartFound: boolean;
  isPictureAvailable: any;
  Base64Picture: string;
  thefile: FileToUpload = new FileToUpload();
  message: string;

  constructor(
    public bicycleBluePrintService: BicycleBluePrintService,
    public uploadService: FileUploadServiceService,
    public route: ActivatedRoute,
    public router: Router,
    private qrService: Qrscan,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer
  ) {}

  unknownDetails = new FormGroup({
    notes: new FormControl(''),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('ID');
      this.ID = id;
    });

    this.generateparttable();

    //Play the audio feed
    setTimeout(( ) => { 
      console.log('ItS BeEn 2 SeCoNdS');
      this.playAudio(); }, 2000);
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/Part.mp3";
    audio.load();
    audio.play();
  }

  buildForm(): void {
    this.unknownDetails = this.formBuilder.group({
      notes: ['', [Validators.required]],
    });
  }

  // getparttable() {
  //   this.bicycleBluePrintService.getsectionpart(this.PictureName).subscribe((res) => {
  //     console.log(res);
  //     this.TableData = res['TableData'];
  //   })
  //  this.PictureName = "Gears";

  // this.bicycleBluePrintService.generatetable(this.PictureName = "Gears").subscribe((data) => {
  //   var part = new Part();
  //   part.PartName = data.PartName;

  //    this.PictureName = data;
  //   console.log(data);

  //   this.TableData = data['result'];
  //   //this.bicycleBluePrintService.PictureName = "Gears";
  //   this.bicycleBluePrintService.PictureName.next("Gears");

  // });
  //     this.bicycleBluePrintService.getparttable(this.PictureName = "Gears").subscribe((res) => {
  //       console.log(res);
  //       this.TableData = res['TableData'];
  //     })
  //  }

  //  generateparttable(){
  //   this.bicycleBluePrintService.getparttable(this.PictureName = "Gears").subscribe((res) => {
  //     console.log(res);
  //     this.TableData = res['TableData'];
  //   })
  // }
  generateparttable() {
    this.bicycleBluePrintService.getsectionpart(this.ID).subscribe((res) => {
      res.forEach(element => {
        this.uploadService.getPartImage(element.PartID).subscribe((data) => {
          this.thefile = data;
          this.Base64Picture = data.fileAsBase64;
          if (this.Base64Picture) {
            this.isPictureAvailable = true;
          }
          this.message = 'Image has been successfully loaded boiii !';
          element.PartImage = this.Base64Picture;
        });
      });
      
      this.TableData = res as PartFault[];
    });
  }

  transformer(img: string) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        'data:image;base64,' + img
      );
  }

  checkID(part: BicyclePart) {
    console.log(part);
    //checking if ID part exists
    this.bicycleBluePrintService.FindPart(part).subscribe((res: any) => {
      console.log(res);
      if (res.Found) {
        this.sendID(part.PartID);
        this.PartFound = false;
      } else {
        alert('This Part does not have a fault associated with it.');
      }
    });
  }

  sendID(id: any) {
    //end of part thing
    this.bicycleBluePrintService.getpartfault(id).subscribe((data) => {
      this.ID = data;
      console.log(this.ID);
    });
    this.qrService.CurrentJobFault.PartID = id;
    this.router.navigate(['/partfaults', { ID: id }],{ skipLocationChange: true });
  }

  reportUnknown(unknownDetails){
    this.qrService.CurrentJobFault.PartID = null;
    this.qrService.CurrentJobFault.PartFaultID = null;
    this.qrService.CurrentJobFault.JobFaultStatusID = 3;
    this.qrService.CurrentJobFault.Notes = unknownDetails.value.notes;
    this.qrService.CurrentJobFaultList.push(this.qrService.CurrentJobFault);
    document.getElementById('disappearpliz').click();

  this.router.navigate(['/reportlist'],{ skipLocationChange: true });
  }
}