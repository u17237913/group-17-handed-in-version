import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { take } from 'rxjs/operators';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { Part } from 'src/app/Classes/Part/part';
import { FaultTypeService } from 'src/app/Services/FaultType/fault-type.service';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';


@Component({
  selector: 'app-create-fault-type',
  templateUrl: './create-fault-type.component.html',
  styleUrls: ['./create-fault-type.component.css'],
})
export class CreateFaultTypeComponent implements OnInit {
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    private uploadService: FileUploadServiceService,
    // public MaintenanceTaskService: MyServiceService,
    public faultservice: FaultTypeService
  ) {}

  PartFaultForm: any;
  data = false;
  message: string;
  ID: any;
  FaultType: FaultType[] = [];
  Parts: Part[] = [];
  imageUrl: string =
    '../../../assets/images/Norco-section-sl-ult-black-all-road-bicycle.jpg';
  //fileToUpload : File = null;

  //declare the max file size
  public MAX_SIZE: number = 10048576; //BE CAREFUL, extra zero included!

  //declare a file variable, this is for the initial upload
  theFile: any = null;

  //variable for error messages - note
  messages: string[] = [];

  //Initialize FormGroup ting

  ngOnInit(): void {
    this.faultservice.FaultTypeForm = {
      FaultTypeID: 0,
      FaultType1: '',
      PartNeeded: '',
      //FaultImage: '',
    };

    this.getParts();

    // var UploadButton = <HTMLInputElement> document.getElementById("hideME");
    var UploadArea = document.getElementById('UploadArea');
    var UploadBtn = document.getElementById('UploadBtn');
    var ObjArea = document.getElementById('ObjArea');
    var CreateObj = document.getElementById('CreateObj');

    UploadArea.hidden = false;
    UploadBtn.hidden = true;
    ObjArea.hidden = false;
    CreateObj.hidden = false;
  } //ngOnInit

  //this event is called from the html file on the file upload element
  onFileChange(event) {
    this.theFile = null;

    //check that there is a file and the file is not 0 bits
    if (event.target.files && event.target.files.length > 0) {
      //Disallow the uploading of files larger than 10MB.
      if (event.target.files[0].size < this.MAX_SIZE) {
        //set theFile property
        this.theFile = event.target.files[0];

        //Show image preview
        var reader = new FileReader();

        reader.readAsDataURL(this.theFile);
        reader.onload = () => {
          this.imageUrl = reader.result as string;
        };
      } else {
        //Display error message <-!-!-> watchout, this allows for multiple files...
        this.messages.push(
          'File: ' + event.target.files[0].name + 'is too large to upload.'
        );
      }
    }
  } //OnFileChange

  //this function is now called when the submit button is clicked
  uploadFile(): void {
    this.readAndUploadFile(this.theFile);
  }

  //this function converts to base 64 and makes the service call, which sends stuff to the API bla bla bla
  private readAndUploadFile(theFile: any) {
    //file to upload model
    let file = new FileToUpload();

    //Set File Information
    file.fileName = theFile.name;

    //Use FileReader() object to get file to upload <-!-!-> NOTE: FileReader only works with newer browsers
    let reader = new FileReader();

    //Setup onload event for reader
    reader.onload = () => {
      //Store base64 encoded representation of file
      file.fileAsBase64 = reader.result.toString();

      //POST to server
      this.uploadService.uploadFaultImage(file).subscribe((respondation) => {
        console.log(respondation);
        this.messages.push('Upload complete');
      });
    };

    //Read the file
    reader.readAsDataURL(theFile);
  }

  onFormSubmit( /*ALL THE HONEY STARTS HAPPENING HERE*/ ) {
    //relax wangu, pane zvirikuitika ipapa so
    this.CreateFaultTaskObj();

    var UploadArea = document.getElementById('UploadArea');
    var UploadBtn = document.getElementById('UploadBtn');
    var CreateObj = document.getElementById('CreateObj');
    var ObjArea = document.getElementById('ObjArea');

    UploadArea.hidden = false;
    UploadBtn.hidden = true;
    ObjArea.hidden = false;
    CreateObj.hidden = false;

  }//OnFormSubmit

  CreateFaultTaskObj() {
    //inject the services
    this.faultservice.AddFaultType(this.faultservice.FaultTypeForm).subscribe((res) => {
      this.data = true;
      console.log(res);
      });//EndSubscription

      console.log(this.faultservice.FaultTypeForm);

      setTimeout(( ) => { 
        console.log('ItS BeEn 7 SeCoNdS');
        this.uploadFile(); }, 7000);
    }

  getParts() {
    this.faultservice
      .GetParts()
      .pipe(take(1))
      .subscribe((Parts) => {
        this.Parts = Parts;
      });
  }

  GoBackToList() {
    setTimeout(( ) => { 
      console.log('ItS BeEn 5 SeCoNdS');
        this.router.navigate(['/faulttype']); }, 3000);
  }
}