import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MyServiceService } from '../../Services/my-service.service';
import { User } from '../../Services/user.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
})
export class ForgotPasswordComponent implements OnInit {
  UserList: User[];
  UserForm: any;
  Email: string;
  constructor(public router: Router, public EmailService: MyServiceService) {}

  ngOnInit(): void {}

  checkEmail(email: string) {
    console.log(email);
    // this.EmailService.GetUserEmail(email).subscribe((data) => {
    //   this.UserList = data as User[];
    //   console.log(this.UserList, 'list');
    //   console.log(data, 'data');
    // });

    this.EmailService.ForgotPasword(email).subscribe((res: any) => {
      if (res == 'NotFound') {
        alert('This email does not exist. Please try again.');
      } else {
        alert('Email sent!');
      }
    });

    this.router.navigate(['/forgotpassword', { EMAIL: email }],{ skipLocationChange: true });
  }

  return() {
    this.router.navigate(['/login']);
  }
}
