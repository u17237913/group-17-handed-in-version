import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { PartTypeService } from 'src/app/Services/PartType/part-type.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { PartType } from 'src/app/Classes/PartType/part-type';

@Component({
  selector: 'app-view-part-type',
  templateUrl: './view-part-type.component.html',
  styleUrls: ['./view-part-type.component.css']
})
export class ViewPartTypeComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private PartTypeService: PartTypeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ID: number;
  data = false;
  message: string;
  oldData: any;

  PartTypeForm = new FormGroup({
    PartTypeID: new FormControl(),
    PartType1: new FormControl(),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.ID = id;
    });

    //inject service call to get current object details
    this.PartTypeService.getPartTypes().subscribe((data) =>
      data.forEach((element) => {
        if (element.PartTypeID == this.ID) {
          this.PartTypeForm = this.formBuilder.group({
            PartTypeID: [element.PartTypeID, Validators.required],
            PartType1: [element.PartType1, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

  } //ngOnInit

  onFormSubmit(PartTypeForm) {
    const parttype = PartTypeForm.value;
    this.EditPartType(parttype);
  } //OnFormSubmit

  EditPartType(parttype: PartType) {
    this.PartTypeService.UpdatePartType(parttype, this.oldData).subscribe(() => {
      this.data = true;

      this.message = 'Part Type has been successfully updated boiii !';
      this.PartTypeForm.reset;
      console.log(this.PartTypeForm.value + 'we adding');

      this.router.navigate(['/parttype']);
    }); //UpdatePartType
  } //EditPartType

  DeletePartType(id: number) {
    id = this.ID;
    this.PartTypeService.DeletePartType(id, this.oldData).subscribe(() => {
      this.data = true;

      this.router.navigate(['/parttype']);
    });
  } //DeletePartType

  GoToList() {
    this.router.navigate(['/parttype']);
  } //GoToList

} //OnInit