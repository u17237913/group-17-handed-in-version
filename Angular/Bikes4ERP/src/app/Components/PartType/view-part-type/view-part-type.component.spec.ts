import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPartTypeComponent } from './view-part-type.component';

describe('ViewPartTypeComponent', () => {
  let component: ViewPartTypeComponent;
  let fixture: ComponentFixture<ViewPartTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPartTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPartTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
