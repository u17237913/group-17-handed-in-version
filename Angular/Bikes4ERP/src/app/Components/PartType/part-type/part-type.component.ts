import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PartTypeService } from 'src/app/Services/PartType/part-type.service';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { PartType } from 'src/app/Classes/PartType/part-type';

@Component({
  selector: 'app-part-type',
  templateUrl: './part-type.component.html',
  styleUrls: ['./part-type.component.css']
})
export class PartTypeComponent implements OnInit {
  PartTypeArray: PartType[] = [];
  public values = [];
  LogForm : PartType;
  public TempArray : PartType[] = [];
  public SearchArray: PartType[] =[];
  isArrayStored : boolean = false;
  data = false;
  mySubscription: any;

  constructor(
    private router: Router,
    private PartTypeService: PartTypeService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
    this.PartTypeService.getPartTypes().subscribe(data => data.forEach(element => { 
      var parttype = new PartType();
      parttype.PartTypeID = element.PartTypeID;
      parttype.PartType1 = element.PartType1;

        this.PartTypeArray.push(parttype);
      })
    );
  } //ngOnInit

  EditPartType(ids: number) {
    this.router.navigate(['/viewparttype', { id: ids }],{ skipLocationChange: true });
  } //EditPartType

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    } //Endif
  } //ngOnDestroy

  SearchLogs(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.PartTypeArray.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.PartTypeArray = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.PartTypeArray.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

      /*
      this.LogList.forEach((log) => {
        var SearchString: string = event.target.value;
        if (
        log.TagSerialNumber.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(log);
        }
      }); // For each
*/

      this.PartTypeArray = this.SearchArray;
    } else {
      this.PartTypeArray = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
} //OnInit