import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { TagService } from 'src/app/Services/Tag/tag.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Tag } from 'src/app/Classes/Tag/tag';
import { TagStatus } from 'src/app/Classes/TagStatus/tag-status';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-view-tag',
  templateUrl: './view-tag.component.html',
  styleUrls: ['./view-tag.component.css'],
})
export class ViewTagComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public tagService: TagService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  TagForm: any;
  TagList: Tag[];
  data = false;
  TagID: number;
  TagStatusList: TagStatus[];
  oldData: any;

  modalMode = '';
  modalHeader = '';
  modalBody = '';

  ngOnInit(): void {
    this.GetTagStatus();
    this.GetTags();

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.TagID = id;
    });

    this.TagForm = new FormGroup({
      TagSerialNumber: new FormControl(),
      TagStatusID: new FormControl(),
      DateInstalled: new FormControl(),
    });

    this.tagService.GetTags().subscribe((data) =>
      data.forEach((element) => {
        if (element.TagID == this.TagID) {
          this.TagForm = this.formBuilder.group({
            TagID: [element.TagID, [Validators.required]],
            TagSerialNumber: [element.TagSerialNumber, [Validators.required]],
            TagStatusID: [element.TagStatusID, [Validators.required]],
            DateInstalled: [element.DateInstalled, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );
  }

  Confirm(Mode: string) {
    if (Mode == 'ConfirmUpdate') {
      this.modalHeader = 'Update';
      this.modalMode = 'ConfirmUpdate';
      this.modalBody = 'Are you sure you want to update this tag?';
      var myModal = new Bootstrap.Modal(document.getElementById('TheModal'));
      myModal.show();
    }
    if (Mode == 'ConfirmDelete') {
      this.modalHeader = 'Delete';
      this.modalMode = 'ConfirmDelete';
      this.modalBody = 'Are you sure you want to delete this tag?';
      var myModal = new Bootstrap.Modal(document.getElementById('TheModal'));
      myModal.show();
    }

    var myModal = new Bootstrap.Modal(document.getElementById('TheModal'));
    myModal.show();
  }

  onFormSubmit() {
    const tag = this.TagForm.value;
    this.UpdateTag(this.TagID, tag);
    //console.log('hit');
  }

  UpdateTag(TagID: any, tag: Tag) {
    this.tagService.UpdateTag(TagID, tag, this.oldData).subscribe(() => {
      this.data = true;
      this.modalHeader = 'Success';
      this.modalBody = 'You have successfuly updated the Tag';
      this.modalMode = 'Message';

      var myModal = new Bootstrap.Modal(document.getElementById('TheModal'));
      myModal.show();
      // this.TagForm.reset;
      //this.router.navigate(['/tag']);
    });
  }
  DeleteTag() {
    var TagID = this.TagID;
    this.tagService.DeleteTag(TagID, this.oldData).subscribe(() => {
      this.data = true;
      this.modalHeader = 'Success';
      this.modalBody = 'You have successfuly deleted the Tag';
      this.modalMode = 'Message';

      var myModal = new Bootstrap.Modal(document.getElementById('TheModal'));
      myModal.show();
    });
  }

  Navigate() {
    if (this.modalHeader == 'Success') {
      this.router.navigate(['/tag']);
    }
  }

  GetTags() {
    this.tagService.GetTags().subscribe((data) => {
      this.TagList = data;
    });
  }

  GetTagStatus() {
    this.tagService.GetTagStatus().subscribe((data) => {
      this.TagStatusList = data;
    });
  }
}
