import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BicycleService } from 'src/app/Services/Bicycle/bicycle.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { TagService } from 'src/app/Services/Tag/tag.service';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import * as Bootstrap from '../../../../../node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-assign-tag-bicycle',
  templateUrl: './assign-tag-bicycle.component.html',
  styleUrls: ['./assign-tag-bicycle.component.css'],
})
export class AssignTagBicycleComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public bicycleService: BicycleService,
    public route: ActivatedRoute,
    public router: Router,
    public tagService: TagService
  ) {}

  public CurrentSerialNumber: string;
  public BicycleList: Bicycle[] = [];
  public BicycleCode: number;

  public TempArray: Bicycle[] = [];
  public SearchArray: Bicycle[] = [];
  isArrayStored: boolean = false;

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.CurrentSerialNumber = id;
    });

    this.tagService.GetBicyclesWithoutTags().subscribe((bicycles) => {
      this.BicycleList = bicycles;
      console.log(this.BicycleList);
    });
  }

  AssignBicycleTag() {
    this.tagService
      .AssignTag(this.BicycleCode, this.CurrentSerialNumber)
      .subscribe((res) => {
        var myModal = new Bootstrap.Modal(
          document.getElementById('AssignTagModal')
        );
        myModal.show();
      });
  }

  BackToTagPage() {
    this.router.navigate(['/assigntag']);
  }

  SearchTags(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.BicycleList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.BicycleList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.BicycleList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.TagList.forEach((tag) => {
        var SearchString: string = event.target.value;
        if (
          tag.TagSerialNumber.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(tag);
        }
      }); // For each
*/

      this.BicycleList = this.SearchArray;
    } else {
      this.BicycleList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}