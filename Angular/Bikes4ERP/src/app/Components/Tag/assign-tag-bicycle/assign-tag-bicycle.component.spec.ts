import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignTagBicycleComponent } from './assign-tag-bicycle.component';

describe('AssignTagBicycleComponent', () => {
  let component: AssignTagBicycleComponent;
  let fixture: ComponentFixture<AssignTagBicycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignTagBicycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignTagBicycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
