import { Component, OnInit } from '@angular/core';
import { Tag } from 'src/app/Classes/Tag/tag';
import { Router } from '@angular/router';
import { TagService } from 'src/app/Services/Tag/tag.service';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  TagList : Tag [] = [];
  public values = [];
  TagForm : Tag;

  public TempArray: Tag[] = [];
  public SearchArray: Tag[] = [];
  isArrayStored: boolean = false;

  constructor(public route : Router, private tagService : TagService) { }

  ngOnInit(): void {

    this.GetTags();

  }

  GetTags(){
    this.tagService.GetTags().subscribe((data) => {
      this.TagList = data as Tag [];
      console.log(this.TagList)
    });
  }

  CreateTag(){
    this.route.navigate(['/createtag'], { skipLocationChange: true })
  }

  ViewTag (ids : number){
    this.route.navigate(['viewtag', { id : ids }], { skipLocationChange: true })
   // console.log(ids)
  }

  // var valid =
  // (keycode > 47 && keycode < 58)   || // number keys
  // keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
  // (keycode > 64 && keycode < 91)   || // letter keys
  // (keycode > 95 && keycode < 112)  || // numpad keys
  // (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
  // (keycode > 218 && keycode < 223);   // [\]' (in order)

  SearchTags(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.TagList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.TagList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.TagList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.TagList.forEach((tag) => {
        var SearchString: string = event.target.value;
        if (
          tag.TagSerialNumber.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(tag);
        }
      }); // For each
*/
      this.TagList = this.SearchArray;
    } else {
      this.TagList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}