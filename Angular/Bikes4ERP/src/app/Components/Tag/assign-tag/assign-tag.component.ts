import { Component, OnInit } from '@angular/core';
import { TagService } from 'src/app/Services/Tag/tag.service';
import { Router } from '@angular/router';
import { TagAssignmentVM } from 'src/app/Classes/Tag/tag-assignment-vm';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';
import { Tag } from 'src/app/Classes/Tag/tag.js';

@Component({
  selector: 'app-tag',
  templateUrl: './assign-tag.component.html',
  styleUrls: ['./assign-tag.component.css'],
})
export class AssignTagComponent implements OnInit {

  public TagAssignmentList: TagAssignmentVM []=[];
  public TempArray: TagAssignmentVM[] = [];
  public SearchArray: TagAssignmentVM[] = [];
  isArrayStored: boolean = false;
  constructor(public tagService: TagService, public router: Router) {}

  ngOnInit(): void {
    this.tagService.GetTagAssignmentList().subscribe((tagAssignmentList) => {
      this.TagAssignmentList = tagAssignmentList;
      console.log(this.TagAssignmentList);
    });
  } // ngOnInit


  UnassignTag(BicycleCode: number) {
    this.tagService.UnAssignTag(BicycleCode).subscribe((res) => {
      var myModal = new Bootstrap.Modal(
        document.getElementById('UnassignModal')
      );
      myModal.show();
      this.ngOnInit();
    });
  }

  AssignTag(TagSerialNumber: string) {
    this.router.navigate(['assigntagbicycle', { id: TagSerialNumber }]);
  }

  SearchTags(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.TagAssignmentList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.TagAssignmentList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.TagAssignmentList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.TagAssignmentList.forEach((tag) => {
        var SearchString: string = event.target.value;
        if (
          tag.TagSerialNumber.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(tag);
        }
      }); // For each
*/

      this.TagAssignmentList = this.SearchArray;
    } else {
      this.TagAssignmentList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}