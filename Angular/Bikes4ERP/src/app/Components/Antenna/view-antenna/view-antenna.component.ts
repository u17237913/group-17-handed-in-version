import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { AntennaService } from 'src/app/Services/Antenna/antenna.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Antenna } from 'src/app/Classes/Antenna/antenna';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import { AntennaStatus } from 'src/app/Classes/AntennaStatus/antenna-status';

@Component({
  selector: 'app-view-antenna',
  templateUrl: './view-antenna.component.html',
  styleUrls: ['./view-antenna.component.css'],
})
export class ViewAntennaComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public antennaService: AntennaService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  AntennaForm: any;
  AntennaList: Antenna[];
  AntennaID: number;
  message: string;
  data = false;
  LocationList: LocationViewModel[];
  AntennaStatusList: AntennaStatus[];
  oldData: any

  ngOnInit(): void {
    this.GetAntennaStatus();
    this.GetLocations();

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.AntennaID = id;
    });

    this.antennaService.GetAntennas().subscribe((data) =>
      data.forEach((element) => {
        if (element.AntennaID == this.AntennaID) {
          this.AntennaForm = this.formBuilder.group({
            AntennaAddress: [element.AntennaAddress, [Validators.required]],
            AntennaID: [element.AntennaID, [Validators.required]],
            AntennaStatusID: [element.AntennaStatusID, [Validators.required]],
            SuburbID: [element.SuburbID, [Validators.required]],
            CoOrdinates: [element.CoOrdinates, [Validators.required]],
            DateInstalled: [element.DateInstalled, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

    this.AntennaForm = new FormGroup({
      CoOrdinates: new FormControl(),
      AntennaStatusID: new FormControl(),
      AntennaAddress: new FormControl(),
      SuburbID: new FormControl(),
      DateInstalled: new FormControl(),
    });
  }
  onFormSubmit(AntennaForm) {
    const antenna = AntennaForm.value;
    this.UpdateAntenna(this.AntennaID, antenna);
  }

  UpdateAntenna(AntennaID: any, antenna: Antenna) {
    this.antennaService.UpdateAntenna(AntennaID, antenna, this.oldData).subscribe(() => {
      this.data = true;
      this.message = 'Antenna has been updated';
      this.AntennaForm.reset;
      // this.router.navigate(['/antenna']);
    });
  }
  DeleteAntenna(AntennaID: any) {
    AntennaID = this.AntennaID;
    this.antennaService.DeleteAntenna(AntennaID, this.oldData).subscribe(() => {
      this.data = true;
      // this.router.navigate(['/antenna']);
    });
  }

  GetLocations() {
    this.antennaService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    });
  }

  GetAntennaStatus() {
    this.antennaService.GetAntennaStatus().subscribe((data) => {
      this.AntennaStatusList = data;
    });
  }
  GoBack() {
    this.router.navigate(['/antenna']);
  }
}
