import { Component, OnInit } from '@angular/core';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import { Antenna } from 'src/app/Classes/Antenna/antenna';
import { Router } from '@angular/router';
import { AntennaService } from 'src/app/Services/Antenna/antenna.service';
import { AntennaStatus } from 'src/app/Classes/AntennaStatus/antenna-status';

@Component({
  selector: 'app-create-antenna',
  templateUrl: './create-antenna.component.html',
  styleUrls: ['./create-antenna.component.css'],
})
export class CreateAntennaComponent implements OnInit {
  LocationList: LocationViewModel[] = [];
  AntennaList: Antenna[] = [];
  AntennaStatus: AntennaStatus[];

  AntennaForm: any;
  data = false;
  message: string;
  public values = [];
  ngModel: any;

  constructor(
    private router: Router,
    public route: Router,
    public antennaService: AntennaService
  ) {}

  ngOnInit(): void {
    // this.GetVillages();

    this.GetLocations();
    this.GetAntennas();
    this.GetAntennaStatus();

    this.antennaService.AntennaForm = {
      ID: 0,
      SuburbID: 0,
      SurburbName: '',
      AntennaSerialNumber: '',
      AntennaStatusID: 0,
      AntennaStatu: '',
      DateInstalled: '',
    }; // this.service
  }

  SaveAntenna() {
    this.antennaService
      .AddAntenna(this.antennaService.AntennaForm)
      .subscribe((res) => {});
  }
  GetLocations() {
    this.antennaService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    });
  }

  GetAntennas() {
    this.antennaService.GetAntennas().subscribe((data) => {
      this.AntennaList = data;
      console.log(this.AntennaList);
    });
  }

  GetAntennaStatus() {
    this.antennaService.GetAntennaStatus().subscribe((data) => {
      this.AntennaStatus = data;
    });
  }

  GetSub() {
    this.antennaService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    });
  }

  // onFormSubmit(AntennaForm) {
  //   this.CreateAntenna(AntennaForm);
  // }

  // CreateAntenna(antenna: Antenna) {
  //   this.antennaService.AddAntenna(antenna).subscribe(() => {
  //     this.data = true;
  //     this.message = 'Saved';
  //   });
  // }
  GoBack() {
    this.router.navigate(['/antenna']);
  }
}
