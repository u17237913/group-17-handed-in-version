import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaultTaskComponent } from './fault-task.component';

describe('FaultTaskComponent', () => {
  let component: FaultTaskComponent;
  let fixture: ComponentFixture<FaultTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaultTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaultTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
