import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { take } from 'rxjs/operators';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { Part } from 'src/app/Classes/Part/part';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';

@Component({
  selector: 'app-create-fault-task',
  templateUrl: './create-fault-task.component.html',
  styleUrls: ['./create-fault-task.component.css'],
})
export class CreateFaultTaskComponent implements OnInit {
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public MaintenanceTaskService: MyServiceService
  ) {}
  PartFaultForm: any;
  data = false;
  message: string;
  ID: any;
  FaultType: FaultType[] = [];
  Parts: Part[] = [];

  ngOnInit(): void {
    this.MaintenanceTaskService.PartFaultForm = {
      PartFaultID: 0,
      PartID: 0,
      FaultTypeID: 0,
      FaultImage: '',
    };
    this.getFaults();
    this.getParts();
  }

  getParts() {
    this.MaintenanceTaskService.GetParts()
      .pipe(take(1))
      .subscribe((Parts) => {
        this.Parts = Parts;
        // console.log(this.UserTypes);
      });
  }

  getFaults() {
    this.MaintenanceTaskService.GetFaultTypes()
      .pipe(take(1))
      .subscribe((Faults) => {
        this.FaultType = Faults;
        // console.log(this.UserTypes);
      });
  }

  // onFormSubmit(MaintenanceTaskForm) {
  //   const maintenancetask = MaintenanceTaskForm.value;
  //   this.CreateMaintenanceTask(maintenancetask);
  // }

  // CreateMaintenanceTask(maintenancetask: PartFault) {
  //   this.MaintenanceTaskService.AddPartFault(maintenancetask).subscribe(() => {
  //     this.data = true;
  //     this.message = 'Saved';
  //   });
  // }

  SaveMaintenanceTask() {
    this.MaintenanceTaskService.AddPartFault(
      this.MaintenanceTaskService.PartFaultForm
    ).subscribe((res) => {
      console.log(res);
    });
    console.log(this.MaintenanceTaskService.PartFaultForm);
  }
  GoBackToList() {
    this.router.navigate(['faulttask']);
  }
}
