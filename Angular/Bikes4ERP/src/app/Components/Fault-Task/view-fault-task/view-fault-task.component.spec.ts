import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFaultTaskComponent } from './view-fault-task.component';

describe('ViewFaultTaskComponent', () => {
  let component: ViewFaultTaskComponent;
  let fixture: ComponentFixture<ViewFaultTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFaultTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFaultTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
