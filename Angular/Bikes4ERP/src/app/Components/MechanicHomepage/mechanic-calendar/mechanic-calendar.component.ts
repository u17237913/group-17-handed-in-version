import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarMomentDateFormatter,
} from 'angular-calendar';
import { ActivatedRoute, Router } from '@angular/router';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-mechanic-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './mechanic-calendar.component.html',
  styleUrls: ['./mechanic-calendar.component.css'],
})
export class MechanicCalendarComponent {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = true;

  FormDate: string;
  WithTime: string;
  WithoutTime: string;
  OutputString: string;

  constructor(
    private modal: NgbModal,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  //returning the day clicked
  dayClicked(date: any) {
    console.log(date.date);

    this.FormDate = date.date.toString();
    this.WithTime = this.FormDate.substring(0, 24);
    this.WithoutTime = this.FormDate.substring(4, 24);

    this.FormDate = this.WithoutTime;

    console.log(this.WithTime);

    //array with months
    var MonthArr = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];

    var MonthArr2 = [
      '01',
      '02',
      '03',
      '04',
      '05',
      '06',
      '07',
      '08',
      '09',
      '10',
      '11',
      '12',
    ];

    var CorrectDay = this.FormDate.substring(4, 6);

    var MonthNum = '';
    var CorrectMonth = this.FormDate.substring(0, 3);

    for (var i = 0; i < 12; i++) {
      if (CorrectMonth == MonthArr[i]) {
        MonthNum = MonthArr2[i];
      }
    }
    var CorrectYear = this.FormDate.substring(7, 11);

    this.OutputString = CorrectYear + '/' + MonthNum + '/' + CorrectDay;
    console.log(this.OutputString);
    this.router.navigate(['/joblist', { DATE: this.OutputString }],{ skipLocationChange: true });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  ViewJobList() {
    this.router.navigate(['/joblist']);
  }
}
