import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { JobFault } from 'src/app/Classes/JobFault/job-fault';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { from } from 'rxjs';
import { FormGroup, CheckboxControlValueAccessor } from '@angular/forms';
import { __await } from 'tslib';
import { RepairJob } from 'src/app/Classes/RepairJob/repair-job';
import { Location } from '@angular/common';
import { element } from 'protractor';

@Component({
  selector: 'app-fault-list',
  templateUrl: './fault-list.component.html',
  styleUrls: ['./fault-list.component.css'],
})
export class FaultListComponent implements OnInit, AfterViewChecked {
  FaultList: JobFault[];
  public values = [];
  faultForm: FormGroup;
  ID: any;
  complete: boolean = false;

  // isChecked: any;

  constructor(
    public FaultService: MyServiceService,
    public router: Router,
    public route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.PopulateTable();
    //this.CheckBox();
  }

  UpdateStatus(faultID: number, faultJob: JobFault) {
    //var tempSectionPart = faultJob.SectionPart;
    var tempPart = faultJob.Part;
    //this.PopulateTable();
    //faultJob.SectionPart = '';
    faultJob.Part = '';
    faultJob.JobFaultStatus = '';
    if (faultJob.JobFaultStatusID == 3) {
      // this.isChecked = true;

      faultJob.JobFaultStatusID = 2;
      faultJob.JobFaultStatus = 'Complete';
    } else if (faultJob.JobFaultStatusID == 2) {
      faultJob.JobFaultStatusID = 3;
      faultJob.JobFaultStatus = 'Incomplete';
    }

    console.log(faultJob);
    this.FaultService.UpdateJobFaults(faultID, faultJob).subscribe(() => {});
    //faultJob.SectionPart = tempSectionPart;
    faultJob.Part = tempPart;
    // this.PopulateTable();
    this.completionCheck();
  }

  //function to populate the table
  PopulateTable() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });

    console.log('hello');
    this.FaultService.GetJobFaults(this.ID).subscribe((data) =>
      data.forEach((element) => {
        if (element.RepairJobID == this.ID) {
          console.log(element.RepairJobID);
          this.FaultList = data as JobFault[];
          this.completionCheck();
        }
      })
    );
  }

  async CheckBox() {
    await this.PopulateTable();
    // debugger;
    this.FaultList.forEach((element) => {});
    // this.FaultService.GetJobFaults2(this.ID).subscribe((data) =>
    //   data.forEach((element) => {
    //     if (element.RepairJobID == this.ID) {
    //       console.log(element.RepairJobID);
    //       this.FaultList = data as JobFault[];
    //     }
    //   })
    // );
  }

  ngAfterViewChecked() {
    this.FaultList.forEach((fault) => {
      var CheckBox = document.getElementById(
        'Check' + fault.JobFaultID.toString()
      ) as HTMLInputElement;
      if (fault.JobFaultStatus == 'Complete') {
        CheckBox.checked = true;
      } else {
        CheckBox.checked = false;
      }
    });
  }

  UpdateJobStatus(JobID: number) {
    console.log(JobID);
    this.FaultService.UpdateJobStatus(JobID).subscribe(() => {});
  }

  goBack() {
    this.location.back();
  }

  completionCheck() {
    var count = 0;
    var check = 0;
    this.FaultList.forEach((element) => {
      count = count + 1;
      if (element.JobFaultStatusID == 2) {
        check = check + 1;
      }
    });
    if (count == check) {
      this.complete = true;
    } else {
      this.complete = false;
    }
  }

  // checkCheckBox(thisEntry: JobFault) {
  //   var CheckBox = 'Check' + thisEntry.JobFaultID.toString();

  //   if (thisEntry.JobFaultStatusID == 2) {
  //     document.getElementById(CheckBox).setAttribute('checked', 'cheched');
  //   } else {
  //     document.getElementById(CheckBox).removeAttribute('checked');
  //   }
  // }
}
