import { Component, OnInit } from '@angular/core';
import { MaintenanceJob } from 'src/app/Classes/MaintenanceJob/maintenance-job';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { User } from 'src/app/Services/user.model';
import { RepairJob } from 'src/app/Classes/RepairJob/repair-job';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css'],
})
export class JobListComponent implements OnInit {
  FormDate: any;
  JobList: MaintenanceJob[];
  public values = [];
  JobForm: MaintenanceJob;
  Date: string;

  RepairJobList: RepairJob[];
  public values1 = [];
  RepairJobForm: RepairJob;
  UserID : any;

  public isCollapsed = true;

  constructor(
    private JobService: MyServiceService,
    public router: Router,
    public route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // this.FaultService.GetJobFaults(this.ID).subscribe((data) =>
    //   data.forEach((element) => {
    //     if (element.RepairJobID == this.ID) {
    //       console.log(element.RepairJobID);
    //       this.FaultList = data as JobFault[];
    //     }
    //   })
    // );
    this.UserID = localStorage.getItem('UserID')

    this.route.paramMap.subscribe((params: ParamMap) => {
      let date = params.get('DATE');
      this.FormDate = date;
    });

    this.JobService.GetRepairJob(this.FormDate, this.UserID).subscribe((data) => {
      this.RepairJobList = data as RepairJob[];
    });

    this.JobService.GetMaintenanceJobs(this.FormDate, this.UserID).subscribe((data) => {
      this.JobList = data as MaintenanceJob[];
    });
  }
  
  MaintenanceList(id: number) {
     //checking if mechanic has < business rule days
     this.JobService.IsCheckedIn(id).subscribe((res: string) => {
      console.log(res)
      if (res == "NotCheckedIn") {
       alert('This Bike is not checked in, please check in the bike first');
        
      } else {
        
        this.router.navigate(['/maintenancelist', { ID: id }],{ skipLocationChange: true })
      }
    });

   
  }

  JobTasks(id: number) {

     //checking if mechanic has < business rule days
     this.JobService.IsCheckedInMaintenance(id).subscribe((res: string) => {
      console.log(res)
      if (res == "NotCheckedIn") {
       alert('This Bike is not checked in, please check in the bike first');
        
      } 
      else {
        
        this.router.navigate(['/faultlist', { ID: id }],{ skipLocationChange: true });
      }
    });
   
  }
}
