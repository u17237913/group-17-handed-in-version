import { Component, OnInit } from '@angular/core';
import { UserType } from 'src/app/Classes/UserType/user-type';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-type',
  templateUrl: './user-type.component.html',
  styleUrls: ['./user-type.component.css'],
})
export class UserTypeComponent implements OnInit {

  UserTypeList: UserType[];
  public values = [];
  UserTypeForm: UserType;

  //search stuff
  public TempArray: UserType[] = [];
  public SearchArray: UserType[] = [];
  isArrayStored: boolean = false;

  constructor(
    private UserTypeService: MyServiceService,
    public router: Router
  ) {}

  ngOnInit(): void {
    console.log('hello');
    this.UserTypeService.GetUserTypes().subscribe((data) => {
      this.UserTypeList = data as UserType[];
      console.log(this.UserTypeList, 'list');
      console.log(data, 'data');
    });
  }

  ViewUserType(id: number) {
    this.router.navigate(['/viewusertype', { ID: id }], { skipLocationChange: true });
  }

  CreateUserType() {
    this.router.navigate(['/createusertype']);
  }

  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.UserTypeList.slice();
      this.isArrayStored = true;
    }

    if (event.target.value != '') {
      this.SearchArray = [];
      this.UserTypeList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.UserTypeList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.UserTypeList.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.UserType.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.UserTypeList = this.SearchArray;
    } else {
      this.UserTypeList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}