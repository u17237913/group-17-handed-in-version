import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { take } from 'rxjs/operators';
import { UserType } from 'src/app/Classes/UserType/user-type';

@Component({
  selector: 'app-create-user-type',
  templateUrl: './create-user-type.component.html',
  styleUrls: ['./create-user-type.component.css'],
})
export class CreateUserTypeComponent implements OnInit {
  UserTypeList: UserType[];
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public UserTypeService: MyServiceService
  ) {}

  UserTypeForm: any;

  ngOnInit(): void {
    this.UserTypeService.UserTypeForm = {
      UserType1: '',
      UserPermissions: '',
    };
  }

  SaveUserType() {
    console.log(this.UserTypeService.UserTypeForm);
    this.UserTypeService.AddNewUserType(
      this.UserTypeService.UserTypeForm
    ).subscribe((res) => {
      console.log(res);
    });

    // this.GoBackToList();
  }
  onFormSubmit(UserTypeForm) {}

  GoBackToList() {
    this.router.navigate(['/usertype']);
  }
}
