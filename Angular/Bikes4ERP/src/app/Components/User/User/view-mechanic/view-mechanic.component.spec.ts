import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMechanicComponent } from './view-mechanic.component';

describe('ViewMechanicComponent', () => {
  let component: ViewMechanicComponent;
  let fixture: ComponentFixture<ViewMechanicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMechanicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMechanicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
