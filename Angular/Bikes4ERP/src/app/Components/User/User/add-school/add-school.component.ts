import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { MaintenanceTask } from 'src/app/Classes/MaintenanceTask/maintenance-task';
import { take } from 'rxjs/operators';
import { MaintenanceType } from 'src/app/Classes/MaintenanceType/maintenance-type';
import { Part } from 'src/app/Classes/Part/part';
import { WorkDay } from 'src/app/Classes/WorkDay/work-day';
import { School } from 'src/app/Classes/School/school';
import { Console } from 'console';

@Component({
  selector: 'app-add-school',
  templateUrl: './add-school.component.html',
  styleUrls: ['./add-school.component.css'],
})
export class AddSchoolComponent implements OnInit {
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public route: ActivatedRoute,
    public MaintenanceTaskService: MyServiceService
  ) {}

  MaintenanceTypes: WorkDay[] = [];
  Parts: School[] = [];
  ID: number;
  Days: boolean = false;
  
  

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });
    this.MaintenanceTaskService.MaintenanceTaskForm = {
      MechanicSchoolID: 0,
      MechanicID: 0,
      SchoolID: '',
      WorkDayID: '',
      UserID: this.ID,
    };

    this.getMaintenanceTypes();
    this.getParts();
  }

  SaveMaintenanceTask() {
    console.log(this.MaintenanceTaskService.MaintenanceTaskForm.UserID);
    this.MaintenanceTaskService.AddMechanicSchool(
      this.MaintenanceTaskService.MaintenanceTaskForm
    ).subscribe((res) => {
      console.log(res);
    });
  }
  CheckMech() {
    //checking if mechanic has < business rule days
    this.MaintenanceTaskService.CheckWorkdays(
      this.MaintenanceTaskService.MaintenanceTaskForm
    ).subscribe((res: any) => {
      console.log(res)
      if (res.TooManyDays) {
       alert('This Mechanic already has too many work days');
        this.Days = false;
       

      } else {
        this.Days = true;  
        
      }
    });
  }

  getMaintenanceTypes() {
    this.MaintenanceTaskService.GetWorkDays()
      .pipe(take(1))
      .subscribe((MaintenanceTypes) => {
        this.MaintenanceTypes = MaintenanceTypes;
        // console.log(this.UserTypes);
      });
  }
  getParts() {
    this.MaintenanceTaskService.GetSchools()
      .pipe(take(1))
      .subscribe((Parts) => {
        this.Parts = Parts;
        // console.log(this.UserTypes);
      });
  }

  GoBackToList() {
    this.router.navigate(['/user']);
  }
}
