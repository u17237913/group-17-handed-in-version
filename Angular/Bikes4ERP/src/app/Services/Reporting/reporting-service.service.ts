import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { from, Observable } from 'rxjs';
import { JobType } from 'src/app/Classes/JobType/job-type';
import { School } from 'src/app/Classes/School/school';
import { BicycleHistoryVM } from 'src/app/Classes/ReportingVMs/bicycle-history-vm';
import { map } from 'rxjs/operators';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';
import { User } from 'src/app/Services/user.model';

@Injectable({
  providedIn: 'root'
})
export class ReportingServiceService {

  url : string;
  header : any;
  option : any;

  constructor(private http : HttpClient, public audit : AuditTrailService) { 
    this.url = environment.apiURL + '/Reporting/';
    const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.option = {headers : headers};
}

//GET MECHANIC SCHEDULE DATA [1]
  getMechanicScheduleReportData(start : Date, end : Date, mechanic : number) {
    //this.audit.addAuditTrail(4, "Check-In, RepairJob, and MaintenanceJob", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get(this.url + "GetMechanicSchedule?StartDate="+start + "&EndDate="+end + "&SelectedMechanic="+mechanic).pipe(map(result => result))
  }

  //ADDITIONAL LOOKUP ENDPOINTS
  getMechanics():Observable<User[]> { 
    const headers = {
      Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    };
    return this.http.get<User[]>(this.url + 'GetUsers/', { headers }); 
  }



//GET MECHANIC SCHEDULE REQUIREMENTS DATA [2]
  getMechanicRequirementsReportData(start : Date, end : Date, mechanic : number) {
    //this.audit.addAuditTrail(4, "Check-In, RepairJob, and MaintenanceJob", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get(this.url + "GetMechanicRequirementsSchedule?StartDate="+start + "&EndDate="+end + "&SelectedMechanic="+mechanic).pipe(map(result => result))
  }

  getMechRepLilTable(start : Date, end : Date) {
    //this.audit.addAuditTrail(4, "JobFault", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get(this.url + "GetRequirementsLittleTable?StartDate="+start + "&EndDate="+end).pipe(map(result => result))
  }

/*.*/

//GET STUDENT REPORT DATA [3]
  getStudentReportData(start : Date, end : Date/*, selectedSchool? : number*/) {
    //this.audit.addAuditTrail(4, "Student, School, StudentMark, and Log", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get<any>(this.url + "GetStudentReport?StartDate="+start + "&EndDate="+end).pipe(map(result => result))
  }

//GET BICYCLE HISTORY DATA [4]
  getBicycleHistoryReportData(selectedSchool : number, selectedJobType : number) {
    //this.audit.addAuditTrail(4, "Bicycle, BicycleStudent, and Student", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get(this.url + "GetBicycleHistory?SelectedSchool="+selectedSchool + "&SelectedJobType="+selectedJobType).pipe(map(result => result))
  }

  //ADDITIONAL LOOKUP ENDPOINTS
  getSchools():Observable<School[]> { 
    //this.audit.addAuditTrail(4, "School", null, null).subscribe(() => console.error);
    return this.http.get<School[]>(this.url + 'GetAllSchools/'); 
  }

  getJobTypes():Observable<JobType[]> { 
    //this.audit.addAuditTrail(4, "JobType", null, null).subscribe(() => console.error);
    return this.http.get<JobType[]>(this.url + 'GetAllJobTypes/'); 
  }



//GET LOG REPORT DATA [5]
  getLogReportData(start : Date, end : Date) {
    //this.audit.addAuditTrail(4, "Log", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get(this.url + "GetLogReport?StartDate="+start + "&EndDate="+end).pipe(map(result => result))
  }


//GET PARTS USED DATA [6]
  getPartsUsedReportData(start : Date, end : Date, mechanic : number) {
    //this.audit.addAuditTrail(4, "JobFault", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get(this.url + "GetPartsUsedReport?StartDate="+start + "&EndDate="+end + "&SelectedMechanic="+mechanic).pipe(map(result => result))
  }

  getPartsUsedRepLilTable(start : Date, end : Date) {
    //this.audit.addAuditTrail(4, "JobFault", null, null).subscribe(() => console.error);
    // Pipe and map formats the data into a format readable by Typescript
    return this.http.get(this.url + "GetPartsUsedLittleTable?StartDate="+start + "&EndDate="+end).pipe(map(result => result))
  }

}