import { TestBed } from '@angular/core/testing';

import { BicyclePartServiceService } from './bicycle-part-service.service';

describe('BicyclePartServiceService', () => {
  let service: BicyclePartServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BicyclePartServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
