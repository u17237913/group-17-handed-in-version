import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Antenna } from 'src/app/Classes/Antenna/antenna';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import { AntennaStatus } from 'src/app/Classes/AntennaStatus/antenna-status';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root',
})
export class AntennaService {
  AntennaForm: any;

  constructor(private http: HttpClient, public audit: AuditTrailService) {}

  GetAntennas(): Observable<Antenna[]> {
    //this.audit.addAuditTrail(4, "Antenna", null, null).subscribe(() => console.error);
    return this.http.get<Antenna[]>(
      environment.apiURL + '/Antennas/GetAntennas'
    );
  }

  AddAntenna(newAntenna: Antenna) {
    this.audit
      .addAuditTrail(1, 'Antenna', newAntenna, null)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/Antennae/PostAntenna',
      newAntenna
    );
  }

  UpdateAntenna(id: any, antenna: Antenna, olddata: any) {
    this.audit
      .addAuditTrail(2, 'Antenna', antenna, olddata)
      .subscribe(() => console.error);
    // debugger;
    return this.http.put(
      environment.apiURL + '/Antennas/PutAntenna?id=' + id,
      antenna
    );
  }

  DeleteAntenna(id: number, olddata: any) {
    this.audit
      .addAuditTrail(3, 'Antenna', null, olddata)
      .subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/Antennas/DeleteAntenna?id=' + id
    );
  }

  GetLocations(): Observable<LocationViewModel[]> {
    //this.audit.addAuditTrail(4, "Village, Suburb, City, Province, and Country", null, null).subscribe(() => console.error);
    return this.http.get<LocationViewModel[]>(
      environment.apiURL + '/Schools/GetLocations'
    );
  }

  GetAntennaStatus(): Observable<AntennaStatus[]> {
    //this.audit.addAuditTrail(4, "Antenna", null, null).subscribe(() => console.error);
    return this.http.get<AntennaStatus[]>(
      environment.apiURL + '/Antennas/GetAntennaStatus'
    );
  }
}
