import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Tag } from 'src/app/Classes/Tag/tag';
import { TagStatus } from 'src/app/Classes/TagStatus/tag-status';
import { TagAssignmentVM } from 'src/app/Classes/Tag/tag-assignment-vm';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root',
})
export class TagService {
  TagForm: any;

  constructor(private http: HttpClient, public audit : AuditTrailService) {}

  GetTags(): Observable<Tag[]> {
    //this.audit.addAuditTrail(4, "Tag", null, null).subscribe(() => console.error);
    return this.http.get<Tag[]>(environment.apiURL + '/Tags/GetTags');
  }

  GetTagAssignmentList()  {
    //this.audit.addAuditTrail(4, "Tag and Bicycle", null, null).subscribe(() => console.error);
    return this.http.get<TagAssignmentVM[]>(
      environment.apiURL + '/Tags/GetTagAssignmentList'
    );
  }

  UnAssignTag(BicycleCode: number) {
    this.audit.addAuditTrail(2, "Tag and Bicycle", null, BicycleCode).subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/Tags/UassignTag?BicycleCode=' + BicycleCode,
      null
    );
  }

  AssignTag(BicycleCode: number, TagSerialNumber: string) {
    this.audit.addAuditTrail(2, "Tag and Bicycle", TagSerialNumber, BicycleCode).subscribe(() => console.error);
    return this.http.post(
      environment.apiURL +
        '/Tags/AssignBicycleTag?BicycleCode=' +
        BicycleCode +
        '&TagSerialNumber=' +
        TagSerialNumber,
      null
    );
  }

  GetBicyclesWithoutTags() {
    //this.audit.addAuditTrail(4, "Tag and Bicycle", null, null).subscribe(() => console.error);
    return this.http.get<Bicycle[]>(
      environment.apiURL + '/Tags/GetBicyclesWithoutTags'
    );
  }

  AddTag(newTag: Tag) {
    this.audit.addAuditTrail(1, "Tag", newTag, null).subscribe(() => console.error);
    return this.http.post(environment.apiURL + '/Tags/PostTag', newTag);
  }

  UpdateTag(id: any, tag: Tag, olddata: any) {
    this.audit.addAuditTrail(2, "Tag", tag, olddata).subscribe(() => console.error);
    // debugger;
    return this.http.put(environment.apiURL + '/Tags/PutTag?id=' + id, tag);
  }

  DeleteTag(id: number, olddata: any) {
    this.audit.addAuditTrail(3, "Tag", null, olddata).subscribe(() => console.error);
    return this.http.delete(environment.apiURL + '/Tags/DeleteTag?id=' + id);
  }

  GetTagStatus(): Observable<TagStatus[]> {
    //this.audit.addAuditTrail(4, "TagStatus", null, null).subscribe(() => console.error);
    return this.http.get<TagStatus[]>(
      environment.apiURL + '/Tags/GetTagStatus'
    );
  }
}
