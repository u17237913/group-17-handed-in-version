import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class BicycleBrandService {

  url : string;
  header : any;
  option : any;

  constructor(private http : HttpClient, public audit : AuditTrailService) { 
    this.url = environment.apiURL + '/BicycleBrands/';
    const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.option = {headers : headers};
}

//Bicycle Brand ---------------------------------------//

    //GET ---> Bicycle Brand ---------------------------------------------------------------------
    getBicycleBrands():Observable<BicycleBrand[]> {
      //this.audit.addAuditTrail(4, "BicycleBrand", null, null).subscribe(() => console.error);
      return this.http.get<BicycleBrand[]>(this.url + 'GetAllBicycleBrands/'); 
    }

    //ADD ---> Bicycle Brand ---------------------------------------------------------------------
    addBicycleBrand(newBicycleBrand : BicycleBrand){
      this.audit.addAuditTrail(1, "BicycleBrand", newBicycleBrand, null).subscribe(() => console.error);
      return this.http.post(this.url + 'AddBicycleBrand/' , newBicycleBrand); 
    }

    //UPDATE ---> Bicycle Brand ---------------------------------------------------------------------
    UpdateBicycleBrand(updatedBicycleBrand : BicycleBrand, olddata: any){
      this.audit.addAuditTrail(2, "BicycleBrand", updatedBicycleBrand, olddata).subscribe(() => console.error);
      return this.http.post(this.url + 'UpdateBicycleBrand/' , updatedBicycleBrand); 
  }

    //DELETE ---> Bicycle Brand ---------------------------------------------------------------------
    DeleteBicycleBrand(id : number, olddata: any){ 
      this.audit.addAuditTrail(3, "BicycleBrand", null, olddata).subscribe(() => console.error);
      return this.http.delete(this.url + 'DeleteBicycleBrand/?id=' + id, this.option); 
  }
}