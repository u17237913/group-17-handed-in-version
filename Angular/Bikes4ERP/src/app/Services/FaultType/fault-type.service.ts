import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';
import { Part } from 'src/app/Classes/Part/part';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';

@Injectable({
  providedIn: 'root',
})
export class FaultTypeService {
  constructor(private http: HttpClient, public audit: AuditTrailService) {}
  FaultTypeForm: any;
  PartFaultForm: any;

  GetFaultTypes() {
    //this.audit.addAuditTrail(4, "FaultType", null, null).subscribe(() => console.error);
    return this.http.get<FaultType[]>(
      environment.apiURL + '/FaultTypes/GetFaultTypes'
    );
  }

  GetParts() {
    //this.audit.addAuditTrail(4, 'Part', null, null).subscribe(() => console.error);
    return this.http.get<Part[]>(
      environment.apiURL + '/MaintenanceTasks/GetParts'
    );
  }
  AddFaultType(faulttype: FaultType) {
    // this.audit
    //   .addAuditTrail(1, 'FaultType', faulttype, null)
    //   .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/FaultTypes/PostFaultType',
      faulttype
    );

    
  }
  

  // AddPartFault(partFault: PartFault) {
  //   this.audit
  //     .addAuditTrail(1, 'PartFault', partFault, null)
  //     .subscribe(() => console.error);
  //   return this.http.post(
  //     environment.apiURL + '/PartFaults/PostPartFault',
  //     partFault
  //   );

    
  // }

  GetFaultTypeByPart(PartID: number) {
    //this.audit.addAuditTrail(4, "FaultType", null, null).subscribe(() => console.error);
    return this.http.get<FaultType>(
      environment.apiURL + '/FaultTypes/GetFaultTypeByPart?PartID=' + PartID
    );
  }
}
