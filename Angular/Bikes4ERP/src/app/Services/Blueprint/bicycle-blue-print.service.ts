import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Blueprint } from 'src/app/Classes/Blueprint/blueprint';
import { map } from 'rxjs/operators';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';
import { BicyclePart } from 'src/app/Classes/BicyclePart/bicycle-part';

@Injectable({
  providedIn: 'root',
})
export class BicycleBluePrintService {
  //public PictureName: string;
  public PictureName = new Subject<string>();

  constructor(private http: HttpClient, public audit: AuditTrailService) {}

  getsectionpart(id: number): Observable<any> {
    //this.audit.addAuditTrail(4, "Section", null, null).subscribe(() => console.error);
    return this.http.get<any>(
      environment.apiURL + '/BicyclePart/GetSectionPart?id=' + id
    );
  }

  getparttable(sectionID: number): Observable<any> {
    //this.audit.addAuditTrail(4, "Part", null, null).subscribe(() => console.error);
    return this.http
      .get<any>(
        environment.apiURL +
          '/BicyclePart/GetSectionPart?SectionID=' +
          sectionID
      )
      .pipe(map((result) => result));
  }

  getpartfault(id: number): Observable<any> {
    //this.audit.addAuditTrail(4, "PartFault", null, null).subscribe(() => console.error);
    return this.http.get<any>(
      environment.apiURL + '/BicyclePart/getRepairFault?id=' + id
    );
  }

  getpartfaulttable(PartID: number): Observable<any> {
    //this.audit.addAuditTrail(4, "PartFault", null, null).subscribe(() => console.error);
    return this.http
      .get<any>(environment.apiURL + '/BicyclePart/getRepairFault?id=' + PartID)
      .pipe(map((result) => result));
  }

  FindPart(bikepart: BicyclePart) {
    return this.http.post(
      environment.apiURL + '/BicyclePart/FindPart',
      bikepart
    );
  }
}
