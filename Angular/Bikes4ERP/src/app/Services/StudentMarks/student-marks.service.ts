import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { StudentMark } from 'src/app/Classes/StudentMark/student-mark';
import { MarkType } from 'src/app/Classes/MarkType/mark-type';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';
import { StudentMarksVM } from 'src/app/Classes/StudentMark/student-marks-vm';

@Injectable({
  providedIn: 'root'
})
export class StudentMarksService {

  StudentMarkForm : any;
  constructor( private http : HttpClient, public audit : AuditTrailService) { }

  GetStudentMarks () : Observable <StudentMark[]>  {
    //this.audit.addAuditTrail(4, "StudentMark", null, null).subscribe(() => console.error);
    return this.http.get<StudentMark[]>(environment.apiURL + '/StudentMarks/GetStudentMarks')
  }

  CaptureMark (studentMark : StudentMark) {
    this.audit.addAuditTrail(1, "StudentMArk", studentMark, null).subscribe(() => console.error);
    return this.http.post<StudentMark[]>(
      environment.apiURL + '/StudentMarks/PostStudentMark' , studentMark
    )
  }

  GetMarkTypes () : Observable <MarkType[]>  {
    //this.audit.addAuditTrail(4, "MarkType", null, null).subscribe(() => console.error);
    return this.http.get<MarkType[]>(environment.apiURL + '/StudentMarks/GetMarkTypes')
  }

  GetSpecificMarks (id : any) : Observable<StudentMark[]> {
    //this.audit.addAuditTrail(4, "StudentMark", null, null).subscribe(() => console.error);
    return this.http.get<StudentMark[]>(environment.apiURL + '/StudentMarks/GetStudentMarksforID?id=' + id);
  }

  GetStudentAllMarks () : Observable <StudentMarksVM[]>  {
    //this.audit.addAuditTrail(4, "StudentMark", null, null).subscribe(() => console.error);
    return this.http.get<StudentMarksVM[]>(environment.apiURL + '/StudentMarks/GetALLStudentMarks')
  }

}
