import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { School } from 'src/app/Classes/School/school';
import { Observable } from 'rxjs';
import { Village } from 'src/app/Classes/Village/village';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import { User } from '../user.model';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';
import { WorkDay } from 'src/app/Classes/WorkDay/work-day';

@Injectable({
  providedIn: 'root',
})
export class SchoolService {
  SchoolForm: any;

  constructor(private http: HttpClient, public audit: AuditTrailService) {}

  GetSchools(): Observable<School[]> {
    //this.audit.addAuditTrail(4, "School", null, null).subscribe(() => console.error);
    return this.http.get<School[]>(environment.apiURL + '/Schools/GetSchools');
  }

  GetLocations(): Observable<LocationViewModel[]> {
    //this.audit.addAuditTrail(4, "Village, Suburb, City, Province, and Country", null, null).subscribe(() => console.error);
    return this.http.get<LocationViewModel[]>(
      environment.apiURL + '/Schools/GetLocations'
    );
  }

  AddSchool(newSchool: School) {
    this.audit
      .addAuditTrail(1, 'School', newSchool, null)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/Schools/PostSchool',
      newSchool
    );
  }

  UpdateSchool(id: any, school: School, olddata: any) {
    this.audit
      .addAuditTrail(2, 'School', school, olddata)
      .subscribe(() => console.error);
    return this.http.put(
      environment.apiURL + '/Schools/PutSchool?id=' + id,
      school
    );
  }

  DeleteSchool(id: number, olddata: any) {
    this.audit
      .addAuditTrail(3, 'School', null, olddata)
      .subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/Schools/DeleteSchool?id=' + id
    );
  }

  GetWorkDays() {
    //this.audit.addAuditTrail(4, 'Part', null, null).subscribe(() => console.error);
    return this.http.get<WorkDay[]>(environment.apiURL + '/Users/GetWorkDays');
  }
}
