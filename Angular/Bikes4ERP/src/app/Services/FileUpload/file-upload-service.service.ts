import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { from, Observable } from 'rxjs';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})

export class FileUploadServiceService {
  //
  public upload_URL : string;
  public read_URL : string;
  public update_URL : string;

  public httpOptions : any;

  //
  public uploadPart_URL: string;
  public readPart_URL: string;
  public updatePart_URL: string;

  //
  public uploadFault_URL: string;
  public readFault_URL: string;
  public updateFault_URL: string;

  public readFaultIMAGE_URL: string;

  //
  public uploadDOC_URL: string;
  public readDOC_URL: string;
  public updateDOC_URL: string;

  constructor(private http:HttpClient, public audit : AuditTrailService) {
    //add the actual upload action plis -> SECTIONS
    this.upload_URL = environment.apiURL + '/BicyclePart/UploadFile';
    this.read_URL = environment.apiURL + '/BicyclePart/GetFile/';
    this.update_URL = environment.apiURL + '/BicyclePart/UpdateFile/';

    //add the actual upload action plis -> PARTS
    this.uploadPart_URL = environment.apiURL + '/BicyclePart/UploadPartImage';
    this.readPart_URL = environment.apiURL + '/BicyclePart/GetPartImage/';
    this.updatePart_URL = environment.apiURL + '/BicyclePart/UpdatePartImage/';

    //add the actual upload action plis -> DOCS
    this.uploadDOC_URL = environment.apiURL + '/Documents/UploadDocument';
    this.readDOC_URL = environment.apiURL + '/Documents/GetDocument/';
    this.updateDOC_URL = environment.apiURL + '/Documents/UpdateDocument/';

    //add the actual upload action plis  -> FAULTS
    this.uploadFault_URL = environment.apiURL + '/PartFaults/UploadFile';
    this.readFault_URL = environment.apiURL + '/PartFaults/GetFile/';
    this.updateFault_URL = environment.apiURL + '/PartFaults/UpdateFile/';

    this.readFaultIMAGE_URL = environment.apiURL + '/PartFaults/GetImage/';

    this.httpOptions = { headers : new HttpHeaders({'Content-Type' : 'application/json', }),};
  }

//section
  uploadFile(theFile : FileToUpload) : Observable<any> { 
    return this.http.post<FileToUpload>(this.upload_URL, theFile, this.httpOptions); 
  }

  updateFile(theFile : FileToUpload, id : number) : Observable<any> { 
    return this.http.post<FileToUpload>(this.update_URL + '?id=' +id, theFile, this.httpOptions); 
  }

  getFile(id : number) : Observable<any> { 
    //this.audit.addAuditTrail(4, null, null, null).subscribe(() => console.error);
    return this.http.get<FileToUpload>(this.read_URL + '?id=' + id, this.httpOptions)
  }

//part
  uploadPartImage(theFile : FileToUpload) : Observable<any> { 
    return this.http.post<FileToUpload>(this.uploadPart_URL, theFile, this.httpOptions); 
  }

  updatePartImage(theFile : FileToUpload, id : number) : Observable<any> { 
    return this.http.post<FileToUpload>(this.updatePart_URL + '?id=' +id, theFile, this.httpOptions); 
  }

  getPartImage(id : number) : Observable<any> { 
    //this.audit.addAuditTrail(4, null, null, null).subscribe(() => console.error);
    return this.http.get<FileToUpload>(this.readPart_URL + '?id=' + id, this.httpOptions)
  }

//fault
  uploadFaultImage(theFile : FileToUpload) : Observable<any> { 
    return this.http.post<FileToUpload>(this.uploadFault_URL, theFile, this.httpOptions); 
  }

  updateFaultImage(theFile : FileToUpload, id : number) : Observable<any> { 
    return this.http.post<FileToUpload>(this.updateFault_URL + '?id=' +id, theFile, this.httpOptions); 
  }

  getFaultImage(id : number) : Observable<any> { 
    //this.audit.addAuditTrail(4, null, null, null).subscribe(() => console.error);
    return this.http.get<FileToUpload>(this.readFault_URL + '?id=' + id, this.httpOptions)
  }

//
  getFaultIMAGE(id : number) : Observable<any> { 
    //this.audit.addAuditTrail(4, null, null, null).subscribe(() => console.error);
    return this.http.get<FileToUpload>(this.readFaultIMAGE_URL + '?id=' + id, this.httpOptions)
  }


//document
  uploadDocument(theFile : FileToUpload) : Observable<any> { 
    return this.http.post<FileToUpload>(this.uploadDOC_URL, theFile, this.httpOptions); 
  }

  updateDocument(theFile : FileToUpload, id : number) : Observable<any> { 
    return this.http.post<FileToUpload>(this.updateDOC_URL + '?id=' +id, theFile, this.httpOptions); 
  }

  getDocument(id : number) : Observable<any> { 
    //this.audit.addAuditTrail(4, null, null, null).subscribe(() => console.error);
    return this.http.get<FileToUpload>(this.readDOC_URL + '?id=' + id, this.httpOptions)
  }
}