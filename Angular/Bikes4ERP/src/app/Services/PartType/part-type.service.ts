import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { PartType } from 'src/app/Classes/PartType/part-type';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class PartTypeService {

  url : string;
  header : any;
  option : any;

  constructor(private http : HttpClient, public audit : AuditTrailService) { 
    this.url = environment.apiURL + '/PartTypes/';
    const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.option = {headers : headers};
}

//PART TYPE ---------------------------------------//

    //GET ---> Part Type ---------------------------------------------------------------------
    getPartTypes():Observable<PartType[]> {
      //this.audit.addAuditTrail(4, "PartType", null, null).subscribe(() => console.error);
      return this.http.get<PartType[]>(this.url + 'GetAllPartTypes/'); 
    }

    //ADD ---> Part Type ---------------------------------------------------------------------
    addPartType(newPartType : PartType){
      this.audit.addAuditTrail(1, "PartType", newPartType, null).subscribe(() => console.error);
      return this.http.post(this.url + 'AddPartType/' , newPartType); 
    }

    //UPDATE ---> Part Type ---------------------------------------------------------------------
    UpdatePartType(updatedPartType : PartType, olddata: any){
      this.audit.addAuditTrail(2, "PartType", updatedPartType, olddata).subscribe(() => console.error);
      return this.http.post(this.url + 'UpdatePartType/' , updatedPartType); 
  }

    //DELETE ---> Part Type ---------------------------------------------------------------------
    DeletePartType(id : number, olddata: any){ 
      this.audit.addAuditTrail(3, "PartType", null, olddata).subscribe(() => console.error);
      return this.http.delete(this.url + 'DeletePartType/?id=' + id, this.option); 
  }

}