import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand';
import { BicycleStudentViewModel } from 'src/app/Classes/Bicycle/bicycle-student-view-model';
import { BicycleStatus } from 'src/app/Classes/BicycleStatus/bicycle-status';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root',
})
export class BicycleService {
  BicycleForm: any;

  constructor(private http: HttpClient, public audit : AuditTrailService) {}

  GetBicycles(): Observable<Bicycle[]> {
    //this.audit.addAuditTrail(4, "Bicycle", null, null).subscribe(() => console.error);
    return this.http.get<Bicycle[]>(
      environment.apiURL + '/Bicycles/GetBicycles'
    );
  }

  AddBicycle(Bicycle: Bicycle) {
    this.audit.addAuditTrail(1, "Bicycle", Bicycle, null).subscribe(() => console.error);
    return this.http.post<Bicycle>(
      environment.apiURL + '/Bicycles/PostBicycle',
      Bicycle
    );
  }

  UpdateBicycle(id: any, bicycle: Bicycle, olddata: any) {
    this.audit.addAuditTrail(2, "Bicycle", bicycle, olddata).subscribe(() => console.error);
    return this.http.put(
      environment.apiURL + '/Bicycles/PutBicycle?id=' + id,
      bicycle
    );
  }

  DeleteBicycle(id: number, olddata: any) {
    this.audit.addAuditTrail(3, "Bicycle", null, olddata).subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/Bicycles/DeleteBicycle?id=' + id
    );
  }

  GetBicycleBrands(): Observable<BicycleBrand[]> {
    //this.audit.addAuditTrail(4, "BicycleBrand", null, null).subscribe(() => console.error);
    return this.http.get<BicycleBrand[]>(
      environment.apiURL + '/Bicycles/GetBicycleBrands'
    );
  }

  GetBicycleStatus() : Observable<BicycleStatus[]>{
    //this.audit.addAuditTrail(4, "BicycleStatus", null, null).subscribe(() => console.error);
    return this.http.get<BicycleStatus[]>(
      environment.apiURL + '/Bicycles/GetBicycleStatus'
    )
  }

  GetBicycleAssign() {
    //this.audit.addAuditTrail(4, "Bicycle, BicycleStudent, and Student", null, null).subscribe(() => console.error);
    return this.http.get<BicycleStudentViewModel[]>(
      environment.apiURL + '/Bicycles/GetBicycleAssign'
    );
  }

  //https://localhost:44348/api/Bicycles/UnassignBicycle?BicycleCode=1
  UnassignBicycle(bicycleCode: number) {
    this.audit.addAuditTrail(2, "Bicycle, BicycleStudent, and Student", null, bicycleCode).subscribe(() => console.error);
    return this.http.get(
      environment.apiURL +
        '/Bicycles/UnassignBicycle?BicycleCode=' +
        bicycleCode
    );
  }

  //https://localhost:44348/api/Bicycles/UnassignBicycle?BicycleCode=1&StudentID=1
  AssignBicycle(bicycleCode: number, StudentID: number) {
    this.audit.addAuditTrail(2, "Bicycle, BicycleStudent, and Student", StudentID, bicycleCode).subscribe(() => console.error);
    return this.http.post(
      environment.apiURL +
        '/Bicycles/AssignBicycle?BicycleCode=' +
        bicycleCode +
        '&StudentID=' +
        StudentID,
      null
      // environment.apiURL +
      //   '/Bicycles/AssignBicycle/' +
      //   bicycleCode +
      //   '/' +
      //   StudentID
    );
  }

}
