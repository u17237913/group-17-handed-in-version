import { Injectable, DebugEventListener } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Student } from 'src/app/Classes/Student/student';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { School } from 'src/app/Classes/School/school';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import { Guardian } from 'src/app/Classes/Guardian/guardian';
import { StuGuaViewModel } from 'src/app/Classes/StuGuaViewModel/stu-gua-view-model';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  StudentForm: any;
  GuardianForm: any;

  header: any;
  option: any;

  constructor(private http: HttpClient, public audit : AuditTrailService) {
    // const headerSettings: { [name: string]: string | string[] } = {};
    // //this.header = new HttpHeaders(headerSettings);
    // this.header = {
    //   Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    // };
    // let headers = new HttpHeaders({
    //   Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    // });
    // this.option = { headers: headers };
  }

  GetStudents(): Observable<Student[]> {
    //this.audit.addAuditTrail(4, "Student", null, null).subscribe(() => console.error);
    return this.http.get<Student[]>(
      environment.apiURL + '/Students/GetStudents'
    );
  }
  //The purpose of this endpoint is to return a list of students
  //specific to the School Administrator that is logged in at the time
  GetStudentList(): Observable<{ [key: string]: Student; }[]> {
    //this.audit.addAuditTrail(4, "Student", null, null).subscribe(() => console.error);
    const headers = {
      Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    };
    return this.http.get<{ [key: string]: Student; }[]>(
      environment.apiURL + '/Students/GetStudentList',
      { headers }
    );
  }

  GetLocations(): Observable<LocationViewModel[]> {
    //this.audit.addAuditTrail(4, "Village, Suburb, City, Province, and Country", null, null).subscribe(() => console.error);
    return this.http.get<LocationViewModel[]>(
      environment.apiURL + '/Students/GetLocations'
    );
  }

  AddStudent(StuGuaViewModel: StuGuaViewModel) {
    this.audit.addAuditTrail(1, "Student", StuGuaViewModel, null).subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/Students/PostStudent',
      StuGuaViewModel
    );
  }

  UpdateStudent(stuGuaViewModel: StuGuaViewModel, olddata: any) {
    this.audit.addAuditTrail(2, "Student, StudentGuardian, and Guardian", stuGuaViewModel, olddata).subscribe(() => console.error);
    return this.http.put(
      environment.apiURL + '/Students/PutStudent',
      stuGuaViewModel
    );
  }

  DeleteStudent(id: number, olddata: any) {
    this.audit.addAuditTrail(3, "Student", null, olddata).subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/Students/DeleteStudent?id=' + id
    );
  }

  GetSchools(): Observable<School[]> {
    //this.audit.addAuditTrail(4, "School", null, null).subscribe(() => console.error);
    return this.http.get<School[]>(environment.apiURL + '/Schools/GetSchools');
  }

  GetStuGua(id: number): Observable<StuGuaViewModel[]> {
    //this.audit.addAuditTrail(4, "Student, StudentGuardian, and Guardian", null, null).subscribe(() => console.error);
    return this.http.get<StuGuaViewModel[]>(
      environment.apiURL + '/Students/GetStuGua?id=' + id
    );
  }

  UpdateSchool(id: any, school: School, olddata: any) {
    this.audit.addAuditTrail(2, "School", school, olddata).subscribe(() => console.error);
    return this.http.put(
      environment.apiURL + '/Schools/PutSchool?id=' + id,
      school
    );
  }
}
