import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CommonModule } from '@angular/common';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { RxReactiveFormsModule } from "@rxweb/reactive-form-validators"
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders, HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { HomePageComponent } from './Components/home-page/home-page.component';
import { HttpErrorInterceptor } from './http-error.interceptor';

import { ViewAntennaComponent } from './Components/Antenna/view-antenna/view-antenna.component';
import { AntennaComponent } from './Components/Antenna/antenna/antenna.component';
import { CreateAntennaComponent } from './Components/Antenna/create-antenna/create-antenna.component';

import { FaultTaskComponent } from './Components/Fault-Task/fault-task/fault-task.component';
import { CreateFaultTaskComponent } from './Components/Fault-Task/create-fault-task/create-fault-task.component';
import { ViewFaultTaskComponent } from './Components/Fault-Task/view-fault-task/view-fault-task.component';
import { MaintenanceTaskComponent } from './Components/Maintenance-Task/maintenance-task/maintenance-task.component';
import { CreateMaintenanceTaskComponent } from './Components/Maintenance-Task/create-maintenance-task/create-maintenance-task.component';
import { ViewMaintenanceTaskComponent } from './Components/Maintenance-Task/view-maintenance-task/view-maintenance-task.component';

import { ViewBicyclePartComponent } from './Components/BicyclePart/view-bicycle-part/view-bicycle-part.component';
import { CreateBicyclePartComponent } from './Components/BicyclePart/create-bicycle-part/create-bicycle-part.component';
import { BicyclePartComponent } from './Components/BicyclePart/bicycle-part/bicycle-part.component';

import { ViewBicycleSectionComponent } from './Components/BicycleSection/view-bicycle-section/view-bicycle-section.component';
import { CreateBicycleSectionComponent } from './Components/BicycleSection/create-bicycle-section/create-bicycle-section.component';
import { BicycleSectionComponent } from './Components/BicycleSection/bicycle-section/bicycle-section.component';

import { SchoolComponent } from './Components/School/school/school.component';
import { ViewSchoolComponent } from './Components/School/view-school/view-school.component';
import { CreateSchoolComponent } from './Components/School/create-school/create-school.component';

import { StudentReportComponent } from './Components/Reporting/ManagementReports/student-report/student-report.component';
import { BicycleHistoryReportComponent } from './Components/Reporting/TransactionalReports/bicycle-history-report/bicycle-history-report.component';
import { LogReportComponent } from './Components/Reporting/TransactionalReports/log-report/log-report.component';
import { MechanicScheduleReportComponent } from './Components/Reporting/SimpleListReports/mechanic-schedule-report/mechanic-schedule-report.component';
import { MechanicScheduleRequirementsReportComponent } from './Components/Reporting/SimpleListReports/mechanic-schedule-requirements-report/mechanic-schedule-requirements-report.component';
import { PartsUsedReportComponent } from './Components/Reporting/SimpleListReports/parts-used-report/parts-used-report.component';

import { UserComponent } from './Components/User/User/user/user.component';
import { UserTypeComponent } from './Components/User/UserType/user-type/user-type.component';
import { CreateUserComponent } from './Components/User/User/create-user/create-user.component';
import { ViewUserTypeComponent } from './Components/User/UserType/view-user-type/view-user-type.component';
import { CreateUserTypeComponent } from './Components/User/UserType/create-user-type/create-user-type.component';

import { ForgotPasswordComponent } from './Components/forgot-password/forgot-password.component';
import { PasswordResetComponent } from './Components/forgot-password/password-reset/password-reset.component';
import { UserRoleComponent } from './Components/User/User/user-role/user-role.component';

import { ViewRouteComponent } from './Components/Route/Route/view-route/view-route.component';
import { RouteComponent } from './Components/Route/Route/route/route.component';
import { CreateRouteComponent } from './Components/Route/Route/create-route/create-route.component';

import { MechanicCalendarComponent } from './Components/MechanicHomepage/mechanic-calendar/mechanic-calendar.component';

import { JobListComponent } from './Components/MechanicHomepage/job-list/job-list.component';
import { FaultListComponent } from './Components/MechanicHomepage/fault-list/fault-list.component';
import { BicycleBlueprintComponent } from './Components/ReportRepairJob/bicycle-blueprint/bicycle-blueprint.component';
import { SectionBlueprintComponent } from './Components/ReportRepairJob/section-blueprint/section-blueprint.component';
import { ReportListComponent } from './Components/ReportRepairJob/report-list/report-list.component';
import { PartFaultsComponent } from './Components/ReportRepairJob/part-faults/part-faults.component';
import { QrScanComponent } from './Components/qr-scan/qr-scan.component';

//why is a service in here???
import { StudentService } from './Services/Student/student.service';
import { StudentComponent } from './Components/Student/student/student.component';
import { CreateStudentComponent } from './Components/Student/create-student/create-student.component';
import { ViewStudentComponent } from './Components/Student/view-student/view-student.component';
import { BicycleComponent } from './Components/Bicycle bike/bicycle/bicycle.component';
import { CreateBicycleComponent } from './Components/Bicycle bike/create-bicycle/create-bicycle.component';
import { ViewBicycleComponent } from './Components/Bicycle bike/view-bicycle/view-bicycle.component';
import { AssignBicycleComponent } from './Components/Bicycle bike/Assign bicycle/bicycle/assign-bicycle.component';
import {MatTableModule} from '@angular/material/table';

import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeFormat } from '@zxing/library';
import { BehaviorSubject } from 'rxjs';

import { ViewUserComponent } from './Components/User/User/view-user/view-user.component';
import { MaintenanceListComponent } from './Components/MechanicHomepage/maintenance-list/maintenance-list.component';
import { AssignBicycleStudentComponent } from './Components/Bicycle bike/Assign bicycle/bicycle/assign-bicycle-student/assign-bicycle-student.component';
import { AssignTagComponent } from './Components/Tag/assign-tag/assign-tag.component';
import { AssignTagBicycleComponent } from './Components/Tag/assign-tag-bicycle/assign-tag-bicycle.component';
import { TagComponent } from './Components/Tag/tag/tag.component';
import { ViewTagComponent } from './Components/Tag/view-tag/view-tag.component';

import { AuthGuard } from './Auth/auth.guard';
import { SearchLogEntryComponent } from './Components/LogEntry/search-log-entry/search-log-entry.component';
import { StudentMarkComponent } from './Components/Student marks/student mark/student-mark.component';
import { CaptureMarkComponent } from './Components/Student marks/capture-mark/capture-mark.component';
import { CreateTagComponent } from './Components/Tag/create-tag/create-tag.component';

import { MarkTypeComponent } from './Components/Mark Type/mark-type/mark-type.component';
import { CreateMarkTypeComponent } from './Components/Mark Type/create-mark-type/create-mark-type.component';
import { ViewMarkTypeComponent } from './Components/Mark Type/view-mark-type/view-mark-type.component';
import { AuditTrailComponent } from './Components/AuditTrail/audit-trail/audit-trail.component';
import { FaultTypeComponent } from './Components/FaultType/fault-type/fault-type.component';
import { CreateFaultTypeComponent } from './Components/FaultType/create-fault-type/create-fault-type.component';
import { ViewFaultTypeComponent } from './Components/FaultType/view-fault-type/view-fault-type.component';
import { BicycleBrandComponent } from './Components/BicycleBrand/bicycle-brand/bicycle-brand.component';
import { CreateBicycleBrandComponent } from './Components/BicycleBrand/create-bicycle-brand/create-bicycle-brand.component';
import { ViewBicycleBrandComponent } from './Components/BicycleBrand/view-bicycle-brand/view-bicycle-brand.component';
//Business Rules
import { BusinessRuleComponent } from './Components/BusinessRule/business-rule/business-rule.component';
import { CreateBusinessRuleComponent } from './Components/BusinessRule/create-business-rule/create-business-rule.component';
import { ViewBusinessRuleComponent } from './Components/BusinessRule/view-business-rule/view-business-rule.component';

import { MatSelectModule } from '@angular/material/select';
import { ViewMechanicComponent } from './Components/User/User/view-mechanic/view-mechanic.component';
import { AddSchoolComponent } from './Components/User/User/add-school/add-school.component';

//Document
import { DocumentComponent } from './Components/Document/document/document.component';
import { CreateDocumentComponent } from './Components/Document/create-document/create-document.component';
import { ViewDocumentComponent } from './Components/Document/view-document/view-document.component';
import { LoadingComponent } from './Services/LoadingScreen/Component/loading.component';
import { LoadingInterceptor } from './Services/LoadingScreen/Interceptor/loading.interceptor';
import { StaticComponent } from './Components/static/static.component';

//Part Type
import { PartTypeComponent } from './Components/PartType/part-type/part-type.component';
import { ViewPartTypeComponent } from './Components/PartType/view-part-type/view-part-type.component';
import { CreatePartTypeComponent } from './Components/PartType/create-part-type/create-part-type.component';
import { JwtInterceptor } from './Services/jwt.interceptor';
//import { StaticComponent } from './Components/static/static.component';

@NgModule({
  declarations: [
    AppComponent,

    LoginComponent,
    HomePageComponent,
    AuditTrailComponent,

    TagComponent,
    ViewTagComponent,
    CreateTagComponent,

    SearchLogEntryComponent,

    ViewBicyclePartComponent,
    CreateBicyclePartComponent,
    BicyclePartComponent,

    BicycleComponent,
    CreateBicycleComponent,
    ViewBicycleComponent,

    ViewBicycleSectionComponent,
    CreateBicycleSectionComponent,
    BicycleSectionComponent,

    CreateBicycleComponent,

    AntennaComponent,
    CreateAntennaComponent,
    ViewAntennaComponent,

    FaultTaskComponent,
    CreateFaultTaskComponent,
    ViewFaultTaskComponent,

    MaintenanceTaskComponent,
    CreateMaintenanceTaskComponent,
    ViewMaintenanceTaskComponent,

    MechanicCalendarComponent,
    JobListComponent,
    FaultListComponent,

    BicycleBlueprintComponent,
    SectionBlueprintComponent,

    ReportListComponent,
    PartFaultsComponent,

    QrScanComponent,

    SchoolComponent,
    ViewSchoolComponent,
    CreateSchoolComponent,

    StudentReportComponent,
    BicycleHistoryReportComponent,
    LogReportComponent,
    MechanicScheduleReportComponent,
    MechanicScheduleRequirementsReportComponent,
    PartsUsedReportComponent,

    SchoolComponent,
    ViewUserComponent,
    UserComponent,
    UserTypeComponent,

    CreateStudentComponent,
    StudentComponent,
    ViewStudentComponent,

    CreateAntennaComponent,
    CreateUserComponent,

    ViewUserTypeComponent,
    CreateUserTypeComponent,

    ForgotPasswordComponent,

    PasswordResetComponent,

    ViewRouteComponent,
    RouteComponent,
    CreateRouteComponent,

    UserRoleComponent,

    AssignBicycleComponent,
    AssignBicycleStudentComponent,

    StudentMarkComponent,
    CaptureMarkComponent,

    MarkTypeComponent,
    CreateMarkTypeComponent,
    ViewMarkTypeComponent,

    MaintenanceListComponent,

    AssignTagComponent,
    AssignTagBicycleComponent,

    BusinessRuleComponent,
    CreateBusinessRuleComponent,
    ViewBusinessRuleComponent,

    FaultTypeComponent,
    CreateFaultTypeComponent,
    ViewFaultTypeComponent,

    BicycleBrandComponent,
    CreateBicycleBrandComponent,
    ViewBicycleBrandComponent,

    ViewMechanicComponent,
    AddSchoolComponent,

    DocumentComponent,
    CreateDocumentComponent,
    ViewDocumentComponent,
    LoadingComponent,
    StaticComponent,

    PartTypeComponent,
    ViewPartTypeComponent,
    CreatePartTypeComponent,
    StaticComponent,
  ],

  imports: [
    BrowserModule,

    AppRoutingModule,
    MatTableModule,
    ZXingScannerModule,
    NgxQRCodeModule,
    RxReactiveFormsModule,
    ReactiveFormsModule,
    DropDownListModule,

    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),

    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),
    BrowserAnimationsModule,
    MatSelectModule,
  ],

  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],

  exports: [MechanicCalendarComponent],
})

export class AppModule {}