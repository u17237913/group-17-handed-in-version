import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(public router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,

    //state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    state: RouterStateSnapshot
  ): boolean {
    if (localStorage.getItem('UserToken') == null) {
      this.router.navigate(['/login']);
    } else {
      let role = next.data['roles'] as string;

      if (role) {
        if (role == localStorage.getItem('UserType')) {
          return true;
        } else {
          //There is supposed to be code in here to navigate you to a
          //forbidden page, can do that later
          this.router.navigate(['/login']);
          //return false;
        }
      }
    }

    return true;
  }
}
