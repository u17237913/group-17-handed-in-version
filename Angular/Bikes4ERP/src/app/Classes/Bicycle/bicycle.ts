export class Bicycle {
  //The Constructor
  constructor() {}

  //Data Fields
  BicycleID: number;
  BicycleStatusID: number;
  DateAdded: string;
  TagID: number;
  QRCode: string;
  BicyleCode: number;
  BicycleBrandID: number;
  BicycleBrandName: string;
  BicycleStatus: string;
} //Bicycle
