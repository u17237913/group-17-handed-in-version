export class AuditTrail {

    //The Constructor
    constructor(){}

    //Data Fields
    AuditTrailID : number;
    UserID : number;
    TransactionDescription : string;
    DateTime : string;
    Username : string;
    TransactionType : string;

}//AuditTrail