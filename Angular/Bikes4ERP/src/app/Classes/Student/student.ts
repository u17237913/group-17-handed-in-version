import { Guardian } from '../Guardian/guardian';

export class Student {
  constructor() {}

  StudentDoB: string;
  StudentFullName : string;
  Grade: number;
  GuardianID: number;
  SchoolID: number;
  StudentID: number;
  VillageID: number;
  PhoneNumber: number;
  StudentName: string;
  StudentSurname: string;
  BicycleCode: number;
  Guardian: Guardian;
}
