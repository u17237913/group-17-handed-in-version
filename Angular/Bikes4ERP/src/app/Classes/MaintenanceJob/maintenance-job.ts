export class MaintenanceJob {

    constructor (){}

    Assigned : boolean
    DateScheduled : string
    BicycleID : number
    CheckInID : number
    MaintenanceJobID : number
    BicycleCode: string
    MaintenanceJobStatusID : number
}
