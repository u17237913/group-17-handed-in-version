export class FaultType {
  //The Constructor
  constructor() {}

  //Data Fields
  FaultTypeID: number;
  FaultType1: string;
  PartNeeded: string;
  PartFaultID: number;
} //FaultType
