export class FaultListVM {
  constructor() {}
  FaultID: number;
  SectionName: string;
  PartName: string;
  FaultTypeName: string;
  JobFaultID: number;
}
