export class School {

    constructor () {}

    SchoolID : number
    VillageID : number
    SchoolTelephone : number
    SchoolEmail : string
    SchoolName : string
    VillageName : string;
}
