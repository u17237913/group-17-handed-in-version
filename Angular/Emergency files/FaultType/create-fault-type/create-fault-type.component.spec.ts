import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFaultTypeComponent } from './create-fault-type.component';

describe('CreateFaultTypeComponent', () => {
  let component: CreateFaultTypeComponent;
  let fixture: ComponentFixture<CreateFaultTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFaultTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFaultTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
