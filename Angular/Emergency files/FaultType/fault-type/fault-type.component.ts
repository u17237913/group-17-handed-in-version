import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { FaultTypeService } from 'src/app/Services/FaultType/fault-type.service';
import { MyServiceService } from 'src/app/Services/my-service.service';

@Component({
  selector: 'app-fault-type',
  templateUrl: './fault-type.component.html',
  styleUrls: ['./fault-type.component.css'],
})
export class FaultTypeComponent implements OnInit {
  MaintenanceTaskList: FaultType[];
  public values = [];
  MaintenanceTaskForm: FaultType;
  //search stuff
  public TempArray: FaultType[] = [];
  public SearchArray: FaultType[] = [];
  isArrayStored: boolean = false;
  constructor(
    private MaintenanceTaskService: FaultTypeService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.MaintenanceTaskService.GetFaultTypes().subscribe((data) => {
      this.MaintenanceTaskList = data as FaultType[];
      console.log(this.MaintenanceTaskList);
    });
  }

  ViewMaintenanceTask(id: number) {
    this.router.navigate(['/viewfaulttype', { ID: id }],{ skipLocationChange: true });
  }

  CreateFaultType() {
    this.router.navigate(['/createfaulttype']);
  }
  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.MaintenanceTaskList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.MaintenanceTaskList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.MaintenanceTaskList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.MaintenanceTaskList.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.FaultType1.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.MaintenanceTaskList = this.SearchArray;
    } else {
      this.MaintenanceTaskList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}