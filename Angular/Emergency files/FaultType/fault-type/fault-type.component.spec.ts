import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaultTypeComponent } from './fault-type.component';

describe('FaultTypeComponent', () => {
  let component: FaultTypeComponent;
  let fixture: ComponentFixture<FaultTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaultTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaultTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
