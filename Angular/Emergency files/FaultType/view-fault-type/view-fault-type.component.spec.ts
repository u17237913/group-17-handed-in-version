import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFaultTypeComponent } from './view-fault-type.component';

describe('ViewFaultTypeComponent', () => {
  let component: ViewFaultTypeComponent;
  let fixture: ComponentFixture<ViewFaultTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFaultTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFaultTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
