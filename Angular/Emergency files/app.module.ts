import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './home-page/home-page.component';

import { ViewAntennaComponent } from './Antenna/view-antenna/view-antenna.component';
import { AntennaComponent } from './Antenna/antenna/antenna.component';
import { CreateAntennaComponent } from './Antenna/create-antenna/create-antenna.component';

import { FaultTaskComponent } from './Fault-Task/fault-task/fault-task.component';
import { CreateFaultTaskComponent } from './Fault-Task/create-fault-task/create-fault-task.component';
import { ViewFaultTaskComponent } from './Fault-Task/view-fault-task/view-fault-task.component';
import { MaintenanceTaskComponent } from './Maintenance-Task/maintenance-task/maintenance-task.component';
import { CreateMaintenanceTaskComponent } from './Maintenance-Task/create-maintenance-task/create-maintenance-task.component';
import { ViewMaintenanceTaskComponent } from './Maintenance-Task/view-maintenance-task/view-maintenance-task.component';

import { ViewBicyclePartComponent } from './BicyclePart/view-bicycle-part/view-bicycle-part.component';
import { CreateBicyclePartComponent } from './BicyclePart/create-bicycle-part/create-bicycle-part.component';
import { BicyclePartComponent } from './BicyclePart/bicycle-part/bicycle-part.component';

import { ViewBicycleSectionComponent } from './BicycleSection/view-bicycle-section/view-bicycle-section.component';
import { CreateBicycleSectionComponent } from './BicycleSection/create-bicycle-section/create-bicycle-section.component';
import { BicycleSectionComponent } from './BicycleSection/bicycle-section/bicycle-section.component';

import { SchoolComponent } from './School/school/school.component';
import { ViewSchoolComponent } from './School/view-school/view-school.component';
import { CreateSchoolComponent } from './School/create-school/create-school.component';
import { StudentReportComponent } from './Reporting/ManagementReports/student-report/student-report.component';
import { BicycleHistoryReportComponent } from './Reporting/TransactionalReports/bicycle-history-report/bicycle-history-report.component';
import { LogReportComponent } from './Reporting/TransactionalReports/log-report/log-report.component';
import { MechanicScheduleReportComponent } from './Reporting/SimpleListReports/mechanic-schedule-report/mechanic-schedule-report.component';
import { MechanicScheduleRequirementsReportComponent } from './Reporting/SimpleListReports/mechanic-schedule-requirements-report/mechanic-schedule-requirements-report.component';
import { PartsUsedReportComponent } from './Reporting/SimpleListReports/parts-used-report/parts-used-report.component';

import { UserComponent } from './User/User/user/user.component';
import { UserTypeComponent } from './User/UserType/user-type/user-type.component';
import { CreateUserComponent } from './User/User/create-user/create-user.component';
import { ViewUserTypeComponent } from './User/UserType/view-user-type/view-user-type.component';
import { CreateUserTypeComponent } from './User/UserType/create-user-type/create-user-type.component';


import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PasswordResetComponent } from './forgot-password/password-reset/password-reset.component';
import { ViewRouteComponent } from './Route/Route/view-route/view-route.component';
import { RouteComponent } from './Route/Route/route/route.component';
import { CreateRouteComponent } from './Route/Route/create-route/create-route.component';
import { UserRoleComponent } from './User/User/user-role/user-role.component';
import { TestComponentComponent } from './test-component/test-component.component';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { MechanicCalendarComponent } from './MechanicHomepage/mechanic-calendar/mechanic-calendar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JobListComponent } from './MechanicHomepage/job-list/job-list.component';
import { FaultListComponent } from './MechanicHomepage/fault-list/fault-list.component';
import { BicycleBlueprintComponent } from './ReportRepairJob/bicycle-blueprint/bicycle-blueprint.component';
import { SectionBlueprintComponent } from './ReportRepairJob/section-blueprint/section-blueprint.component';
import { ReportListComponent } from './ReportRepairJob/report-list/report-list.component';
import { PartFaultsComponent } from './ReportRepairJob/part-faults/part-faults.component';
import { QrScanComponent } from './qr-scan/qr-scan.component';
import { CheckinBicycleComponent } from './MechanicHomepage/checkin-bicycle/checkin-bicycle.component';
import { FixFaultComponent } from './MechanicHomepage/fix-fault/fix-fault.component';

@NgModule({
  declarations: [
    AppComponent, 
    LoginComponent, 
    HomePageComponent, 
    
    ViewBicyclePartComponent, 
    CreateBicyclePartComponent, 
    BicyclePartComponent, 
    
    ViewBicycleSectionComponent, 
    CreateBicycleSectionComponent, 
    BicycleSectionComponent,

    AntennaComponent, 
    CreateAntennaComponent, 
    ViewAntennaComponent,
    FaultTaskComponent, CreateFaultTaskComponent, ViewFaultTaskComponent, MaintenanceTaskComponent, CreateMaintenanceTaskComponent, ViewMaintenanceTaskComponent,
    AppComponent, 
    LoginComponent, 
    HomePageComponent, 

    MechanicCalendarComponent, 
    JobListComponent, 
    FaultListComponent, 
    BicycleBlueprintComponent, 
    SectionBlueprintComponent, 
    ReportListComponent, 
    PartFaultsComponent, 
    QrScanComponent, 
    CheckinBicycleComponent, 
    FixFaultComponent,
    SchoolComponent, 
    ViewSchoolComponent, 
    CreateSchoolComponent, 
    
    StudentReportComponent, 
    BicycleHistoryReportComponent, 
    LogReportComponent, 
    MechanicScheduleReportComponent, 
    MechanicScheduleRequirementsReportComponent, 
    PartsUsedReportComponent,

    UserComponent,
    UserTypeComponent,

    CreateUserComponent,

    ViewUserTypeComponent,

    CreateUserTypeComponent,

    ForgotPasswordComponent,

    PasswordResetComponent,

    ViewRouteComponent,

    RouteComponent,

    CreateRouteComponent,

    UserRoleComponent,

    TestComponentComponent,],

    
  imports: [
    BrowserModule, 
    AppRoutingModule, 
    CalendarModule.forRoot({ 
      provide: DateAdapter, 
      useFactory: adapterFactory }), 
    NgbModule, 
    CommonModule,
    FormsModule,
    NgbModalModule,
    FlatpickrModule.forRoot()],
  providers: [],
  bootstrap: [AppComponent],
  exports: [MechanicCalendarComponent],
})
export class AppModule {}
