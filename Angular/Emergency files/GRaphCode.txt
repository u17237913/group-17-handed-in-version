GetMarks (){
    //We destroy the chart if it exists so we can create a new one without the old one still taking memory and being loaded in the DOM
    if(this.chart) {this.chart.destroy();}

    this.studentMarksService.GetSpecificMarks(this.StudentID).subscribe((data) =>{
    //this.StudentMarkList = data;
    debugger;
    this.dynamicStudentMarks = data;

    //Get Data for the x-axis and the y-axis -!-!-> DATASET 1
    let keys = data['dynamicStudentMarks'].map((z) => z.Date);
    let Vals = data['dynamicStudentMarks'].map((z) => z.Mark);
    console.log(keys);

    this.chart = new Chart('canvas', {
      type: 'bar',
      data: {
        labels: keys,
        datasets: [
          {
            label: 'Marks',
            data: Vals,
            fill : false,
            barPercentage: 0.75,
            backgroundColor: ['rgba(0, 24, 68, 0.8)'],
            borderColor: ['rgba(0, 24, 68, 1)'],
            borderWidth: 1,
            order: 1
          }],
        },
      options:
      {
        legend: { display: true },
        title: {
          display: true,
          text: 'My Marks'
              },
        scales: {
          xAxes: [{
            display: true,
            gridLines: { offsetGridLines: true }
            }],
          yAxes: [{
            ticks: { beginAtZero: true, max: 100 }
            }],
        }
      }
    }); 
  });
}
/*    })
    
  }*/